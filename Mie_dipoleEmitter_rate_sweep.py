import numpy as np
import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite
from Mie_calculation.Mie_dipoleEmitter_rates import Mie_dipoleEmitter_rates
import Mie_calculation.Mie_dipoleEmitter_rates_testing \
    as Mie_dipoleEmitter_rates_testing


fluorophore = {}
fluorophore['emissionWavelength'] = 520 * 10 ** -9
#fluorophore['emissionWavelength'] = 670 * 10 ** -9

# SWEEP PARAMETERS

n_max = 10

N = 50

emissionWavelength = np.linspace(fluorophore['emissionWavelength'], fluorophore['emissionWavelength'], num=N)  # wavelength in m

sphereRadius = 1 / 2 * np.linspace(80, 80, num=N) * 10 ** -9

dipoleDistance = np.linspace(18, 18, num=N) * 10 ** -9

dipoleVector_theta = np.linspace(0., 90.) * np.pi /180

"""dipoleVector_theta = np.zeros(np.shape(emissionWavelength))
dipoleVector_theta[0] = 0
thetaN = dipoleVector_theta.size
for idx in range(1,thetaN):
    dipoleVector_theta[idx] = np.arccos(np.cos(dipoleVector_theta[idx-1])-1/thetaN)"""

dipoleVector_phi = np.linspace(0., 0.) * np.pi /180

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(emissionWavelength),
    refractiveIndex.silver(emissionWavelength),
    compositionRatio
)
# refractiveIndex_sphere = refractiveIndex.gold(emissionWavelength)
refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
# refractiveIndex_medium = np.real(refractiveIndex.vacuum(emissionWavelength))

# position of the dipole
# the radial coordinate of the dipole position ( initializing here as 0 as it
# should be set in the loop over the sweep parameters:
dipolePosition_r = 0.
# the polar angle of the dipole position:
dipolePosition_theta = 90. * np.pi/180
# the azimuthal angle of the dipole position:
dipolePosition_fi = 0. * np.pi/180
# the position of the dipole emitter in spherical coordiante system centered at the sphere center:
dipolePosition_sphericalComponents = np.array([
    [dipolePosition_r],
    [dipolePosition_theta],
    [dipolePosition_fi]])

# SWEEP OVER THE PARAMETERS

absorptionRate = np.zeros(N)
radiativeRate = np.zeros(N)
totalRate = np.zeros(N)

for idx in range(0, N):

    # update the diple radial position coordinate:
    dipolePosition_r = sphereRadius[idx] + dipoleDistance[idx]
    dipolePosition_sphericalComponents[0] = dipolePosition_r

    # strength and orientation of the dipole (components of the dipole vector in
    # the spherical coordiante system at the dipole position, the "r", "theta"
    # and "fi" unit vectors, respectively):
    dipoleStrength = 1.0
    dipoleVector = dipoleStrength * np.array(
        [
            [np.cos(dipoleVector_theta[idx])],
            [np.sin(dipoleVector_theta[idx]) * np.cos(dipoleVector_phi[idx])],
            [np.sin(dipoleVector_theta[idx]) * np.sin(dipoleVector_phi[idx])],
        ]
    )
    
    # alternatively:
    # perpendicular to the nanosphere surface:
    # dipoleVector = np.array([[1.0], [0.0], [0.0]])
    # parallel to the nanosphere surface:
    # dipoleVector=np.array([[0.], [1.], [0.]])
    # dipoleVector=np.array([[0.], [0.], [1.]])

    # calculate the modified dipole absorption and scattering rates:
    [W_a, W_s, W_t] = Mie_dipoleEmitter_rates(
       emissionWavelength[idx],
       sphereRadius[idx],
       dipolePosition_sphericalComponents,
       dipoleVector,
       refractiveIndex_sphere[idx],
       refractiveIndex_medium[idx],
       n_max
    )

    # for testing purposes, perpendicular dipole orientation to the nanosphere
    # surface:
    """[W_a, W_s, W_t] = Mie_dipoleEmitter_rates_testing.perpendicular2(
        emissionWavelength[idx],
        sphereRadius[idx],
        dipolePosition_sphericalComponents,
        dipoleVector,
        refractiveIndex_sphere[idx],
        refractiveIndex_medium[idx],
        n_max
    )"""

    # for testing purposes, parallel dipole orientation to the nanosphere
    # surface:
    """[W_a, W_s, W_t] = Mie_dipoleEmitter_rates_testing.parallel2(
        emissionWavelength[idx],
        sphereRadius[idx],
        dipolePosition_sphericalComponents,
        dipoleVector,
        refractiveIndex_sphere[idx],
        refractiveIndex_medium[idx],
        n_max
    )"""

    """[W_a, W_s, W_t] = Mie_dipoleEmitter_rates_testing.vectorized(
        emissionWavelength[idx],
        sphereRadius[idx],
        dipolePosition_sphericalComponents,
        dipoleVector,
        refractiveIndex_sphere[idx],
        refractiveIndex_medium[idx],
        n_max
    )"""

    absorptionRate[idx] = W_a
    radiativeRate[idx] = W_s
    totalRate[idx] = W_t


# VISUALIZATION

#quantity = "wavelength":
#quantity = "radius"
#quantity = "diameter"
#quantity = "dimensionless diffraction parameter"
#quantity = "distance"
quantity = "dipole polar angle"
#quantity = "polar angle cumulative probability"
#quantity = "dipole azimuth angle"

if quantity == "wavelength":
    # The "x" quantity in case of wavelength sweep:
    x = emissionWavelength * 10**9	
    xlabel = 'wavelength [nm]'

elif quantity == "radius":
    # The "x" quantity in case of particle radius sweep:
    x = sphereRadius * 10**9
    xlabel = 'particle radius'

elif quantity == "diameter":
    # The "x" quantity in case of particle diameter sweep:
    x = 2 * sphereRadius * 10**9
    xlabel = "particle diameter"

elif quantity == "dimensionless diffraction parameter":
    # The "x" quantity in case of particle dimensionless diffraction parameter:
    x = 2*np.pi*sphereRadius/emissionWavelength
    xlabel = 'dimensionless diffraction parameter'

elif quantity == "distance":
    # The "x" quantity in case of dipole distance sweep:
    x = dipoleDistance * 10**9	
    xlabel = 'dipole distance [nm]'
    
elif quantity == "dipole polar angle":
    # The "x" quantity in case of theta component of the dipole orientation sweep:
    x = dipoleVector_theta * 180 / np.pi
    xlabel = 'dipole polar angle [°]'
    
elif quantity == "polar angle cumulative probability":
    # The "x" quantity in case of theta component of the dipole orientation sweep:
    x = 1.0 * (1 - np.cos(dipoleVector_theta))
    xlabel = 'polar angle cumulative probability'
    
elif quantity == "dipole azimuth angle":
    # The "x" quantity in case of phi component of the dipole orientation sweep:
    x = dipoleVector_phi * 180 / np.pi
    xlabel = 'dipole azimuth angle [°]'

#plottingType = 'all rate'
plottingType = 'radiative rate'

if plottingType == 'all rate':
    plt.figure()
    plt.plot(x, totalRate, label='Total rate')
    plt.plot(x, radiativeRate, label='Radiative rate')
    plt.plot(x, absorptionRate, label='Absorption rate')
    plt.legend()
    
    plt.xlabel(xlabel)
    plt.ylabel("Rate")
    #plt.ylim(0, 40)
    plt.show()
elif plottingType == 'radiative rate':
    #plt.figure()
    plt.plot(x, radiativeRate, label = '{} nm emission'.format(fluorophore['emissionWavelength']*1e9))
    plt.legend()
    
    plt.xlabel(xlabel)
    plt.ylabel("Radiative rate")
    plt.show()
    