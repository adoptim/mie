#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 15 11:16:55 2023

@author: novakt
"""
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def findSinglePeakPosition(x, y):
    # This function finds determines the position of the highest peak with "sub-pixel" position using qudratic interpolation

    dataN = None
    if np.size(x) == np.size(y):
        dataN = np.size(x)
    else:
        raise Exception(
            "The \"x\" and \"y\" vectors that were supplied to the peak finding does not have equal lengths ({} and {}, respectively).".format(
                np.size(x), np.size(y)))

    # index of the maximal data pont:
    peakIndex = np.argmax(y)

    peakPosition = None
    if peakIndex == 0 or peakIndex == dataN - 1:
        # if the maximal data point is on the edge, do not interpolate

        peakPosition = x[peakIndex]

    else:
        # obtain finer peak position using quadratic interpolation:

        # coordinates of the maximal point and of its neighbours:
        x_left = x[peakIndex - 1]
        x_center = x[peakIndex]
        x_right = x[peakIndex + 1]

        # values of the maximal point and of its neighbours:
        y_left = y[peakIndex - 1]
        y_center = y[peakIndex]
        y_right = y[peakIndex + 1]

        # interpolate the peak position
        peakPosition = \
        ((y_left - y_center) * (x_center ** 2 - x_right ** 2) + (y_right - y_center) * (x_left ** 2 - x_center ** 2)) / \
        ((y_left - y_center) * (2 * x_center - 2 * x_right) + (y_right - y_center) * (2 * x_left - 2 * x_center))

    if peakPosition is None:
        raise Exception("Could not determine the position of the peak.")

    return peakPosition




#dataFileName_AF488 = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2022_03_04_AF488_DNA_PAINT_MPTES/M0304_a1_488_50mW_AOTF_20_TIRF_pol_p/M0304_a1_488_50mW_AOTF_20_TIRF_pol_p_filtered_locs.csv'
#dataFileName_AF488 = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2022_03_04_AF488_DNA_PAINT_MPTES/M0304_a1_488_50mW_AOTF_20_TIRF_pol_s/M0304_a1_488_50mW_AOTF_20_TIRF_pol_s_filtered_locs.csv'

dataFileName_AF488 = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2022_03_04_AF488_DNA_PAINT_MPTES/s2_M0304_a2_488_50mW_AOTF_100_TIRF_pol_p/s2_M0304_a2_488_50mW_AOTF_100_TIRF_pol_p_filtered_locs.csv'
#dataFileName_AF488 = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2022_03_04_AF488_DNA_PAINT_MPTES/s2_M0304_a1_488_50mW_AOTF_100_TIRF_pol_s/s2_M0304_a1_488_50mW_AOTF_100_TIRF_pol_s_filtered_locs.csv'

# dataFileName_AF488 = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2022_03_04_AF488_DNA_PAINT_MPTES/s3_M0304_a1_488_50mW_AOTF_100_TIRF_pol_p/s3_M0304_a1_488_50mW_AOTF_100_TIRF_pol_p_filtered_locs.csv'
# dataFileName_AF488 = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2022_03_04_AF488_DNA_PAINT_MPTES/s3_M0304_a1_488_50mW_AOTF_100_TIRF_pol_s/s3_M0304_a1_488_50mW_AOTF_100_TIRF_pol_s_filtered_locs.csv'

# Atto647N
dataFileName_Atto647N = '/home/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2021_07_26_Atto647N_DNA_PAINT/s2_647_40mW_AOTF_50_pol_TIRF_y_locs.csv'
#dataFileName_Atto647N = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2021_07_26_Atto647N_DNA_PAINT/s2_647_40mW_AOTF_50_pol_TIRF_x_locs.csv'

#dataFileName_Atto647N = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2021_07_26_Atto647N_DNA_PAINT/s2_647_30mW_AOTF_30_pol_TIRF_y/s2_647_30mW_AOTF_30_pol_TIRF_y_locs.csv'
#dataFileName_Atto647N = '/mnt/Userdata/novakt/NAS/lab/RnD-Meas/NovakT/Au_Ag_nanosphere_polarization/2021_07_26_Atto647N_DNA_PAINT/s2_647_30mW_AOTF_30_pol_TIRF_x/s2_647_30mW_AOTF_30_pol_TIRF_x_locs.csv'

#######################x
# Atto647N

dataName_Atto647N, extension = os.path.splitext(dataFileName_Atto647N)
pairedIndicesFileName_Atto647N = dataName_Atto647N + '_pairings' + extension
G_factor_Atto647N = 1.0

indices_Atto647N = np.loadtxt(pairedIndicesFileName_Atto647N, delimiter=',', usecols=(0,1), skiprows=1, dtype=int)
sumSignal_Atto647N = np.loadtxt(dataFileName_Atto647N, delimiter=',', usecols=4, skiprows=1)
sumSignal_ordinary_Atto647N = sumSignal_Atto647N[indices_Atto647N[:, 0]-1]
sumSignal_extraodinary_Atto647N = sumSignal_Atto647N[indices_Atto647N[:, 1]-1]

polarizationDegree_Atto647N = (G_factor_Atto647N*sumSignal_ordinary_Atto647N-sumSignal_extraodinary_Atto647N)/(sumSignal_ordinary_Atto647N+G_factor_Atto647N*sumSignal_extraodinary_Atto647N)

##########################
# AF488

dataName_AF488, extension = os.path.splitext(dataFileName_AF488)
pairedIndicesFileName_AF488 = dataName_AF488 + '_pairings' + extension
G_factor_AF488 = 1.0

indices_AF488 = np.loadtxt(pairedIndicesFileName_AF488, delimiter=',', usecols=(0,1), skiprows=1, dtype=int)
sumSignal_AF488 = np.loadtxt(dataFileName_AF488, delimiter=',', usecols=4, skiprows=1)
sumSignal_ordinary_AF488 = sumSignal_AF488[indices_AF488[:, 0]-1]
sumSignal_extraodinary_AF488 = sumSignal_AF488[indices_AF488[:, 1]-1]

polarizationDegree_AF488 = (G_factor_AF488*sumSignal_ordinary_AF488-sumSignal_extraodinary_AF488)/(sumSignal_ordinary_AF488+G_factor_AF488*sumSignal_extraodinary_AF488)



fig=plt.figure()
plt.rc('font', size=12)
ax1=fig.add_subplot(111, label="AF488")
ax2=fig.add_subplot(111, label="Atto647N ", frame_on=False)

#plt.ylim(0,1.1)
plt.axvline(x=0, color='black', ls='-', linewidth = 3)

Atto647N_color = 'red'
binValues_Atto647N, binEdges_Atto647N, p = ax1.hist(polarizationDegree_Atto647N, bins=55, range=(-0.5, 0.5), edgecolor = "black", alpha = 0.5, color = Atto647N_color)
#ax1.set_xlabel('polarization degree', color=Atto647N_color)
#ax1.set_ylabel('localization number', color=Atto647N_color)
ax1.set_xlabel('polarization degree', color="black", size = 16)
ax1.set_ylabel('localization frequency', color="black", size = 16)
ax1.tick_params(axis='x', colors=Atto647N_color)
ax1.tick_params(axis='y', colors=Atto647N_color)

binCenters_Atto647N = (binEdges_Atto647N[0:-1] + binEdges_Atto647N[1:])/2
peakPosition_Atto647N = findSinglePeakPosition(binCenters_Atto647N, binValues_Atto647N)

ax1.tick_params(
    axis='y',          # changes apply to the y-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    right=False,         # ticks along the top edge are off
    labelleft=False) # labels along the bottom edge are off

AF488_color = 'blue'
binValues_AF488, binEdges_AF488, p = ax2.hist(polarizationDegree_AF488, bins=55, range=(-0.5, 0.5), edgecolor = "black", alpha = 0.5, color = AF488_color)
ax2.xaxis.tick_top()
ax2.yaxis.tick_right()
#ax2.set_xlabel('polarization degree', color=AF488_color) 
#ax2.set_ylabel('localization number', color=AF488_color)       
ax2.xaxis.set_label_position('top') 
ax2.yaxis.set_label_position('right') 
ax2.tick_params(axis='x', colors=AF488_color)
#ax2.tick_params(axis='y', colors=AF488_color)
ax2.invert_xaxis()

ax2.tick_params(
    axis='y',          # changes apply to the y-axis
    which='both',      # both major and minor ticks are affected
    left=False,      # ticks along the bottom edge are off
    right=False,         # ticks along the top edge are off
    labelright=False) # labels along the bottom edge are off


binCenters_AF488 = (binEdges_AF488[0:-1] + binEdges_AF488[1:])/2
peakPosition_AF488 = findSinglePeakPosition(binCenters_AF488, binValues_AF488)



AF488_zeroPosition = -0.033
Atto647N_zeroPosition = -0.054

if False:
    ax1.text(0.75, 0.8, 'peak position:\n{0:.3f}'.format(peakPosition_Atto647N), transform=plt.gca().transAxes, color=Atto647N_color)
    ax2.text(0.1, 0.8, 'peak position:\n{0:.3f}'.format(peakPosition_AF488), transform=plt.gca().transAxes, color=AF488_color)
    
    ax1.axvline(x=peakPosition_Atto647N, color='black', ls='--', linewidth=3)
    ax2.axvline(x=peakPosition_AF488, color='black', ls='--', linewidth=3)
else:
    ax1.text(0.75, 0.8, 'peak shift:\n{0:.3f}'.format(peakPosition_Atto647N-Atto647N_zeroPosition), transform=plt.gca().transAxes, color=Atto647N_color)
    ax2.text(0.1, 0.8, 'peak shift:\n{0:.3f}'.format(peakPosition_AF488-AF488_zeroPosition), transform=plt.gca().transAxes, color=AF488_color)
    
    ax1.axvline(x=peakPosition_Atto647N, color='black', ls='--', linewidth=3)
    ax1.axvline(x=Atto647N_zeroPosition, color='black', ls='dotted', linewidth=3)
    arrowPosition_y = (ax1.get_ylim()[1]-ax1.get_ylim()[0]) * 0.8 + ax1.get_ylim()[0];
    ax1.arrow(Atto647N_zeroPosition, 5000, peakPosition_Atto647N-Atto647N_zeroPosition, 0)
    
    ax2.axvline(x=peakPosition_AF488, color='black', ls='--', linewidth=3)
    ax2.axvline(x=AF488_zeroPosition, color='black', ls='dotted', linewidth=3)
    arrowPosition_y = (ax2.get_ylim()[1]-ax2.get_ylim()[0]) * 0.8 + ax2.get_ylim()[0];
    #ax2.arrow(AF488_zeroPosition, arrowPosition_y, peakPosition_AF488-AF488_zeroPosition, 0)
    arrow = mpatches.FancyArrowPatch((AF488_zeroPosition, arrowPosition_y), (peakPosition_AF488-AF488_zeroPosition, 0))
    ax2.add_patch(arrow)
    
    
