#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 15 11:16:55 2023

@author: novakt
"""
import numpy as np
import matplotlib.pyplot as plt



dataFileName_Atto647N  = '/home/n/polarizationDistribution_Atto647N/polarizationDegreeData_Atto647N_67nm.csv'

dataFileName_AF488= '/home/n/polarizationDistribution_AF488/polarizationDegreeData_Atto647N_67nm.csv'


simulatedData_Atto647N  = np.loadtxt(dataFileName_Atto647N, delimiter=',')
polarizationDegree_Atto647N  = simulatedData_Atto647N[:, 3]

simulatedData_AF488  = np.loadtxt(dataFileName_AF488, delimiter=',')
polarizationDegree_AF488  = simulatedData_AF488[:, 3]



fig=plt.figure()
plt.rc('font', size=12)

AF488_color = 'blue'
binValues_AF488, binEdges_AF488, p = plt.hist(polarizationDegree_AF488, bins=40, range=(-1, 1), edgecolor = "black", alpha = 0.5, color = AF488_color, label="AF488")
Atto647N_color = 'red'
binValues_Atto647N, binEdges_Atto647N, p = plt.hist(polarizationDegree_Atto647N, bins=40, range=(-1,1), edgecolor = "black", alpha = 0.5, color = Atto647N_color, label="Atto647N")
plt.xlabel('polarization degree', color="black", size = 16)
plt.ylabel('localization frequency', color="black", size = 16)

plt.legend()