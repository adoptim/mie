import numpy as np
import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite

import Mie_calculation.auxiliary.Mie_summationAccessaries as \
    Mie_summationAccessaries
import Mie_calculation.Mie_coefficients as Mie_coefficients


emissionWavelength = 520 * 10 ** -9  # wavelength in m

sphereRadius = 1 / 2 * 140 * 10 ** -9

dipoleDistance = 18 * 10 ** -9

dipoleVector_theta = 90. * np.pi /180

dipoleVector_phi = 0. * np.pi /180

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(emissionWavelength),
    refractiveIndex.silver(emissionWavelength),
    compositionRatio
)
# refractiveIndex_sphere = refractiveIndex.gold(emissionWavelength)
refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
# refractiveIndex_medium = np.real(refractiveIndex.vacuum(emissionWavelength))

# position of the dipole
# the radial coordinate of the dipole position ( initializing here as 0 as it
# should be set in the loop over the sweep parameters:
dipolePosition_r = sphereRadius + dipoleDistance
# the polar angle of the dipole position:
dipolePosition_theta = 90. * np.pi/180
# the azimuthal angle of the dipole position:
dipolePosition_fi = 0. * np.pi/180
# the position of the dipole emitter in spherical coordiante system centered at the sphere center:
dipolePosition = np.array([
    [dipolePosition_r],
    [dipolePosition_theta],
    [dipolePosition_fi]])

idx = 0
dipoleStrength = 1.0
dipoleVector = dipoleStrength * np.array(
    [
        [np.cos(dipoleVector_theta)],
        [np.sin(dipoleVector_theta) * np.cos(dipoleVector_phi)],
        [np.sin(dipoleVector_theta) * np.sin(dipoleVector_phi)],
    ]
)




n_max = 1

(s_array, t_array, u_array, v_array) = \
    Mie_coefficients.expansionCoefficients_orientationArray(
        dipoleVector, dipolePosition, sphereRadius,
        emissionWavelength, refractiveIndex_sphere,
        refractiveIndex_medium, n_max)
            
# the summation index dependent constant:
D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)


idxOrientation = 0
# the summation index dependent expansion coefficients belonging to
# the given dipole orientation:
s_sequence = s_array[idxOrientation, :]
t_sequence = t_array[idxOrientation, :]
u_sequence = u_array[idxOrientation, :]
v_sequence = v_array[idxOrientation, :]

# the expansion coefficients, must be 1D numpy arrays:
D_s_sequence = D_sequence * s_sequence
D_t_sequence = D_sequence * t_sequence
D_u_sequence = D_sequence * u_sequence
D_v_sequence = D_sequence * v_sequence

s_angle = np.angle(D_s_sequence, deg=True)
s_abs = np.absolute(D_s_sequence)

t_angle = np.angle(D_t_sequence, deg=True)
t_abs = np.absolute(D_t_sequence)

u_angle = np.angle(D_u_sequence, deg=True)
u_abs = np.absolute(D_u_sequence)

v_angle = np.angle(D_v_sequence, deg=True)
v_abs = np.absolute(D_v_sequence)

plt.figure()
plt.plot(u_angle)
plt.plot(v_angle)

plt.figure()
plt.plot(u_abs)
plt.plot(v_abs)