# Convention for array dimensions

Conventions used for the different Numpy array dimensions. When calculating the polarization distribution, the calculation is vectorized for the dipole orientations and for the electric field sampling positions. In this matrix operations are used (variables in "arrays") instead of the loops (variables in "sequences"). `N` stands for the number of electric field sampling points and `M` stands for either the number of dipole orientations, or for the number of electric field sampling positions, depending on the context,`ν` stands for the number of summation indices (number of `n`, or `n` and `m`, or `n`, `m` and `σ` indices, depending on the context).

Dipole or position vector sequences:

    3 x 1

Dipole or position vector arrays:

    3 x N

Bessel, Hankel and Legendre function sequences:

    1 x ν

Bessel, Hankel and Legendre function arrays:

    M x ν

Expansion coefficient sequences:

    1 x ν

Expansion coefficient arrays:

    M x ν

D sequence:

    (ν, )

Spherical vector wave vectors:

    N x 3

Spherical vector wave arrays:

    N x ν x 3

Elelctric field vectors:

    N x 3
