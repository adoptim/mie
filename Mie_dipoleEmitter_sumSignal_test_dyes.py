import numpy as np
import math as math

import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

sphereRadius = 1 / 2 * 80 * 10 ** -9

# OTHER PARAMETERS

dipoleDistance = 18 * 10 ** -9

# number of different dipole positions:
dipolePosition_numberOfPoints = 100

# number of different dipole orientations:
tiltingNumber = dipolePosition_numberOfPoints

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
#collectionAngle = math.asin(1.49/1.52)
collectionAngle = np.pi



farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.sumSignal()

signalNormalizationBoolean = True

fluorophore = {}
fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92


# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
#refractiveIndex_sphere = refractiveIndex_medium

# slow rotation:
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()

dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_520, dipolePosition = Mie_dipoleEmitter_measurementSimulation.getDistribution_position(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean
)


fluorophore = {}
fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65


# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
#refractiveIndex_sphere = refractiveIndex_medium

# slow rotation:
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()

dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_670, dipolePosition = Mie_dipoleEmitter_measurementSimulation.getDistribution_position(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean
)


dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
refractiveIndex_sphere = refractiveIndex_medium
farfieldQuantity_controll, dipolePosition = Mie_dipoleEmitter_measurementSimulation.getDistribution_position(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean
)

angles = np.linspace(0, 90, tiltingNumber)

plt.xlabel('Azimuth angle [°]')
#plt.xlabel('Polar angle [°]')
plt.ylabel('Emission power')
plt.plot(angles, farfieldQuantity_520, label='520 nm emission')
plt.plot(angles, farfieldQuantity_670, label='670 nm emission')
plt.plot(angles, farfieldQuantity_controll, label='without nanoparticle')
plt.legend()
plt.show()

