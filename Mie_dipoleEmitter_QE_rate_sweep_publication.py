import numpy as np
import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite
from Mie_calculation.Mie_dipoleEmitter_rates import Mie_dipoleEmitter_rates

import Mie_calculation.Mie_dipoleEmitter_quantumEfficiency as Mie_dipoleEmitter_quantumEfficiency


def calculate_properties(emissionWavelength, QE_0, sphereRadius, refractiveIndex_sphere, refractiveIndex_medium, dipoleDistance, dipolePosition_sphericalComponent, dipoleVector_theta, dipoleVector_phi):
    
    absorptionRate = np.zeros(N)
    radiativeRate = np.zeros(N)
    totalRate = np.zeros(N)
    
    for idx in range(0, N):
    
        # update the diple radial position coordinate:
        dipolePosition_r = sphereRadius[idx] + dipoleDistance[idx]
        dipolePosition_sphericalComponents[0] = dipolePosition_r
    
        # strength and orientation of the dipole (components of the dipole vector in
        # the spherical coordiante system at the dipole position, the "r", "theta"
        # and "fi" unit vectors, respectively):
        dipoleStrength = 1.0
        dipoleVector = dipoleStrength * np.array(
            [
                [np.cos(dipoleVector_theta[idx])],
                [np.sin(dipoleVector_theta[idx]) * np.cos(dipoleVector_phi[idx])],
                [np.sin(dipoleVector_theta[idx]) * np.sin(dipoleVector_phi[idx])],
            ]
        )
        
        # alternatively:
        # perpendicular to the nanosphere surface:
        # dipoleVector = np.array([[1.0], [0.0], [0.0]])
        # parallel to the nanosphere surface:
        # dipoleVector=np.array([[0.], [1.], [0.]])
        # dipoleVector=np.array([[0.], [0.], [1.]])
    
        # calculate the modified dipole absorption and scattering rates:
        [W_a, W_s, W_t] = Mie_dipoleEmitter_rates(
           emissionWavelength[idx],
           sphereRadius[idx],
           dipolePosition_sphericalComponents,
           dipoleVector,
           refractiveIndex_sphere[idx],
           refractiveIndex_medium[idx],
           n_max
        )
    
        absorptionRate[idx] = W_a
        radiativeRate[idx] = W_s
        totalRate[idx] = W_t
    
    
    QE = Mie_dipoleEmitter_quantumEfficiency.getQE_using_absorptionRate(QE_0, radiativeRate, absorptionRate)
    
    return absorptionRate, radiativeRate, totalRate, QE



fluorophore_520 = {}
fluorophore_520['emissionWavelength'] = 520 * 10 ** -9
fluorophore_520['quantumYield'] = 0.92
fluorophore_670 = {}
fluorophore_670['emissionWavelength'] = 670 * 10 ** -9
fluorophore_670['quantumYield'] = 0.65

# SWEEP PARAMETERS

n_max = 10

N = 50

emissionWavelength_520 = np.linspace(fluorophore_520['emissionWavelength'], fluorophore_520['emissionWavelength'], num=N)  # m
emissionWavelength_670 = np.linspace(fluorophore_670['emissionWavelength'], fluorophore_670['emissionWavelength'], num=N)  # m

sphereRadius = 1 / 2 * np.linspace(80, 80, num=N) * 10 ** -9

dipoleDistance = np.linspace(18, 18, num=N) * 10 ** -9

dipoleVector_theta = np.linspace(0., 90.) * np.pi /180
dipoleVector_phi = np.linspace(0., 0.) * np.pi /180

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere_520 = refractiveIndex_composite(
    refractiveIndex.gold(emissionWavelength_520),
    refractiveIndex.silver(emissionWavelength_520),
    compositionRatio
)
refractiveIndex_medium_520 = np.real(refractiveIndex.water(emissionWavelength_520))

refractiveIndex_sphere_670 = refractiveIndex_composite(
    refractiveIndex.gold(emissionWavelength_670),
    refractiveIndex.silver(emissionWavelength_670),
    compositionRatio
)
refractiveIndex_medium_670 = np.real(refractiveIndex.water(emissionWavelength_670))

# position of the dipole
# the radial coordinate of the dipole position ( initializing here as 0 as it
# should be set in the loop over the sweep parameters:
dipolePosition_r = 0.
# the polar angle of the dipole position:
dipolePosition_theta = 90. * np.pi/180
# the azimuthal angle of the dipole position:
dipolePosition_fi = 0. * np.pi/180
# the position of the dipole emitter in spherical coordiante system centered at the sphere center:
dipolePosition_sphericalComponents = np.array([
    [dipolePosition_r],
    [dipolePosition_theta],
    [dipolePosition_fi]])

# SWEEP OVER THE PARAMETERS

[absorptionRate_520, radiativeRate_520, totalRate_520, QE_520] = calculate_properties(emissionWavelength_520, fluorophore_520['quantumYield'], sphereRadius, refractiveIndex_sphere_520, refractiveIndex_medium_520, dipoleDistance, dipolePosition_sphericalComponents, dipoleVector_theta, dipoleVector_phi)
[absorptionRate_670, radiativeRate_670, totalRate_670, QE_670] = calculate_properties(emissionWavelength_670, fluorophore_670['quantumYield'], sphereRadius, refractiveIndex_sphere_670, refractiveIndex_medium_670, dipoleDistance, dipolePosition_sphericalComponents, dipoleVector_theta, dipoleVector_phi)

# VISUALIZATION
    
def cumulativeTodegress(x):
    return np.arccos(1-x/1.0)*180/np.pi

def degreesToCumulative(x):
    return 1.0 * (1 - np.cos(x/180*np.pi))

# The "x" quantity in case of theta component of the dipole orientation sweep:
x_degrees = dipoleVector_theta * 180 / np.pi
xlabel_degrees = 'polar angle [°]'
# The "x" quantity in case of theta component of the dipole orientation sweep:
x_cumulative = degreesToCumulative(x_degrees)
xlabel_cumulative = 'polar angle cumulative probability'

fig, ax1 = plt.subplots()
ax1.plot(x_cumulative, QE_520, label = 'QE, AF488', linestyle='dotted', linewidth=2, color='blue')
ax1.plot(x_cumulative, QE_670, label = 'QE, Atto647N', linestyle='dotted', linewidth=2, color='red')
ax1.set_xlim(left = 0.0, right=1.0)
ax1.set_xlabel(xlabel_cumulative, fontsize=12)
#ax1.legend()

secax = ax1.secondary_xaxis('top', functions=(cumulativeTodegress, degreesToCumulative))
secax.set_xlabel(xlabel_degrees, fontsize=12)
secax.xaxis.set_ticks(ticks = np.array([0, 15, 30, 45, 60, 75, 90]))

ax1.set_ylabel("quantum efficiency", fontsize=12)

ax2 = ax1.twinx()


#plt.plot(x_cumulative, radiativeRate_520, label = 'rate, AF488', dashes=[6, 3], linewidth=2, color='blue')
#plt.plot(x_cumulative, radiativeRate_670, label = 'rate, Atto647N', dashes=[6, 3], linewidth=2, color='red')
plt.plot(x_cumulative, radiativeRate_520, label = 'rate, AF488', linestyle='dashed', linewidth=2, color='blue')
plt.plot(x_cumulative, radiativeRate_670, label = 'rate, Atto647N', linestyle='dashed', linewidth=2, color='red')
ax2.set_ylabel("radiative rate enhancement", fontsize=12)

lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, loc=0)

#plt.ylim(0, 1)
plt.show()