import numpy as np
from scipy.interpolate import griddata

import Mie_calculation.Mie_dipoleEmitter_electricField as Mie_dipoleEmitter_electricField

def polarizedPSF(
    samplingPoints,
    electricField_aperture,
    collectionAngle, 
    samplingDistance,
    PSF_size
):

    scaleFactor = 4
        
    samplingPoints_aperture = np.transpose(Mie_dipoleEmitter_electricField.apertureSamplingCoordinates(
            samplingPoints))

    apertureRadius = np.sin(collectionAngle)*samplingDistance

    grid_x, grid_y = np.mgrid[-scaleFactor*apertureRadius:scaleFactor*apertureRadius:scaleFactor*PSF_size*1j, -scaleFactor*apertureRadius:scaleFactor*apertureRadius:scaleFactor*PSF_size*1j]

    electricField_collimated = \
        Mie_dipoleEmitter_electricField.collimation(
            samplingPoints, electricField_aperture)

    electricField_collimated_x = electricField_collimated[:, 0]
    electricField_collimated_y = electricField_collimated[:, 1]

    # https://docs.scipy.org/doc/scipy/reference/tutorial/interpolate.html#multivariate-data-interpolation-griddata
    electricField_grid_x = griddata(samplingPoints_aperture, electricField_collimated_x, (grid_x, grid_y), method='cubic')
    electricField_grid_y = griddata(samplingPoints_aperture, electricField_collimated_y, (grid_x, grid_y), method='cubic')

    axialDistance = np.sqrt(grid_x**2 + grid_y**2)
    appertureBool = apertureRadius > axialDistance
    electricField_grid_x[~appertureBool] = 0
    electricField_grid_x[np.isnan(electricField_grid_x)] = 0
    electricField_grid_y[~appertureBool] = 0
    electricField_grid_y[np.isnan(electricField_grid_y)] = 0

    PSF_electricField_x = np.fft.fftshift(np.fft.fft2(electricField_grid_x))
    PSF_electricField_y = np.fft.fftshift(np.fft.fft2(electricField_grid_y))

    #  TODO shape fft output

    PSF_x_full = np.real(PSF_electricField_x*np.conjugate(PSF_electricField_x))
    PSF_y_full = np.real(PSF_electricField_y*np.conjugate(PSF_electricField_y))

    lowerIndex_x = round((grid_x.shape[0]-PSF_size)/2)
    lowerIndex_y = round((grid_x.shape[1]-PSF_size)/2)
    PSF_x = PSF_x_full[lowerIndex_x:lowerIndex_x+PSF_size, lowerIndex_y:lowerIndex_y+PSF_size]
    PSF_y = PSF_y_full[lowerIndex_x:lowerIndex_x+PSF_size, lowerIndex_y:lowerIndex_y+PSF_size]

    # PSF_x = np.real(electricField_grid_x*np.conjugate(electricField_grid_x))
    # PSF_y = np.real(electricField_grid_y*np.conjugate(electricField_grid_y))

    PSF = np.array([PSF_x, PSF_y])

    return PSF
