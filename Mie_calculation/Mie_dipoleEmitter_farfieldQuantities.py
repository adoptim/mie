import numpy as np

import Mie_calculation.Mie_dipoleEmitter_electricField as \
    Mie_dipoleEmitter_electricField
import Mie_calculation.Mie_dipoleEmitter_PSF as Mie_dipoleEmitter_PSF

class farfieldQuantity:

    class polarizedPSF:
        # This class if for getting the polarized far field PSF-s for 

        def __init__(self, PSF_size):
            self.PSF_size = PSF_size

        def initializeArray(self, positionNumber, orientationNumber):
            PSF_array = np.zeros([positionNumber, orientationNumber, 2, self.PSF_size, self.PSF_size])
            
            return PSF_array

        def getQuantityFromFarfield(self, samplingPoints, electricFarfield, collectionAngle, samplingDistance, refractiveIndex_medium):

            
            PSF = Mie_dipoleEmitter_PSF.polarizedPSF(
                samplingPoints, electricFarfield,
                collectionAngle, samplingDistance, self.PSF_size)
                
            return PSF
        
        def averageOrientations(self, PSF_array, freeDipoleSignal):
            
            PSF_vector = np.mean(PSF_array, axis=1)
            
            # all True array since there was no filtering:
            filteringBoolean = np.full(PSF_vector.shape[0], True)
            
            return PSF_vector, filteringBoolean


    class polarizationChannelSignals:
        
        def __init__(self):
            pass

        def initializeArray(self, positionNumber, orientationNumber):
            
            polarizationSignal_array = np.zeros([positionNumber, orientationNumber, 3])
            
            return polarizationSignal_array

        def getQuantityFromFarfield(self, samplingPoints, electricField, collectionAngle, samplingDistance, refractiveIndex_medium):
            
            polarizationChannelSignals = Mie_dipoleEmitter_electricField.polarizationChannelSignals(
                samplingPoints, electricField,
                collectionAngle, refractiveIndex_medium)
                
            return polarizationChannelSignals
        
        def averageOrientations(self, polarizationChannelSignals_array, freeDipoleSignal):
            
            # take the mean of the orientations:
            polarizationChannelSignals = np.mean(polarizationChannelSignals_array, axis=1)
            
            # normalize the signal strength (~photon count):
            polarizationChannelSignals = polarizationChannelSignals / freeDipoleSignal / 2
            
            # all True array since there was no filtering:
            filteringBoolean = np.full(polarizationChannelSignals.shape[0], True)
            
            return polarizationChannelSignals, filteringBoolean

    class polarizationDegree_orientation:
        
        def __init__(self):
            pass

        def initializeArray(self, positionNumber, orientationNumber):
            
            polarizationSignal_array = np.zeros([positionNumber, orientationNumber, 3])
            
            return polarizationSignal_array

        def getQuantityFromFarfield(self, samplingPoints, electricField, collectionAngle, samplingDistance, refractiveIndex_medium):
            
            polarizationChannelSignals = Mie_dipoleEmitter_electricField.polarizationChannelSignals(
                samplingPoints, electricField,
                collectionAngle, refractiveIndex_medium)
                
            return polarizationChannelSignals
        
        def averageOrientations(self, polarizationChannelSignals_array, freeDipoleSignal):
            
            
            # polarization degre calculation:
            polarizationValues = (
                (polarizationChannelSignals_array[:, :, 0] - polarizationChannelSignals_array[:, :, 1])
                / (polarizationChannelSignals_array[:, :, 0] + polarizationChannelSignals_array[:, :, 1]))
            
            filteringBoolean = np.full(polarizationValues.shape[0], True)
            
            return polarizationValues, filteringBoolean


    class polarizationDegree:
        
        def __init__(self, intensitiyFilteringRatio):
            self.intensitiyFilteringRatio = intensitiyFilteringRatio

        def initializeArray(self, positionNumber, orientationNumber):
            
            polarizationSignal_array = np.zeros([positionNumber, orientationNumber, 3])
            
            return polarizationSignal_array

        def getQuantityFromFarfield(self, samplingPoints, electricField, collectionAngle, samplingDistance, refractiveIndex_medium):
            
            polarizationChannelSignals = Mie_dipoleEmitter_electricField.polarizationChannelSignals(
                samplingPoints, electricField,
                collectionAngle, refractiveIndex_medium)
                
            return polarizationChannelSignals
        
        def averageOrientations(self, polarizationChannelSignals_array, freeDipoleSignal):
            
            # take the mean of the orientations:
            polarizationChannelSignals = np.mean(polarizationChannelSignals_array, axis=1)
            
            # normalize the signal strength (~photon count):
            polarizationChannelSignals = polarizationChannelSignals / freeDipoleSignal / 2
            
            filteringBoolean = np.full(polarizationChannelSignals.shape[0], True, dtype=bool)
            if self.intensitiyFilteringRatio != 0:
                boolean_channelX = polarizationChannelSignals[:, 0] >= self.intensitiyFilteringRatio
                boolean_channelY = polarizationChannelSignals[:, 1] >= self.intensitiyFilteringRatio
                
                filteringBoolean = boolean_channelX & boolean_channelY
                
            polarizationChannelSignals = polarizationChannelSignals[filteringBoolean, :]
            
            # polarization degre calculation:
            polarizationValues = (
                (polarizationChannelSignals[:, 0] - polarizationChannelSignals[:, 1])
                / (polarizationChannelSignals[:, 0] + polarizationChannelSignals[:, 1]))

            return polarizationValues, filteringBoolean
        
    class Stokes_parameters:
        
        def __init__(self):
            pass
    
        def initializeArray(self, positionNumber, orientationNumber):
            
            StokesParameters_array = np.zeros([positionNumber, orientationNumber, 4])
            
            return StokesParameters_array
        
        def getQuantityFromFarfield(self, samplingPoints, electricField, collectionAngle, samplingDistance, refractiveIndex_medium):
            
            StokesParameters = Mie_dipoleEmitter_electricField.unnormalized_Stokes(
                samplingPoints, electricField,
                collectionAngle, refractiveIndex_medium)
            
            return StokesParameters
            
        def averageOrientations(self, StokesParameters_array, freeDipoleSignal):
            StokesParameters = np.mean(StokesParameters_array, axis=1)
            print("DEBUG: shape", StokesParameters.shape)
            
            # all True array since there was no filtering:
            filteringBoolean = np.full(StokesParameters.shape[0], True)
            
            return StokesParameters, filteringBoolean
    
    class sumSignal:
        
        def __init__(self):
            pass

        def initializeArray(self, positionNumber, orientationNumber):
            
            sumSignal_array = np.zeros([positionNumber, orientationNumber, 1])
            
            return sumSignal_array

        def getQuantityFromFarfield(self, samplingPoints, electricField, collectionAngle, samplingDistance, refractiveIndex_medium):
            
            polarizationChannelSignals = Mie_dipoleEmitter_electricField.polarizationChannelSignals(
                samplingPoints, electricField,
                collectionAngle, refractiveIndex_medium)
                
            sumSignal = np.sum(polarizationChannelSignals)
            
            return sumSignal
        
        def averageOrientations(self, sumSignal_array, freeDipoleSignal):
            
            # sum the polarization channels and the take the mean of the orientations
            polarizationChannelSignals_sum = np.mean(np.sum(sumSignal_array, axis=2), axis=1)
            
            polarizationChannelSignals_sum = polarizationChannelSignals_sum / freeDipoleSignal
            
            # all True array since there was no filtering:
            filteringBoolean = np.full(polarizationChannelSignals_sum.shape, True)
            
            return polarizationChannelSignals_sum, filteringBoolean
