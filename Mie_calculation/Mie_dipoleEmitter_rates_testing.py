import numpy as np
import math as math
from scipy import special

import Mie_calculation.auxiliary.Mie_physicalConstants as Mie_physicalConstants

import Mie_calculation.Mie_coefficients as Mie_coefficients

import Mie_calculation.auxiliary.Mie_summationAccessaries as Mie_summationAccessaries
import Mie_calculation.auxiliary.sphericalFunctions as sphericalFunctions

from Mie_calculation.Mie_absorptionIntegrals import absorptionIntegrals

# TODO: j_n, beta_n, ....

def perpendicular(
    emissionWavelength,
    R,
    dipolePosition_sphericalComponents,
    dipoleVector,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    n_max
):
        # When the dipole is perpendicular to the nanosphere surface

    # CONSTANTS

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    sigma_conductivity = Mie_physicalConstants.conductivity(c_0, k_0, epsilon_0, refractiveIndex_sphere)

    # polar and azimuth angles of the dipole position vector:
    theta = dipolePosition_sphericalComponents[1, :]
    fi = dipolePosition_sphericalComponents[2, :]

    # dimenionless sizes:
    r = dipolePosition_sphericalComponents[0, :]
    kr = k_medium * r
    kR = k_medium * R

    # RATE CALCULATION

    totalRate = 1
    scatteringRate = 0
    absorptionRate = 0
    absorptionRate_test = 0

    j_sequence, j_der_sequence = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_sequence, h_der_sequence = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_sequence, j_der_sequence)
    
    int_absorbed_sequence = absorptionIntegrals(n_max, R, k_sphere)
    
    indexCount_n = 0
    linearIndex_n = 0
    for n in range(1, n_max + 1):
        indexCount_n += 1
        linearIndex_n = indexCount_n-1

        # get the Mie coefficients
        [a_n, b_n, alpha_n, beta_n] = Mie_coefficients.Mie_coefficients(kR, refr_rel, n)
        # for testing, get them from an "external" code
        # [a_L, b_L]=bhmie(kR, refr_rel, n)

        # sperical Bessel function of the first kind
        #j_n = special.spherical_jn(n, kr)

        # sperical Bessel function of the second kind
        #y_n = special.spherical_yn(n, kr)

        # spherical Hankel function of the first
        #h_n = ( j_n + 1j * y_n)
        
        j_n = j_sequence[0, linearIndex_n]
        h_n = h_sequence[0, linearIndex_n]
        
        int_absorbed_n__1 = int_absorbed_sequence[n-1]
        int_absorbed_n  = int_absorbed_sequence[n]
        int_absorbed_n_1 = int_absorbed_sequence[n+1]
        
        a = totalRate - scatteringRate

        # the radiated power of the dipole-sphere system
        # Merstens (2007), (A2)
        scatteringRate_n = \
            3 / 2 * (2 * n + 1) * n * (n + 1) * \
            np.abs((j_n + b_n * h_n) / kr) ** 2
        scatteringRate += scatteringRate_n

        # Merstens(2007), (A1)
        totalRate_n = \
            3 / 2 * (2 * n + 1) * n * (n + 1) * \
            np.real( b_n * (h_n / kr) ** 2 )
        totalRate += totalRate_n
        print(n, totalRate, totalRate_n)

        absorptionRate_test_n = \
        3 / 2 * np.sqrt(mu_0/epsilon_0) *sigma_conductivity * \
        ( n * ( n + 1 ) * np.abs( beta_n * h_n)**2 / r**2 * \
        ((n+1) * int_absorbed_n__1 + n * int_absorbed_n_1) )
        absorptionRate_test += absorptionRate_test_n

        absorptionRate_n = totalRate_n - scatteringRate_n
        #print(n, absorptionRate_n, absorptionRate_test_n)
    
        #print(n, int_absorbed_n__1, int_absorbed_n, int_absorbed_n_1)

    # the absorbed power
    absorptionRate = totalRate - scatteringRate

    #print("a", absorptionRate_test/absorptionRate)

    return absorptionRate, scatteringRate, totalRate

def perpendicular2(
    emissionWavelength,
    R,
    dipolePosition_sphericalComponents,
    dipoleVector,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    n_max
):
    # This function calculates the scattering/absorption rate  of a
    # dipole placed near a sphere (dielectric or conductive) compared
    # to the free space dipole radiation rate

    # CONSTANTS

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    sigma_conductivity = Mie_physicalConstants.conductivity(c_0, k_0, epsilon_0, refractiveIndex_sphere)

    # polar and azimuth angles of the dipole position vector:
    theta = dipolePosition_sphericalComponents[1, :]
    fi = dipolePosition_sphericalComponents[2, :]

    # dimenionless sizes:
    r = dipolePosition_sphericalComponents[0, :]
    kr = k_medium * r
    kR = k_medium * R

    # dipole emission power in free space:
    W_R0 = freeSpaceRadiationRate(k_medium, c_0, mu_0, dipoleVector)

    # RATE CALCULATION

    j_vect, j_der_vect = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_vect, h_der_vect = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_vect, j_der_vect)

    # sequence of associated Legendre polynomials and their derivates:
    [P_sequence, P_m_per_sinTheta_sequence, P_der_sequence] = sphericalFunctions.associatedLegendrePolynomials(n_max, theta)

    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    int_absorbed_sequence = absorptionIntegrals(n_max, R, k_sphere)

    totalPower = 0
    radiationPower = 0
    absorptionPower = 0

    indexCount_n = 0
    linearIndex_n = 0
    indexCount_n_m = 0
    linearIndex_n_m = 0
    indexCount_n_m_sigma = 0
    linearIndex_n_m_sigma = 0
    for n in range(1, n_max + 1):
        indexCount_n += 1
        linearIndex_n = indexCount_n-1

        # get the Mie coefficients
        [a_n, b_n, alpha_n, beta_n] = Mie_coefficients.Mie_coefficients(kR, refr_rel, n)

        
        j_n = j_vect[:, linearIndex_n]
        j_n_der = j_der_vect[:, linearIndex_n]
        h_n = h_vect[:, linearIndex_n]
        h_n_der = h_der_vect[:, linearIndex_n]
        
        """
        # spherical Bessel function of the first kind, order n, n-1 and the
        # derivate of the order n:
        j_n = special.spherical_jn(n, kr)
        j_n__1 = special.spherical_jn(n - 1, kr)
        j_n_der = j_n__1 - (n + 1) / kr * j_n

        # spherical Bessel function of the second kind, order n, n-1 and the
        # derivate of the order n
        y_n = special.spherical_yn(n, kr)
        y_n__1 = special.spherical_yn(n - 1, kr)
        y_n_der = y_n__1 - (n + 1) / kr * y_n

        # spherical Hankel function of the first kind, order n, n-1 and the
        # derivate of the order n:
        h_n = ( j_n + 1j * y_n)
        h_n__1 = j_n__1 + 1j * y_n__1
        h_n_der = j_n_der + 1j * y_n_der
        """

        int_absorbed_n__1 = int_absorbed_sequence[linearIndex_n-1]
        int_absorbed_n  = int_absorbed_sequence[linearIndex_n]
        int_absorbed_n_1 = int_absorbed_sequence[linearIndex_n+1]

        for m in range(0, n + 1):
            indexCount_n_m += 1
            linearIndex_n_m = indexCount_n_m-1

            # getting the required associated Legendre polynomials:
            P_m_n = P_sequence[:, linearIndex_n_m]
            P_m_per_sinTheta_m_n = P_m_per_sinTheta_sequence[:, linearIndex_n_m]
            P_m_n_der = P_der_sequence[:, linearIndex_n_m]

            for sigma in range(1, 3):
                indexCount_n_m_sigma += 1
                linearIndex_n_m_sigma = indexCount_n_m_sigma-1

                D_sigma_m_n = D_sequence[linearIndex_n_m_sigma]

                """# testing, perpendicular:
                s_sigma_m_n, t_sigma_m_n, p_sigma_m_n, q_sigma_m_n = \
                Mie_coefficients.expansionCoefficients_dipoleField_testing_perpendicular(
                k_medium, epsilon_0, n, m, sigma, kr, j_n, h_n, dipoleVector)"""

                s_sigma_m_n, t_sigma_m_n, p_sigma_m_n, q_sigma_m_n = \
                    Mie_coefficients.expansionCoefficients_dipoleField(
                        k_medium, epsilon_0, n, m, sigma, kr, theta, fi,
                        j_n, j_n_der, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der,
                        dipoleVector)

                # expansion coefficients of the scattered field:
                u_sigma_m_n = a_n * p_sigma_m_n
                v_sigma_m_n = b_n * q_sigma_m_n
                # expansion coefficients of the transmitted field:
                f_sigma_m_n = alpha_n * p_sigma_m_n
                g_sigma_m_n = beta_n * q_sigma_m_n

                radiationPower = radiationPower + (
                        np.sqrt(epsilon_0 / mu_0) * np.pi / k_medium ** 2
                    ) * 1 / 2 * D_sigma_m_n * (
                        np.abs(s_sigma_m_n + u_sigma_m_n) ** 2
                        + np.abs(t_sigma_m_n + v_sigma_m_n) ** 2
                    )

                """radiationPower = radiationPower + \
                    np.sqrt(epsilon_0 / mu_0) * np.pi / k_medium ** 2 * \
                    1 / 2 * D_sigma_m_n * \
                    ( np.abs(s_sigma_m_n + u_sigma_m_n) ** 2
                    + np.abs(t_sigma_m_n + v_sigma_m_n) ** 2 )"""

                # TODO: check the sign
                absorptionPower = absorptionPower + \
                    np.pi * sigma_conductivity / 8 * 4 * D_sigma_m_n * \
                    (
                    np.abs(f_sigma_m_n) ** 2 * int_absorbed_n
                      + 1 / (2 * n + 1) * np.abs(g_sigma_m_n) ** 2
                      * ((n + 1) * int_absorbed_n__1 + n * int_absorbed_n_1)
                    )


    totalPower = absorptionPower + radiationPower

    # the relative rates:
    absorptionRate = absorptionPower / W_R0
    scatteringRate = radiationPower / W_R0
    totalRate = totalPower / W_R0

    # return absorption, scattering and total (Purcell factor) rates relative
    # to the free space emission rate:
    return absorptionRate, scatteringRate, totalRate



def parallel(
    emissionWavelength,
    R,
    dipolePosition_sphericalComponents,
    dipoleVector,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    n_max
):
    # When the dipole is parallel to the nanosphere surface
    
    # CONSTANTS

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    sigma_conductivity = Mie_physicalConstants.conductivity(c_0, k_0, epsilon_0, refractiveIndex_sphere)

    # polar and azimuth angles of the dipole position vector:
    theta = dipolePosition_sphericalComponents[1, :]
    fi = dipolePosition_sphericalComponents[2, :]

    # dimenionless sizes:
    r = dipolePosition_sphericalComponents[0, :]
    kr = k_medium * r
    kR = k_medium * R

    # RATE CALCULATION

    totalRate = 1
    radiativeRate = 0
    absorptionRate = 0
    absorptionRate_test = 0

    j_sequence, j_der_sequence = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_sequence, h_der_sequence = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_sequence, j_der_sequence)

    int_absorbed_sequence = absorptionIntegrals(n_max, R, k_sphere)

    indexCount_n = 0
    linearIndex_n = 0
    for n in range(1, n_max + 1):
        indexCount_n += 1
        linearIndex_n = indexCount_n-1

        # get the Mie coefficients
        [a_n, b_n, alpha_n, beta_n] = Mie_coefficients.Mie_coefficients(kR, refr_rel, n)
        # for testing, get them from an "external" code
        # [a_L, b_L]=bhmie(kR, refr_rel, n)

        """
        # sperical Bessel function of the first kind
        j_n = special.spherical_jn(n, kr)
        j_n__1 = special.spherical_jn(n - 1, kr)
        j_n_der = j_n__1 - (n + 1) / kr * j_n

        # sperical Bessel function of the second kind
        y_n = special.spherical_yn(n, kr)
        y_n__1 = special.spherical_yn(n - 1, kr)
        y_n_der = y_n__1 - (n + 1) / kr * y_n

        # spherical Hankel function of the first
        # TODO first or second kind?
        h_n = ( j_n + 1j * y_n)
        # really, the positive signs resulted negative absorption results
        h_n__1 = j_n__1 + 1j * y_n__1
        h_n_der = j_n_der + 1j * y_n_der
        """

        j_n = j_sequence[0, linearIndex_n]
        j_n_der = j_der_sequence[0, linearIndex_n]

        h_n = h_sequence[0, linearIndex_n]
        h_n_der = h_der_sequence[0, linearIndex_n]

        int_absorbed_n__1 = int_absorbed_sequence[linearIndex_n-1]
        int_absorbed_n = int_absorbed_sequence[linearIndex_n]
        int_absorbed_n_1 = int_absorbed_sequence[linearIndex_n+1]

        # derivatives of Riccati-Bessel functions
        # denoted as Greek "zeta" in Merstens(2007) and as Greek "xi" in
        # Wikipedia ("zeta" denotes something else in the Wikipedia)
        zeta_n_der = h_n + kr * h_n_der
        # denoted as Greek "psi" Merstens(2007) and as "S" in Wikipedia
        psi_n_der = j_n + kr * j_n_der

        radiativeRate = radiativeRate + \
            3 / 4 * (2 * n + 1) * \
            ( np.abs(j_n + a_n * h_n) ** 2 + np.abs( ( psi_n_der + b_n * zeta_n_der ) / kr) ** 2 )

        totalRate = totalRate + \
            3 / 2 * np.real( (n + 1 / 2) * \
            (b_n * (zeta_n_der / kr) ** 2 + a_n * (h_n) ** 2 )
        )

        # the absorbed power
        absorptionRate = totalRate - radiativeRate

        absorptionRate_test = absorptionRate_test + \
        3 / 4 * np.sqrt(mu_0/epsilon_0) * sigma_conductivity * k_medium**2 * (2 * n + 1 ) * \
        ( np.abs( alpha_n * h_n)**2 * int_absorbed_n + np.abs(beta_n/kr*(kr*h_n))**2 * 1 / (2 * n + 1) * \
        ( (n+1) * int_absorbed_n__1 + n * int_absorbed_n_1) )

    print(absorptionRate_test/absorptionRate)

    return absorptionRate, radiativeRate, totalRate


def parallel2(
    emissionWavelength,
    R,
    dipolePosition_sphericalComponents,
    dipoleVector,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    n_max
):
    # This function calculates the scattering/absorption rate  of a
    # dipole placed near a sphere (dielectric or conductive) compared
    # to the free space dipole radiation rate

    # CONSTANTS

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    sigma_conductivity = Mie_physicalConstants.conductivity(c_0, k_0, epsilon_0, refractiveIndex_sphere)

    # polar and azimuth angles of the dipole position vector:
    theta = dipolePosition_sphericalComponents[1, :]
    fi = dipolePosition_sphericalComponents[2, :]

    # dimenionless sizes:
    r = dipolePosition_sphericalComponents[0, :]
    kr = k_medium * r
    kR = k_medium * R

    # dipole emission power in free space:
    W_R0 = freeSpaceRadiationRate(k_medium, c_0, mu_0, dipoleVector)

    # RATE CALCULATION

    j_vect, j_der_vect = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_vect, h_der_vect = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_vect, j_der_vect)

    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    int_absorbed_sequence = absorptionIntegrals(n_max, R, k_sphere)

    totalPower = 0
    radiationPower = 0
    absorptionPower = 0

    indexCount_n = 0
    linearIndex_n = 0
    indexCount_n_m = 0
    linearIndex_n_m = 0
    indexCount_n_m_sigma = 0
    linearIndex_n_m_sigma = 0
    for n in range(1, n_max + 1):
        indexCount_n += 1
        linearIndex_n = indexCount_n-1

        # get the Mie coefficients
        [a_n, b_n, alpha_n, beta_n] = Mie_coefficients.Mie_coefficients(kR, refr_rel, n)

        j_n = j_vect[:, linearIndex_n]
        j_n_der = j_der_vect[:, linearIndex_n]
        h_n = h_vect[:, linearIndex_n]
        h_n_der = h_der_vect[:, linearIndex_n]

        # derivatives of Riccati-Bessel functions
        # denoted as Greek "zeta" in Merstens(2007) and as Greek "xi" in
        # Wikipedia ("zeta" denotes something else in the Wikipedia)
        zeta_n_der = h_n + kr * h_n_der
        # denoted as Greek "psi" Merstens(2007) and as "S" in Wikipedia
        psi_n_der = j_n + kr * j_n_der
        
        int_absorbed = int_absorbed_sequence[n-1]
        int_absorbed__1 = int_absorbed_sequence[n]
        int_absorbed_1 = int_absorbed_sequence[n+1]

        for m in range(0, n + 1):
            indexCount_n_m += 1
            linearIndex_n_m = indexCount_n_m-1

            for sigma in range(1, 3):
                indexCount_n_m_sigma += 1
                linearIndex_n_m_sigma = indexCount_n_m_sigma-1
                
                D_sigma_m_n = D_sequence[linearIndex_n_m_sigma]

                # testing, parallel:
                s_sigma_m_n, t_sigma_m_n, p_sigma_m_n, q_sigma_m_n = \
                Mie_coefficients.expansionCoefficients_dipoleField_testing_parallel(
                k_medium, epsilon_0, n, m, sigma, kr, j_n, j_n_der, h_n, h_n_der, dipoleVector)

                # expansion coefficients of the scattered field:
                u_sigma_m_n = a_n * p_sigma_m_n
                v_sigma_m_n = b_n * q_sigma_m_n
                # expansion coefficients of the transmitted field:
                f_sigma_m_n = alpha_n * p_sigma_m_n
                g_sigma_m_n = beta_n * q_sigma_m_n

                radiationPower = radiationPower + (
                    np.sqrt(epsilon_0 / mu_0) * np.pi / k_medium ** 2
                ) * 1 / 2 * D_sigma_m_n * (
                    np.abs(s_sigma_m_n + u_sigma_m_n) ** 2
                    + np.abs(t_sigma_m_n + v_sigma_m_n) ** 2
                )

                # TODO: check the sign
                absorptionPower =\
                    (absorptionPower
                     + np.pi * sigma_conductivity / 8 * 4 * D_sigma_m_n *
                     (np.abs(f_sigma_m_n) ** 2 * int_absorbed
                      + 1 / (2 * n + 1) * np.abs(g_sigma_m_n) ** 2
                      * ((n + 1) * int_absorbed__1 + n * int_absorbed_1)
                      )
                     )

    totalPower = absorptionPower + radiationPower

    # the relative rates:
    absorptionRate = absorptionPower / W_R0
    scatteringRate = radiationPower / W_R0
    totalRate = totalPower / W_R0

    # return absorption, scattering and total (Purcell factor) rates relative
    # to the free space emission rate:
    return absorptionRate, scatteringRate, totalRate


def vectorized(
    emissionWavelength,
    R,
    dipolePosition_sphericalComponents,
    dipoleVector,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    n_max
):

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    sigma_conductivity = Mie_physicalConstants.conductivity(c_0, k_0, epsilon_0, refractiveIndex_sphere)

    # dipole emission power in free space:
    W_R0 = freeSpaceRadiationRate(k_medium, c_0, mu_0, dipoleVector)

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    (s_array, t_array, u_array, v_array) = Mie_coefficients.expansionCoefficients_orientationArray(dipoleVector, dipolePosition_sphericalComponents, R, emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium, n_max)

    radiationPower = (
        np.sqrt(epsilon_0 / mu_0) * np.pi / k_medium ** 2
    ) * 1 / 2 * np.sum(
        D_sequence*np.abs((s_array + u_array)) ** 2
        + D_sequence*np.abs((t_array + v_array)) ** 2, axis=1)

    radiationPower_test = 0

    indexCount_n = 0
    linearIndex_n = 0
    indexCount_n_m = 0
    linearIndex_n_m = 0
    indexCount_n_m_sigma = 0
    linearIndex_n_m_sigma = 0
    for n in range(1, n_max + 1):
        indexCount_n += 1
        linearIndex_n = indexCount_n-1

        for m in range(0, n + 1):
            indexCount_n_m += 1
            linearIndex_n_m = indexCount_n_m-1

            
            for sigma in range(1, 3):
                indexCount_n_m_sigma += 1
                linearIndex_n_m_sigma = indexCount_n_m_sigma-1
            
                D_sigma_m_n = D_sequence[linearIndex_n_m_sigma]

                s_sigma_m_n = s_array[0, linearIndex_n_m_sigma]
                t_sigma_m_n = t_array[0, linearIndex_n_m_sigma]
                
                # expansion coefficients of the scattered field:
                u_sigma_m_n = u_array[0, linearIndex_n_m_sigma]
                v_sigma_m_n = v_array[0, linearIndex_n_m_sigma]
                
                radiationPower_test = radiationPower_test + (
                    np.sqrt(epsilon_0 / mu_0) * np.pi / k_medium ** 2
                ) * 1 / 2 * D_sigma_m_n * (
                    np.abs(s_sigma_m_n + u_sigma_m_n) ** 2
                    + np.abs(t_sigma_m_n + v_sigma_m_n) ** 2
                )
        
    # the relative rates:
    absorptionRate = radiationPower_test / W_R0
    scatteringRate = radiationPower / W_R0
    totalRate = 0
    
    print(absorptionRate)
    print(scatteringRate)

    return absorptionRate, scatteringRate, totalRate


def freeSpaceRadiationRate(k_medium, c_0, mu_0, dipoleVector):

    W_R0 = (
        (k_medium * c_0) ** 4 / 12 / np.pi * mu_0 / c_0
        * np.matmul(np.transpose(dipoleVector), np.conjugate(dipoleVector))
        )
    
        # incorrect formula:
        # omega = k_0 * c_0
        # W_R0 = omega ** 4 / 12 / np.pi * mu_0 / c_0
        # * np.matmul(np.transpose(dipoleVector), np.conjugate(dipoleVector))


    return W_R0
