import numpy as np
from scipy import special


def absorptionIntegrals(n_max, R, k_sphere):

    # number of points for the numerical integration (summation):
    number_int = int(np.ceil(R / (0.1 * 10 ** -9)))
    # step size and radial distance for the integration:
    dr = R / number_int
    r_int = np.linspace(dr / 2, R - dr / 2, number_int)

    int_absorbed = np.ones([n_max+2])

    for n in range(0, n_max + 2):

        # spherical bessel functions for the numerical integration:
        j_n_int = special.spherical_jn(n, k_sphere * r_int)

        # numerical integrals for calculating the power absorption:
        int_absorbed[n] = sum(np.abs(j_n_int) ** 2 * r_int ** 2) * dr
        
        #print(n, n_max, int_absorbed[n])

    return int_absorbed
