import numpy as np
from Mie_calculation.Mie_coefficients import coefficients
from Mie_calculation.Mie_dimensionlessQuantities import sizeParameter
from Mie_calculation.Mie_dimensionlessQuantities import relativeRefractiveIndex

#from bhmie import bhmie


def Mie_planeWave_efficiencies(lambda_0, r, refr_sphere, refr_medium):

	x=sizeParameter(lambda_0, r, refr_sphere, refr_medium)

	# sperical Bessel function:


	#spherical_jn(n, z[, derivative])
	#spherical_yn(n, z[, derivative])

	# spherical Hankel functions of the first kind

	#h_n=spherical_jn(n, z[, derivative])+np.i*spherical_yn(n, z[, derivative])


	# Mie scattering of plane wave according to the Wikipedia



	Q_e=0
	Q_s=0
	m=relativeRefractiveIndex(refr_sphere, refr_medium)
	
	
	
	L_max=int(np.abs(x) + 4.*np.abs(x)**0.3333 + 10.0)+3
	#L_max=100
	
	for L in range(1, L_max+1):
		
		[a_L, b_L, alpha_L, beta_L]=coefficients(x, m, L)
		
		#[a_n, b_n]=bhmie(x, m, L)
		#print(a_n,a_L)
		#print(b_n,b_L)
		#print(L)
		
		Q_s=Q_s+2/x**2*(2*L+1)*(abs(a_L)**2+abs(b_L)**2)
		Q_e=Q_e-2/x**2*(2*L+1)*np.real(a_L+b_L)
		
	
	
	Q_a=Q_e-Q_s

	return Q_a, Q_s, Q_e
