import numpy as np

import Mie_calculation.Mie_coefficients as Mie_coefficients
import Mie_calculation.Mie_sphericalVectorWaves as Mie_sphericalVectorWaves
import Mie_calculation.Mie_dipoleEmitter_electricField as \
    Mie_dipoleEmitter_electricField
import Mie_calculation.Mie_electricField as Mie_electricField

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors
import Mie_calculation.auxiliary.pointDistributionOnSphere as \
    pointDistributionOnSphere
import Mie_calculation.auxiliary.coordinateManagement as coordinateManagement
import Mie_calculation.auxiliary.Mie_summationAccessaries as \
    Mie_summationAccessaries
import Mie_calculation.auxiliary.cropPositionsOnSphere as cropPositionsOnSphere

import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as \
    Mie_dipoleEmitter_farfieldQuantities


# TODO sampling point number
def simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max,
    fieldCalculationType='total',
    coverSlipCropping = True,
):
    
    emissionWavelength = fluorophore['emissionWavelength']
    excitationWavelength = fluorophore['excitationWavelength']
    
    dipoleVector_numberOfPoints = dipoleOrientationObject.dipoleVector_numberOfPoints

    sphereRadius = dipolePositionObject.sphereRadius
    dipolePosition_numberOfPoints = dipolePositionObject.dipolePosition_numberOfPoints
    dipoleDistance = dipolePositionObject.dipoleDistance
    # dipole positions, evenly distributed on a sphere aroung the spherical
    # particle, 3xN array:
    dipolePosition = dipolePositionObject.getPositions()

    if coverSlipCropping:
        # crop the possible dipole positions so they lie outside of a cone
        # (modelling the binding of the particle to the glass coverslip):
        dipolePosition, dipolePosition_numberOfPoints = \
            cropPositionsOnSphere.coverslipCropping_sphericalCoodinates(dipolePosition, sphereRadius, dipoleDistance)

    # point coordinate for the electric farfield sampling, evenly distributed
    # on a conical sphere, 3xN array:
    if collectionAngle == 0:
        samplingPointDistributionType = "z axis"
        electricField_numberOfPoints_4PI = 1
    else:
        samplingPointDistributionType = "even"
    
    positionVector_farField = \
        pointDistributionOnSphere.\
        getPositionVectorsWithinCone_sphericalCoordinates(
            electricField_numberOfPoints_4PI, samplingDistance,
            collectionAngle, samplingPointDistributionType)

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    # spherical vector waves for all n, m, sigma index and for all sampling
    # position:
    (M_3_array, N_3_array) = \
        Mie_sphericalVectorWaves.arraysForDipoleElectricField(
            positionVector_farField, emissionWavelength,
            refractiveIndex_medium, n_max)
    
    freeDipoleSignal = calculateFreeDipoleSignal(
        fluorophore,
        electricField_numberOfPoints_4PI,
        samplingDistance,
        collectionAngle,
        refractiveIndex_sphere,
        refractiveIndex_medium,
        excitationWeightingFunction,
        emissionWeightingFunction,
        signalNormalizationBoolean,
        n_max
    )

    
    farfieldQuantity = farfieldQuantityFunction.initializeArray(dipolePosition_numberOfPoints, dipoleVector_numberOfPoints)
    
    for idxPosition in range(0, dipolePosition_numberOfPoints):

        # dipole vectors, pointing to evenly distributed directions ( acquired
        # from the coordinates of points on unit sphere centered at the origo ),
        # 3xN array:
        dipoleVector = dipoleOrientationObject.getOrientations()

        dipolePosition_act = np.zeros([3, 1])
        dipolePosition_act[:, 0] = dipolePosition[:, idxPosition]

        # expansion coefficients for all n, m, sigma index and for all dipole
        # orientation:
        (s_array, t_array, u_array, v_array) = \
            Mie_coefficients.expansionCoefficients_orientationArray(
                dipoleVector, dipolePosition_act, sphereRadius,
                emissionWavelength, refractiveIndex_sphere,
                refractiveIndex_medium, n_max)

        # weightings for the fluorescence enahncement:
        excitationWeighting = excitationWeightingFunction.getWeighting(dipolePosition_act, dipoleVector, sphereRadius, excitationWavelength, refractiveIndex_medium, refractiveIndex_sphere)
        if idxPosition == 0:
            # the emission weighting should not change with the position
            emissionWeighting = emissionWeightingFunction.getWeighting(dipolePosition_act, dipoleVector, sphereRadius, emissionWavelength, refractiveIndex_medium, refractiveIndex_sphere, n_max)

        for idxOrientation in range(0, dipoleVector_numberOfPoints):
            # varying the dipole orientation

            # the summation index dependent expansion coefficients belonging to
            # the given dipole orientation:
            s_sequence_orientation = s_array[idxOrientation, :]
            t_sequence_orientation = t_array[idxOrientation, :]
            u_sequence_orientation = u_array[idxOrientation, :]
            v_sequence_orientation = v_array[idxOrientation, :]

            # electric field distribution on a sampling points within the cone
            # with fixed dipole orientation and dipole position:
            if fieldCalculationType=='total':
                electricField = \
                    Mie_dipoleEmitter_electricField.getElectricField_total(
                        D_sequence,
                        s_sequence_orientation, t_sequence_orientation,
                        u_sequence_orientation, v_sequence_orientation,
                        M_3_array, N_3_array)
            
            elif fieldCalculationType== 'scattered':
                electricField = \
                    Mie_dipoleEmitter_electricField.getElectricField_scattered(
                        D_sequence,
                        s_sequence_orientation, t_sequence_orientation,
                        u_sequence_orientation, v_sequence_orientation,
                        M_3_array, N_3_array)
            else:
                raise Exception('Invalid electric field calculation type was given.')
                    
            
                
            """electricField = \
                Mie_dipoleEmitter_electricField.getElectricField_selectedMode(
                    D_sequence,
                    s_sequence_orientation, t_sequence_orientation,
                    u_sequence_orientation, v_sequence_orientation,
                    M_3_array, N_3_array, 3)"""

            electricField = electricField * \
                excitationWeighting[idxOrientation] * emissionWeighting[idxOrientation]

            # get the signal (~photon count) of the three polarization
            # components (the "z" component should be negligible) for the given
            # dipole orientation and dipole position:
            farfieldQuantity[idxPosition, idxOrientation, :] =\
                farfieldQuantityFunction.getQuantityFromFarfield(
                    positionVector_farField, electricField,
                    collectionAngle, samplingDistance, refractiveIndex_medium)

    # sum the polarization channel signals along the dipole orientations
    # (2nd dimension):
    farfieldQuantity_orientationAveraged, filteringBoolean = farfieldQuantityFunction.averageOrientations(farfieldQuantity, freeDipoleSignal)

    dipolePosition = dipolePosition[:, filteringBoolean]

    return farfieldQuantity_orientationAveraged, dipolePosition

def calculateFreeDipoleSignal(
    fluorophore,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    signalNormalizationBoolean,
    n_max
):
    # This function thresholds the blinking events based on their "x" and "y"
    # channles signal values. For the thresholding value, it calculates the
    # brightness of a free dipole (without the particle) and uses it as the
    # basis of the comparison.
    
    sphereRadius = 1 * 10 ** -9
    dipoleDistance = 1 * 10 ** -9
    dipolePosition_numberOfPoints = 1 
    dipolePositionObject = Mie_dipoleVectors.dipolePosition.top(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)

    dipoleVector_numberOfPoints = 1000
    dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
    
    farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.sumSignal()
    
    refractiveIndex_sphere = refractiveIndex_medium
    
    if not signalNormalizationBoolean:
        farfieldQuantity = np.ones(1)
    else: 
    
        signalNormalizationBoolean = False
        
        # free dipole brightness:
        farfieldQuantity, dipolePosition = simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle,
            refractiveIndex_sphere,
            refractiveIndex_medium,
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )

    if np.size(farfieldQuantity) != 1:
        raise ValueError("When calculating the free dipole brightness, result with invalid dimensions (\"{}\") was returned.".format(farfieldQuantity.shape))
        
    # TODO: move these to their places
    """
    # filter the blinking events:
    signalThreshold = totalSignal_freeDipole * detectabilityRatio
    filterBoolean = (channelSignal[0, :] >= signalThreshold) & (channelSignal[1, :] >= signalThreshold)
    
    dipolePosition_filtered = dipolePosition[:, filterBoolean]
    channelSignal_filtered = channelSignal[:, filterBoolean]
    """
    
    return farfieldQuantity
