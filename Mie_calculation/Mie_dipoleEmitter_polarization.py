import numpy as np


def convertToDescartes(intensity_spherical, positionVector_spherical):

    theta_N = np.shape(electricField_spherical)[1]

    fi_N = np.shape(electricField_spherical)[2]

    d_theta_mat = get_d_theta(theta_mat)
    d_fi_mat = get_d_fi(fi_mat)

    for idx_theta in range(0, theta_N):
        
        power=np.zeros([3,1])
        
        for idx_fi in range(0, fi_N):
            
            theta = positionVector_spherical(1, idx_theta, idx_fi)
            fi = positionVector_spherical(2, idx_theta, idx_fi)
            
            #TODO:check
            T = np.array([
                [0, np.cos(fi)**2, np.sin(fi)**2],
                [0, np.sin(fi)**2, np.cos(fi)**2],
                [1, 0, 0]
            ])

            # TODO: matrix multiplication
            electricField_Projected[:, idx_theta, idx_fi] = \
                numpy.matmul(T, electricField_spherical[:, idx_theta, idx_fi])

            intensity[:, idx_theta, idx_fi] =\
                electricField_Projected[:, idx_theta, idx_fi] * \
                np.conj(electricField_Projected[:, idx_theta, idx_fi])

            r = positionVector_spherical(0, idx_theta, idx_fi)
            d_theta = d_theta_mat(idx_theta, idx_fi)
            d_fi = d_fi_mat(idx_theta, idx_fi)

            df = r**2*d_fi*d_theta*sin(theta)

            power[:] += intensity[:, idx_theta, idx_fi]

        polarizationDegree = (power[0]-power[1])/(power[0]+power[1])


def convertToDescartes(electricField_spherical, positionVector_spherical):

    #positionVector_Descartes = np.zeros(shape(positionVector_spherical))

    electricField_Projected = np.zeros(shape(electricField_spherical))

    theta_N = shape(electricField_spherical)[1]

    fi_N = shape(electricField_spherical)[2]

    d_theta_mat = get_d_theta(theta_mat)
    d_fi_mat = get_d_fi(fi_mat)

    for idx_theta in range(0, theta_N):
        
        power=np.zeros([3,1])
        
        for idx_fi in range(0, fi_N):
            
            theta = positionVector_spherical(1, idx_theta, idx_fi)
            fi = positionVector_spherical(2, idx_theta, idx_fi)
            
            T = np.array([
                [0, np.cos(fi), -np.sin(fi)],
                [0, np.sin(fi), np.cos(fi)],
                [1, 0, 0]
            ])

            # TODO: matrix multiplication
            electricField_Projected[:, idx_theta, idx_fi] = \
                numpy.matmul(T, electricField_spherical[:, idx_theta, idx_fi])

            intensity[:, idx_theta, idx_fi] =\
                electricField_Projected[:, idx_theta, idx_fi] * \
                np.conj(electricField_Projected[:, idx_theta, idx_fi])

            r = positionVector_spherical(0, idx_theta, idx_fi)
            d_theta = d_theta_mat(idx_theta, idx_fi)
            d_fi = d_fi_mat(idx_theta, idx_fi)

            df = r**2*d_fi*d_theta*sin(theta)

            power[:] += intensity[:, idx_theta, idx_fi]

        polarizationDegree = (power[0]-power[1])/(power[0]+power[1])



def get_d_theta(theta_mat):
# This function reutns a matrix containing the delta theta angle values in the dimension where the theta changes 

    theta_N = np.shape(theta_mat)[1]

    phi_N = np.shape(theta_mat)[2]

    d_theta_mat=np.zeros([theta_N, phi_N])
    
    # the delta theta for the intermediate values:
    d_theta_mat[1:theta_N-1, :]=np.abs(theta_mat[2:theta_N, :]-theta_mat[0:theta_N-2, :])/2
    
    # the delta theta for the termival values:
    d_theta_mat[0, :]=np.abs(theta_mat[1, :]-theta_mat[0, :])
    d_theta_mat[theta_N-1, :]=np.abs(theta_mat[theta_N-1, :]-theta_mat[theta_N-2, :])
    
    return d_theta_mat

def get_d_fi(phi_mat):
# This function returns a matrix containing the delta fi angle values in the dimension where the phi changes 

    theta_N = np.shape(phi_mat)[1]

    phi_N = np.shape(phi_mat)[2]

    d_phi_mat=np.zeros([theta_N, phi_N])
    
    # the delta fi for the intermediate values:
    d_phi_mat[:, 1:phi_N-1]=np.abs(phi_mat[:, 2:phi_N]-phi_mat[:, 0:phi_N-2])/2
    
    # the delta fi for the termival values:
    d_phi_mat[:, 0]=np.abs(phi_mat[:, 1]-phi_mat[:, ])
    d_phi_mat[:, phi_N-1]=np.abs(phi_mat[:, phi_N-1]-phi_mat[:, theta_N-2])
    
    return d_phi_mat
