# These functions calculate the modified quentum efficiency (by the local
# environment, dynamic quencher...) from the intrinsic quamtum efficiency of
# the fluorophore.

def getQE_using_totalRate(QE_0, radiativeRate, totalRate):
    
    QE = radiativeRate / (totalRate + 1 / QE_0 - 1)
    
    return QE
    
def getQE_using_absorptionRate(QE_0, radiativeRate, absorptionRate):

    QE = radiativeRate / (radiativeRate + absorptionRate + 1 / QE_0 - 1)
    
    return QE


def getQE_with_quenhcer(QE_0, quenchingCoefficient, concentration):
    
    QE = QE_0 / (concentration * quenchingCoefficient)
    
    return QE
