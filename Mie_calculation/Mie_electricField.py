import numpy as np

import Mie_calculation.auxiliary.Mie_physicalConstants as Mie_physicalConstants
from Mie_calculation.Mie_coefficients import Mie_coefficients
import Mie_calculation.auxiliary.sphericalFunctions as sphericalFunctions
import Mie_calculation.Mie_sphericalVectorWaves as Mie_sphericalVectorWaves
import Mie_calculation.auxiliary.Mie_summationAccessaries as Mie_summationAccessaries
import Mie_calculation.auxiliary.coordinateManagement as coordinateManagement


# TODO direction, negative or positive??????????

def planeWave_direction_Z_polarization_X(samplingPositions, R, k_medium, refractiveIndex_relative):
    
    
    r = samplingPositions[0, :]
    theta = samplingPositions[1, :]
    phi = samplingPositions[2, :]
    
    # dimensionless sphere radius and sampling radii:
    kr = k_medium * r
    kR = k_medium * R
    
    # let the amplitude of the excitation field be: 
    E_0 = 1
    
    # number of terms in the expansion:
    n_max=10
    
    # Bessels functions and their derivates for the "n" expansion terms:
    j_array, j_der_array = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_array, h_der_array = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_array, j_der_array)

    # Legendre polynomials for the "n" and "m" expansion terms:
    P_array, P_m_per_sinTheta_sequence, P_der_array = sphericalFunctions.associatedLegendrePolynomials(n_max, theta)
    
    # electric field calculation:
    E_incident = np.zeros([np.size(samplingPositions, 1), 3], dtype=np.complex128)
    E_scattered = np.zeros([np.size(samplingPositions, 1), 3], dtype=np.complex128)
    for n in range(1, n_max+1):
        
        a_n, b_n, alpha_n, beta_n = Mie_coefficients(kR, refractiveIndex_relative, n)
        
        linearIndex_n = Mie_summationAccessaries.indexMapping_n(n)
        j_n = j_array[:, linearIndex_n]
        j_n_der = j_der_array[:, linearIndex_n]
        h_n = h_array[:, linearIndex_n]
        h_n_der = h_der_array[:, linearIndex_n]
        
        m = 1
        linearIndex_m_n = Mie_summationAccessaries.indexMapping_n_m(n, m)
        P_1_n = P_array[:, linearIndex_m_n]
        P_m_per_sinTheta_1_n = P_m_per_sinTheta_sequence[:, linearIndex_m_n]
        P_1_n_der = P_der_array[:, linearIndex_m_n]
        
        # getting the spherical vector waves:
        sigma_o = 1
        sigma_e = 2
        M_1_o_1_n = Mie_sphericalVectorWaves.M_1(n, m, sigma_o, theta, phi, j_n, P_1_n, P_m_per_sinTheta_1_n, P_1_n_der)
        N_1_e_1_n = Mie_sphericalVectorWaves.N_1(n, m, sigma_e, kr, theta, phi, j_n, j_n_der, P_1_n, P_m_per_sinTheta_1_n, P_1_n_der)
        M_3_o_1_n = Mie_sphericalVectorWaves.M_3(n, m, sigma_o, theta, phi, h_n, P_1_n, P_m_per_sinTheta_1_n, P_1_n_der)
        N_3_e_1_n = Mie_sphericalVectorWaves.N_3(n, m, sigma_e, kr, theta, phi, h_n, h_n_der, P_1_n, P_m_per_sinTheta_1_n, P_1_n_der)
        
        E_incident += E_0 * 1j**n * (2 * n + 1)/(n * (n + 1)) * (M_1_o_1_n - 1j * N_1_e_1_n)
        E_scattered += E_0 * 1j**n * (2 * n + 1)/(n * (n + 1)) * (1j * a_n * M_3_o_1_n - b_n * N_3_e_1_n)
    
    E_total = E_incident + E_scattered
    
    return E_total


def planeWave_direction_X_polarization_Z(samplingPositions, R, k_medium, refractiveIndex_relative):

    rotationAngle = 90 * np.pi/180;
    samplingPositions_rotated_spherical = coordinateManagement.rotateCoordinates_Y_spherical(samplingPositions, rotationAngle)
    
    electricField_rotatedPosition = planeWave_direction_Z_polarization_X(samplingPositions_rotated_spherical, R, k_medium, refractiveIndex_relative)
    
    rotationAngle_inverse = -rotationAngle;
    electricField = coordinateManagement.rotateVectorField_Y_spherical(samplingPositions_rotated_spherical, electricField_rotatedPosition, rotationAngle_inverse, samplingPositions)
    
    return electricField

def scatteredPower(distance, electricField, refractiveIndex_medium):


    # calculate the "intensities" of the sampling points from the
    # sampled electric field, 3xN numpy array:
    intensityDistribution_Descartes =\
        electricFieldToIntensity(electricField, refractiveIndex_medium)

    # area of unit surface element, required for integration over the sphere:
    collectionAngle = np.pi
    unitSurfaceElementArea = 2 * np.pi * distance**2 * (1 - np.cos(collectionAngle)) / np.size(intensityDistribution_Descartes)

    # summing along the sampling points (2nd dimension) and getting the the polarization
    # signal components for the given dipole orientation and dipole position,
    # the result is a 3 element 1D numpy array:
    power = np.sum(intensityDistribution_Descartes, axis=0) * unitSurfaceElementArea

    return power

def electricFieldToIntensity(electricField, refractiveIndex_medium):
    
    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()

    epsilon=np.real(refractiveIndex_medium**2)
    
    # TODO:check the refractive index, why does it only give right result when it is squared??? (by the way, it equals to epsilon....)
    intensity=epsilon_0*epsilon*c_0/refractiveIndex_medium**2/2*np.real(np.sum(electricField*np.conjugate(electricField), axis=1))
    #intensity=epsilon_0*epsilon*c_0/refractiveIndex_medium/2*np.real(electricField_4PI*np.conjugate(electricField))
    
    return intensity


