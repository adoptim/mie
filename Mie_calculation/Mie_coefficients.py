import math as math
import numpy as np
from scipy import special

import Mie_calculation.Mie_sphericalVectorWaves as Mie_sphericalVectorWaves

# from bhmie import bhmie


def Mie_coefficients(x, m, L):
    # This function returns the Mie coefficients.

    # x: dimensionless radius of the sphere (radius * immersion medium wave vector)
    # m: relative refractive index of the sphere (the ratio of the refractive index of the sphere and of the immersion medium)
    # L: Mie summation coefficient index (also donated as "n")

    # source="Wiki"
    # source="modified Wiki"
    source = "Ruppin_1982"
    # source="Zi"
    # source="antenna model"
    # source="bhmie"

    if source == "Wiki":
        # original Wiki, not correct
        S_L_x = x * special.spherical_jn(L, x)
        S_L_mx = m * x * special.spherical_jn(L, m * x)
        S_L_1_x = x * special.spherical_jn(L - 1, x)
        S_L_1_mx = m * x * special.spherical_jn(L - 1, m * x)
        S_L_x_der = S_L_1_x - L / x * special.spherical_jn(L, x)
        S_L_mx_der = S_L_1_mx - L / (m * x) * special.spherical_jn(L, m * x)

        C_L_x = -x * special.spherical_yn(L, x)
        C_L_1_x = -x * special.spherical_yn(L - 1, x)
        C_L_x_der = C_L_1_x - L / x * special.spherical_yn(L, x)

        a_L = (S_L_x * S_L_mx_der / S_L_mx - m * S_L_x_der) / (
            C_L_x * S_L_mx_der / S_L_mx - m * C_L_x_der
        )
        b_L = (m * S_L_x * S_L_mx_der / S_L_mx - S_L_x_der) / (
            m * C_L_x * S_L_mx_der / S_L_mx - C_L_x_der
        )

        # dummies:
        alpha_L = 1
        beta_L = 1

    elif source == "modified Wiki":
        # modified Wiki, might be correct but unsure
        S_L_x = x * special.spherical_jn(L, x)
        S_L_mx = m * x * special.spherical_jn(L, m * x)
        S_L_1_x = x * special.spherical_jn(L - 1, x)
        S_L_1_mx = m * x * special.spherical_jn(L - 1, m * x)
        S_L_x_der = S_L_1_x - L / x * S_L_x
        S_L_mx_der = S_L_1_mx - L / (m * x) * S_L_mx

        C_L_x = -x * special.spherical_yn(L, x)
        C_L_1_x = -x * special.spherical_yn(L - 1, x)
        C_L_x_der = C_L_1_x - L / x * C_L_x

        xi_L_x = S_L_x - 1j * C_L_x
        xi_L_1_x = S_L_1_x - 1j * C_L_1_x
        xi_L_x_der = S_L_x_der - 1j * C_L_x_der

        a_L = (S_L_x * S_L_mx_der - m * S_L_x_der * S_L_mx) / (
            xi_L_x * S_L_mx_der - m * xi_L_x_der * S_L_mx
        )
        b_L = (m * S_L_x * S_L_mx_der - S_L_x_der * S_L_mx) / (
            m * xi_L_x_der * S_L_mx - xi_L_x * S_L_mx_der
        )

        # dummies:
        alpha_L = 1
        beta_L = 1

    elif source == "Ruppin_1982":
        # Ruppin_1982

        n = L
        epsilon = m ** 2
        # TODO the square root operation is ambigious, check this one
        sqrt_epsilon = m
        kR = x
        k1R = m * kR

        # antenna model testing
        j_n_kR = special.spherical_jn(n, kR)
        j_n_k1R = special.spherical_jn(n, k1R)
        # j_n_1_kR=special.spherical_jn(n+1, kR)
        # j_n_1_k1R=special.spherical_jn(n+1, k1R)
        # j_n_kR_der=-j_n_1_kR+n/kR*j_n_kR
        # j_n_k1R_der=-j_n_1_k1R+n/k1R*j_n_k1R
        j_n_1_kR = special.spherical_jn(n - 1, kR)
        j_n_1_k1R = special.spherical_jn(n - 1, k1R)
        j_n_kR_der = j_n_1_kR - (n + 1) / kR * j_n_kR
        j_n_k1R_der = j_n_1_k1R - (n + 1) / k1R * j_n_k1R

        y_n_kR = special.spherical_yn(n, kR)
        # y_n_1_kR=special.spherical_yn(n+1, kR)
        # y_n_kR_der=-y_n_1_kR+n/kR*y_n_kR
        y_n_1_kR = special.spherical_yn(n - 1, kR)
        y_n_kR_der = y_n_1_kR - (n + 1) / kR * y_n_kR

        h_n_kR = j_n_kR + 1j * y_n_kR
        h_n_kR_der = j_n_kR_der + 1j * y_n_kR_der

        a_L = (
            (j_n_kR + kR * j_n_kR_der) * j_n_k1R
            - j_n_kR * (j_n_k1R + k1R * j_n_k1R_der)
        ) / (
            (j_n_k1R + k1R * j_n_k1R_der) * h_n_kR
            - j_n_k1R * (h_n_kR + kR * h_n_kR_der)
        )

        b_L = (
            (j_n_k1R + k1R * j_n_k1R_der) * j_n_kR
            - epsilon * (j_n_kR + kR * j_n_kR_der) * j_n_k1R
        ) / (
            epsilon * (h_n_kR + kR * h_n_kR_der) * j_n_k1R
            - (j_n_k1R + k1R * j_n_k1R_der) * h_n_kR
        )

        alpha_L = (
            (j_n_kR + kR * j_n_kR_der) * h_n_kR - j_n_kR * (h_n_kR + kR * h_n_kR_der)
        ) / (
            (j_n_k1R + k1R * j_n_k1R_der) * h_n_kR
            - j_n_k1R * (h_n_kR + kR * h_n_kR_der)
        )

        beta_L = (
            sqrt_epsilon
            * (
                (h_n_kR + kR * h_n_kR_der) * j_n_kR
                - (j_n_kR + kR * j_n_kR_der) * h_n_kR
            )
            / (
                epsilon * (h_n_kR + kR * h_n_kR_der) * j_n_k1R
                - (j_n_k1R + k1R * j_n_k1R) * h_n_kR
            )
        )

    elif source == "Zi":
        # Zi, Markov chain
        n = L
        kR = x
        k1R = m * kR

        # antenna model testing
        j_n_kR = special.spherical_jn(n, kR)
        j_n_k1R = special.spherical_jn(n, k1R)
        # j_n_1_kR=special.spherical_jn(n+1, kR)
        # j_n_1_k1R=special.spherical_jn(n+1, k1R)
        # j_n_kR_der=-j_n_1_kR+n/kR*j_n_kR
        # j_n_k1R_der=-j_n_1_k1R+n/k1R*j_n_k1R
        j_n_1_kR = special.spherical_jn(n - 1, kR)
        j_n_1_k1R = special.spherical_jn(n - 1, k1R)
        j_n_kR_der = j_n_1_kR - (n + 1) / kR * j_n_kR
        j_n_k1R_der = j_n_1_k1R - (n + 1) / k1R * j_n_k1R

        y_n_kR = special.spherical_yn(n, kR)
        # y_n_1_kR=special.spherical_yn(n+1, kR)
        # y_n_kR_der=-y_n_1_kR+n/kR*y_n_kR
        y_n_1_kR = special.spherical_yn(n - 1, kR)
        y_n_kR_der = y_n_1_kR - (n + 1) / kR * y_n_kR

        h_n_kR = j_n_kR - 1j * y_n_kR
        h_n_kR_der = j_n_kR_der - 1j * y_n_kR_der

        a_L = (
            j_n_kR * (j_n_k1R + k1R * j_n_k1R_der)
            - m * (j_n_kR + kR * j_n_kR_der) * j_n_k1R
        ) / (
            (j_n_k1R + k1R * j_n_k1R_der) * h_n_kR
            - m * j_n_k1R * (h_n_kR + kR * h_n_kR_der)
        )

        b_L = (
            m * (j_n_k1R + k1R * j_n_k1R_der) * j_n_kR
            - (j_n_kR + kR * j_n_kR_der) * j_n_k1R
        ) / (
            m * (j_n_k1R + k1R * j_n_k1R_der) * h_n_kR
            - (h_n_kR + kR * h_n_kR_der) * j_n_k1R
        )

        # dummies:
        alpha_L = 1
        beta_L = 1

    elif source == "antenna model":
        # antenna model test

        ka = x
        ksa = m * x

        # antenna model testing
        j_1_ka = special.spherical_jn(1, ka)
        j_1_ksa = special.spherical_jn(1, ksa)
        j_1_1_ka = special.spherical_jn(1 + 1, ka)
        j_1_1_ksa = special.spherical_jn(1 + 1, ksa)
        j_1_ka_der = -j_1_1_ka + 1 / ka * j_1_ka
        j_1_ksa_der = -j_1_1_ksa + 1 / ksa * j_1_ksa

        y_1_ka = special.spherical_yn(1, ka)
        y_1_1_ka = special.spherical_yn(1 + 1, ka)
        y_1_ka_der = -y_1_1_ka + 1 / ka * y_1_ka

        h_1_ka = j_1_ka - 1j * y_1_ka
        h_1_ka_der = j_1_ka_der - 1j * y_1_ka_der

        a_L = 1
        b_1 = (
            j_1_ka * (j_1_ksa + ksa * j_1_ksa_der)
            - m ** 2 * j_1_ksa * (j_1_ka + ka * j_1_ka_der)
        ) / (
            m ** 2 * j_1_ksa * (h_1_ka + ka * h_1_ka_der)
            - h_1_ka * (j_1_ksa + ksa * j_1_ksa_der)
        )

        B = 2 * ka ** 3 / 3 * (m ** 2 - 1) / (m ** 2 + 2)

        b_L = b_1
        # b_L=1j*B

        # dummies:
        alpha_L = 1
        beta_L = 1

    elif source == "bhmie":
        #[b_L, a_L] = bhmie(x, m, L)
        # dummies:
        alpha_L = 1
        beta_L = 1

    return a_L, b_L, alpha_L, beta_L

def expansionCoefficients_dipoleField(k_medium, epsilon_0, n, m, sigma, kr, theta, fi, j_n, j_n_der, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der, dipoleVector):
# This function returns the expansion coefficients of the dipole field in the special case when the dipole orientation is parallel to the nanosphere surface.

    M_1_sigma_m_n, N_1_sigma_m_n, M_3_sigma_m_n, N_3_sigma_m_n = Mie_sphericalVectorWaves.getAll(
    n, m, sigma, kr, theta, fi, j_n, j_n_der, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)

    s_sigma_m_n = (
        np.matmul(M_1_sigma_m_n, dipoleVector)
        * 1j * k_medium ** 3 / epsilon_0 / np.pi
    )

    t_sigma_m_n = (
        np.matmul(N_1_sigma_m_n, dipoleVector)
        * 1j * k_medium ** 3 / epsilon_0 / np.pi
    )

    p_sigma_m_n = (
        np.matmul(M_3_sigma_m_n, dipoleVector)
        * 1j * k_medium ** 3 / epsilon_0 / np.pi
    )
    
    q_sigma_m_n = (
        np.matmul(N_3_sigma_m_n, dipoleVector)
        * 1j * k_medium ** 3 / epsilon_0 / np.pi
    )

    """dipole_strength = np.sqrt(
        np.sum(dipoleVector * np.conjugate(dipoleVector), axis=1)
    )
    t_sigma_m_n_2 = (
        1j * k_medium ** 3 / (np.pi * epsilon_0)
        * n * (n + 1) / kr * j_n * dipole_strength
    )
    q_sigma_m_n_2 = (
        1j * k_medium ** 3 / (np.pi * epsilon_0)
        * n * (n + 1) / kr * h_n * dipole_strength
    )"""

    """if m==1:
        print(n, m, sigma, dipoleVector, np.transpose(M_1_sigma_m_n), np.transpose(N_1_sigma_m_n))"""


    return (s_sigma_m_n, t_sigma_m_n, p_sigma_m_n, q_sigma_m_n)

def expansionCoefficients_dipoleField_testing_parallel(k_medium, epsilon_0, n, m, sigma, kr, j_n, j_n_der, h_n, h_n_der, dipoleVector):
# Expansion coefficients of the dipole field in the special case when the dipole orientation is parallel to the nanosphere surface. Useful for testing.

    dipole_strength = np.sqrt(
        np.sum(dipoleVector * np.conjugate(dipoleVector), axis=1)
    )

    # parallel
    if m == 1:
        if sigma == 2:
            # TODO: derivate with respect to r or to kr????
            t_sigma_m_n = (
                1j * k_medium ** 3 / (2 * np.pi * epsilon_0)
                * n * (n + 1) / kr * (j_n + kr * j_n_der)
                * dipole_strength
            )
            q_sigma_m_n = (
                1j * k_medium ** 3 / (2 * np.pi * epsilon_0)
                * n * (n + 1) / kr * (h_n + kr * h_n_der)
                * dipole_strength
            )
            s_sigma_m_n = 0
            p_sigma_m_n = 0
        elif sigma == 1:
            t_sigma_m_n = 0
            q_sigma_m_n = 0
            s_sigma_m_n = (
                1j * k_medium ** 3 / (2 * np.pi * epsilon_0)
                * n * (n + 1) * j_n * dipole_strength
            )
            p_sigma_m_n = (
                1j * k_medium ** 3 / (2 * np.pi * epsilon_0)
                * n * (n + 1) * h_n * dipole_strength
            )
    else:
        t_sigma_m_n = 0
        q_sigma_m_n = 0
        s_sigma_m_n = 0
        p_sigma_m_n = 0
        
    return s_sigma_m_n, t_sigma_m_n, p_sigma_m_n, q_sigma_m_n

def expansionCoefficients_dipoleField_testing_perpendicular(k_medium, epsilon_0, n, m, sigma, kr, j_n, h_n, dipoleVector):
    # Expansion coefficients of the dipole field in the special case when the dipole orientation is perpendicular to the nanosphere surface. Useful for testing.

    dipole_strength = np.sqrt(
        np.sum(dipoleVector * np.conjugate(dipoleVector), axis=1)
    )

    if m == 0:
        if sigma == 2:
            t_sigma_m_n = (
                1j * k_medium ** 3 / (np.pi * epsilon_0)
                * n * (n + 1) / kr * j_n * dipole_strength
            )
            q_sigma_m_n = (
                1j * k_medium ** 3 / (np.pi * epsilon_0)
                * n * (n + 1) / kr * h_n * dipole_strength
            )
            s_sigma_m_n = 0
            p_sigma_m_n = 0
        elif sigma == 1:
            t_sigma_m_n = 0
            q_sigma_m_n = 0
            s_sigma_m_n = 0
            p_sigma_m_n = 0
    else:
        t_sigma_m_n = 0
        q_sigma_m_n = 0
        s_sigma_m_n = 0
        p_sigma_m_n = 0
        
    return s_sigma_m_n, t_sigma_m_n, p_sigma_m_n, q_sigma_m_n
    
# decide whether these are needed:
#def expansion_coefficients_scatteredField():
#   u, v
#def expansion_coefficients_transmittedField():
#    f,g


import Mie_calculation.auxiliary.Mie_summationAccessaries as Mie_summationAccessaries
import Mie_calculation.auxiliary.sphericalFunctions as sphericalFunctions

def expansionCoefficients_orientationArray(dipoleVector, dipolePosition, sphereRadius, emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium, n_max):
# This funvtion calculates the expansion coefficients for an array of dipole vectors (with varying orientation but with fixed dipole strength and with fixed dipole position).

    # number of dipole orientations:
    dipoleVector_numberOfPoints = dipoleVector.shape[1]

    indexNumber = Mie_summationAccessaries.indexNumber_n_m_sigma(n_max)

    # the expansion coefficients:
    s_array = np.zeros([dipoleVector_numberOfPoints, indexNumber], dtype=np.cdouble)
    t_array = np.zeros([dipoleVector_numberOfPoints, indexNumber], dtype=np.cdouble)
    u_array = np.zeros([dipoleVector_numberOfPoints, indexNumber], dtype=np.cdouble)
    v_array = np.zeros([dipoleVector_numberOfPoints, indexNumber], dtype=np.cdouble)

    k_0 = 2 * np.pi / emissionWavelength

    refr_rel = refractiveIndex_sphere / refractiveIndex_medium

    k_medium = refractiveIndex_medium * k_0
    epsilon_0 = 8.8541878128 * 10 ** -12

    kR = k_medium * sphereRadius
    kr = k_medium * dipolePosition[0, :]
    theta = dipolePosition[1, :]
    phi = dipolePosition[2, :]

    j_vect, j_der_vect = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_vect, h_der_vect = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_vect, j_der_vect)
    P_vect, P_m_per_sinTheta_vect, P_der_vect = sphericalFunctions.associatedLegendrePolynomials(n_max, theta)

    indexCount_n = 0
    linearIndex_n = 0
    indexCount_n_m = 0
    linearIndex_n_m = 0
    indexCount_n_m_sigma = 0
    linearIndex_n_m_sigma = 0
    for n in range(1, n_max+1):
        indexCount_n += 1
        linearIndex_n = indexCount_n-1

        j_n = j_vect[:, linearIndex_n]
        j_n_der = j_der_vect[:, linearIndex_n]
        h_n = h_vect[:, linearIndex_n]
        h_n_der = h_der_vect[:, linearIndex_n]

        # the Mie coefficients:
        a_n, b_n, alpha_n, beta_n = Mie_coefficients(kR, refr_rel, n)

        for m in range(0, n+1):
            indexCount_n_m += 1
            linearIndex_n_m = indexCount_n_m-1

            P_m_n = P_vect[:, linearIndex_n_m]
            P_m_per_sinTheta_m_n = P_m_per_sinTheta_vect[:, linearIndex_n_m]
            P_m_n_der = P_der_vect[:, linearIndex_n_m]

            for sigma in range(1, 3):
                indexCount_n_m_sigma += 1
                linearIndex_n_m_sigma = indexCount_n_m_sigma-1

                # the expansion coefficients for the given summation index:
                (s_sigma_m_n, t_sigma_m_n, p_sigma_m_n, q_sigma_m_n) = expansionCoefficients_dipoleField(k_medium, epsilon_0, n, m, sigma, kr, theta, phi, j_n, j_n_der, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der, dipoleVector)

                # review it!!!!!!
                s_array[:, linearIndex_n_m_sigma] = s_sigma_m_n[0, :]
                t_array[:, linearIndex_n_m_sigma] = t_sigma_m_n[0, :]

                u_array[:, linearIndex_n_m_sigma] = p_sigma_m_n[0, :]*a_n
                v_array[:, linearIndex_n_m_sigma] = q_sigma_m_n[0, :]*b_n

    return s_array, t_array, u_array, v_array
