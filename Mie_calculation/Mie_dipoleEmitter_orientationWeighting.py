import numpy as np

import Mie_calculation.auxiliary.coordinateManagement as coordinateManagement
import Mie_calculation.Mie_electricField as Mie_electricField
import Mie_calculation.Mie_dipoleEmitter_rates as Mie_dipoleEmitter_rates
import Mie_calculation.Mie_dipoleEmitter_quantumEfficiency as Mie_dipoleEmitter_quantumEfficiency

class excitationWeighting:
    # Different methods for accounting for the fluorescence enhancement on the
    # excitation side. The calculated weigthings are for the emitted electric
    # field's amplitde, not the any energy related quantity. Required when the
    # contribution of multiple dipoles with different orientation has to be
    # compared.
    # Returns the weightings for a single dipole position with multiple dipole
    # orientations. 
    
    class none:
        # Do not use any weighting for the excitation, e.g fast rotation
        # (compared to the fluorescence lifetime) when the total emitted photon
        # number is not in the scope if the calculattion (e.g only the
        # polarization degree)
        
        def __init__(self):
            pass
        
        def getWeighting(self, dipolePosition_actual, dipoleVector, sphereRadius, excitationWavelength, refractiveIndex_medium, refractiveIndex_sphere):
            
            weighting = np.ones([dipoleVector.shape[1]])
            
            return weighting
        
    
    class planeWave_direction_X_polarization_Z_slowRotation:
        # Account for the excitation electric field as the local electric field
        # around the spherical particle produced by a "z" linearily polarized
        # plane wave propagating in the "x" direction for all given dipole
        # orientions individually. Useful for the e.g the slow rotation case
        # (compared to the fluorescence lifetime)
        
        def __init__(self):
            pass
        
        def getWeighting(self, dipolePosition_actual, dipoleVector, sphereRadius, excitationWavelength, refractiveIndex_medium, refractiveIndex_sphere):
            
            
            k_medium = refractiveIndex_medium * 2 * np.pi / excitationWavelength
            refractiveIndex_relative = refractiveIndex_sphere / refractiveIndex_medium
            
            # calculate the excitation electric field in Descartes coordinates:
            excitationField_spherical = Mie_electricField.planeWave_direction_X_polarization_Z(dipolePosition_actual, sphereRadius, k_medium, refractiveIndex_relative)
            excitationField_Descartes = coordinateManagement.sphericalComponentsToDescartes(dipolePosition_actual, excitationField_spherical)
            
            # the dipole vector of the flurophone in Descartes coordinates:
            dipoleVector_Descartes = np.transpose(coordinateManagement.sphericalComponentsToDescartes(dipolePosition_actual, np.transpose(dipoleVector)))
            
            # calculate the excitation weighting for the amitted electric
            # field amplitude as sqrt(|E*p|^2) = |E*p|, where "E" is the
            # excitation field, "p" is the dipole moment:
            weighting = np.transpose(np.abs(np.matmul(excitationField_Descartes, dipoleVector_Descartes)))
            
            return weighting
        
    class planeWave_direction_X_polarization_Z_fastRotation:
        # Account for the excitation electric field as averaged (to all given
        # orientiations) the local electric field around the spherical
        # particle produced by a "z" linearily polarized plane wave propagating
        # in the "x" direction. useful for the e.g the fast rotation case
        # (compared to the fluorescence lifetime)
        
        def __init__(self):
            pass
        
        def getWeighting(self, dipolePosition_actual, dipoleVector, sphereRadius, excitationWavelength, refractiveIndex_medium, refractiveIndex_sphere):
            
            # TODO: include the constants resultant from the averaging integral
            
            k_medium = refractiveIndex_medium * 2 * np.pi / excitationWavelength
            refractiveIndex_relative = refractiveIndex_sphere / refractiveIndex_medium
            
            # calculate the excitation electric field in Descartes coordinates:
            excitationField_spherical = Mie_electricField.planeWave_direction_X_polarization_Z(dipolePosition_actual, sphereRadius, k_medium, refractiveIndex_relative)
            excitationField_Descartes = coordinateManagement.sphericalComponentsToDescartes(dipolePosition_actual, excitationField_spherical)
            
            # the excitation intensity (after averaging, the excitation rate is
            # linerarily proportional to this to this):
            excitationStrength = np.sum(excitationField_Descartes * np.conjugate(excitationField_Descartes))
            
            weighting = np.ones(dipoleVector.shape[1]) * excitationStrength
            
            return weighting
        
class emissionWeighting:
    # Different methods for accounting for the fluorescence emission strength
    # beside the radiative rate enhancement, e.g accounting for the quantum
    # efficiency. The calculated weigthings are for the emitted electric
    # field's amplitde, not the any energy related quantity. Required when the
    # contribution of multiple dipoles with different orientation has to be
    # compared.
    # Returns the weightings for a single dipole position with multiple dipole
    # orientations. 
    
    class none:
        # Do not use any weighting, only the electric field's amplitude will
        # be determined by radiative rate (and the emission angle
        # distribution), usable for the e.g fast rotation case (compared to
        # the fluorescence lifetime) when the total emitted photon
        # number is not in the scope if the calculattion (e.g only the
        # polarization degree)
        
        def __init__(self):
            pass
        
        def getWeighting(self, dipolePosition_actual, dipoleVector, sphereRadius, emissionWavelength, refractiveIndex_medium, refractiveIndex_sphere, n_max):
            
            weighting = np.ones([dipoleVector.shape[1]])
            
            return weighting
    
    class no_absorption:
        # Eliminate the contribution of the radiative rate and consider the
        # emission's quantum efficiency for every dipole orientation
        # individually, applicable in e.g. the slow rotation case (compared to
        # the fluorescence lifetime)
        # The number of emitted photons is solely determined by the excitation
        # strength and the quantum yield.
        
        def __init__(self, QE_0):
            self.QE_0 = QE_0
        
        def getWeighting(self, dipolePosition_actual, dipoleVector, sphereRadius, emissionWavelength, refractiveIndex_medium, refractiveIndex_sphere, n_max):
            
            # get the fluorescence rates (total, radiate, non-radiative) for
            # the quantum efficiency calculation:
            absorptionRate, radiativeRate, totalRate =  Mie_dipoleEmitter_rates.Mie_dipoleEmitter_rates(
                emissionWavelength,
                sphereRadius,
                dipolePosition_actual,
                dipoleVector,
                refractiveIndex_sphere,
                refractiveIndex_medium,
                n_max
            )

            weighting = 1/np.sqrt(radiativeRate)*self.QE_0
                
            return weighting
    
    class excitationLimitedCase_slowRotation:
        # Eliminate the contribution of the radiative rate and consider the
        # emission's quantum efficiency for every dipole orientation
        # individually, applicable in e.g. the slow rotation case (compared to
        # the fluorescence lifetime)
        # The number of emitted photons is solely determined by the excitation
        # strength and the quantum yield.
        
        def __init__(self, QE_0):
            self.QE_0 = QE_0
        
        def getWeighting(self, dipolePosition_actual, dipoleVector, sphereRadius, emissionWavelength, refractiveIndex_medium, refractiveIndex_sphere, n_max):
            
            # get the fluorescence rates (total, radiate, non-radiative) for
            # the quantum efficiency calculation:
            absorptionRate, radiativeRate, totalRate =  Mie_dipoleEmitter_rates.Mie_dipoleEmitter_rates(
                emissionWavelength,
                sphereRadius,
                dipolePosition_actual,
                dipoleVector,
                refractiveIndex_sphere,
                refractiveIndex_medium,
                n_max
            )
            
            # calculate the quantum efficiency accounting for the instrinsic
            # quantum efficiency (QE_0) of the fluorophore:
            QE = Mie_dipoleEmitter_quantumEfficiency.getQE_using_totalRate(self.QE_0, radiativeRate, totalRate)

            weighting = 1/np.sqrt(radiativeRate) * np.sqrt(QE)
                
            return weighting

    class excitationLimitedCase_fastRotation:
        # Eliminate the contribution of the radiative rate and consider the
        # emission's quantum efficiency, applicable in e.g. the fast rotation
        # case (compared to the fluorescence lifetime)
        # The number of emitted photons is solely determined by the excitation
        # strength, the quantum yield and the relative radiative rate of the 
        # given dipole orienation compared to the average..
        
        def __init__(self, QE_0):
            self.QE_0 = QE_0
        
        def getWeighting(self, dipolePosition_actual, dipoleVector, sphereRadius, emissionWavelength, refractiveIndex_medium, refractiveIndex_sphere, n_max):
            
            # get the fluorescence rates (total, radiate, non-radiative) for
            # the quantum efficiency calculation:
            absorptionRate, radiativeRate, totalRate =  Mie_dipoleEmitter_rates.Mie_dipoleEmitter_rates(
                emissionWavelength,
                sphereRadius,
                dipolePosition_actual,
                dipoleVector,
                refractiveIndex_sphere,
                refractiveIndex_medium,
                n_max
            )
            
            # the rates averaged to the given oritentations:
            absorptionRate_mean = np.mean(absorptionRate)
            radiativeRate_mean = np.mean(radiativeRate)
            
            # quantum yield of the emission process when the dipole orientations are averaged for a single photon emission
            QE = Mie_dipoleEmitter_quantumEfficiency.getQE_using_absorptionRate(self.QE_0, radiativeRate_mean, absorptionRate_mean)

            weighting = np.ones([dipoleVector.shape[1]]) * 1 / np.sqrt(radiativeRate_mean) * np.sqrt(QE)
                
            return weighting
