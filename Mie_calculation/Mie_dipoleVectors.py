import numpy as np

import Mie_calculation.auxiliary.pointDistributionOnSphere as \
    pointDistributionOnSphere


# Dimensions of the dipole orientation or position vector arrays: 
# 3 x N,
# where N is the number of positions or orientations.


class dipolePosition:

    class evenDistribution_4pi:
        # This class if for getting the polarized far field PSF-s for 

        def __init__(self,
                     sphereRadius,
                     dipoleDistance,
                     dipolePosition_numberOfPoints):
            self.sphereRadius = sphereRadius
            self.dipoleDistance = dipoleDistance
            self.dipolePosition_numberOfPoints = dipolePosition_numberOfPoints


        def getPositions(self):

            
            # the radial coordinate of the dipole position:
            dipolePosition_r = self.sphereRadius + self.dipoleDistance
            # dipole positions, evenly distributed on a sphere aroung the spherical
            # particle, 3xN array:
            dipolePosition = \
                pointDistributionOnSphere.getPositionVectors_sphericalCoordinates(
                    self.dipolePosition_numberOfPoints, dipolePosition_r, "even")
                
            return dipolePosition
        
    class uniformDistribution_4pi:
        # This class if for getting the polarized far field PSF-s for 

        def __init__(self,
                     sphereRadius,
                     dipoleDistance,
                     dipolePosition_numberOfPoints):
            self.sphereRadius = sphereRadius
            self.dipoleDistance = dipoleDistance
            self.dipolePosition_numberOfPoints = dipolePosition_numberOfPoints


        def getPositions(self):

            
            # the radial coordinate of the dipole position:
            dipolePosition_r = self.sphereRadius + self.dipoleDistance
            # dipole positions, evenly distributed on a sphere aroung the spherical
            # particle, 3xN array:
            dipolePosition = \
                pointDistributionOnSphere.getPositionVectors_sphericalCoordinates(
                    self.dipolePosition_numberOfPoints, dipolePosition_r, "uniform")
        
            return dipolePosition
    
    class arbitrary:
        def __init__(self,
                     sphereRadius,
                     dipoleDistance,
                     dipolePosition_numberOfPoints,
                     theta,
                     phi):
            self.sphereRadius = sphereRadius
            self.dipoleDistance = dipoleDistance
            self.dipolePosition_numberOfPoints = dipolePosition_numberOfPoints
            self.theta = theta
            self.phi = phi


        def getPositions(self):

            sphereRadius = self.sphereRadius
            if np.size(self.sphereRadius) == 1:
                sphereRadius = np.repeat(self.sphereRadius, self.dipolePosition_numberOfPoints)
            elif np.size(self.sphereRadius) != self.dipolePosition_numberOfPoints:
                raise Exception("The number of supplied distances (\"{}\") is not 1 and does not match the number of dipole positions (\"{}\").", np.size(self.sphereRadius), self.dipolePosition_numberOfPoints)
            
            dipoleDistance = self.dipoleDistance
            if np.size(self.dipoleDistance) == 1:
                dipoleDistance = np.repeat(self.dipoleDistance, self.dipolePosition_numberOfPoints)
            elif np.size(self.dipoleDistance) != self.dipolePosition_numberOfPoints:
                raise Exception("The number of supplied distances (\"{}\") is not 1 and does not match the number of dipole positions (\"{}\").", np.size(self.dipoleDistance), self.dipolePosition_numberOfPoints)
            
            theta = self.theta
            if np.size(self.theta) == 1:
                theta = np.repeat(self.theta, self.dipolePosition_numberOfPoints)
            elif np.size(self.theta) != self.dipolePosition_numberOfPoints:
                raise Exception("The number of supplied theta polar angles (\"{}\") is not 1 and does not match the number of dipole positions (\"{}\").", self.theta, self.dipolePosition_numberOfPoints)
            
            phi = self.phi
            if np.size(self.phi) == 1:
                phi = np.repeat(self.phi, self.dipolePosition_numberOfPoints)
            elif np.size(self.phi) != self.dipolePosition_numberOfPoints:
                raise Exception("The number of supplied distances (\"{}\") is not 1 and does not match the number of dipole positions (\"{}\").", self.phi, self.dipolePosition_numberOfPoints)
                
            # the radial coordinate of the dipole position:
            dipolePosition_r = sphereRadius + dipoleDistance
            # dipole positions, evenly distributed on a sphere aroung the spherical
            # particle, 3xN array:
            dipolePosition = np.array([dipolePosition_r, theta, phi])
                
            return dipolePosition
    
    class top:
        # This class if for getting the polarized far field PSF-s for 

        def __init__(self,
                     sphereRadius,
                     dipoleDistance,
                     dipolePosition_numberOfPoints):
            self.sphereRadius = sphereRadius
            self.dipoleDistance = dipoleDistance
            self.dipolePosition_numberOfPoints = dipolePosition_numberOfPoints


        def getPositions(self):

            
            # the radial coordinate of the dipole position:
            dipolePosition_r = self.sphereRadius + self.dipoleDistance
            # dipole positions, evenly distributed on a sphere aroung the spherical
            # particle, 3xN array:
            dipolePosition = np.repeat(np.array([[dipolePosition_r], [0 * np.pi/180], [0 * np.pi/180]]), self.dipolePosition_numberOfPoints, axis=1)
                
            return dipolePosition
        
    class sideX:
        # This class if for getting the polarized far field PSF-s for 

        def __init__(self,
                     sphereRadius,
                     dipoleDistance,
                     dipolePosition_numberOfPoints):
            self.sphereRadius = sphereRadius
            self.dipoleDistance = dipoleDistance
            self.dipolePosition_numberOfPoints = dipolePosition_numberOfPoints


        def getPositions(self):

            
            # the radial coordinate of the dipole position:
            dipolePosition_r = self.sphereRadius + self.dipoleDistance
            # dipole positions, evenly distributed on a sphere aroung the spherical
            # particle, 3xN array:
            dipolePosition = np.repeat(np.array([[dipolePosition_r], [90 * np.pi/180], [0 * np.pi/180]]), self.dipolePosition_numberOfPoints, axis=1)
                
            return dipolePosition
        
    class sideY:
        # This class if for getting the polarized far field PSF-s for 

        def __init__(self,
                     sphereRadius,
                     dipoleDistance,
                     dipolePosition_numberOfPoints):
            self.sphereRadius = sphereRadius
            self.dipoleDistance = dipoleDistance
            self.dipolePosition_numberOfPoints = dipolePosition_numberOfPoints


        def getPositions(self):

            
            # the radial coordinate of the dipole position:
            dipolePosition_r = self.sphereRadius + self.dipoleDistance
            # dipole positions, evenly distributed on a sphere aroung the spherical
            # particle, 3xN array:
            dipolePosition = np.repeat(np.array([[dipolePosition_r], [90 * np.pi/180], [90 * np.pi/180]]), self.dipolePosition_numberOfPoints, axis=1)
                
            return dipolePosition
        
class dipoleOrientation:
    # Object definitions defining the dipole orientations for the Mie calculation.

    class evenDistribution_4pi:
        # This object returns evenly distributed dipole orientations. Useful for
        # averagig the emission of rotationally free dipoles.

        def __init__(self,
                     dipoleVector_numberOfPoints):
            self.dipoleVector_numberOfPoints = dipoleVector_numberOfPoints


        def getOrientations(self):

            
            # dipole vectors, pointing to evenly distributed directions ( acquired
            # from the coordinates of points on unit sphere centered at the origo ),
            # 3xN array:
            dipoleVector = \
                pointDistributionOnSphere.getUnitOrientationVectors_r_theta_phi(
                   self.dipoleVector_numberOfPoints, "full sphere, even")
                
            return dipoleVector
        
    class uniformDistribution_4pi:
        # This object returns uniformly distributed dipole orientations. Useful for
        # Monte-Carlo simulation of rotationally free dipoles.

        def __init__(self,
                     dipoleVector_numberOfPoints):
            self.dipoleVector_numberOfPoints = dipoleVector_numberOfPoints


        def getOrientations(self):

            
            # dipole vectors, pointing to evenly distributed directions ( acquired
            # from the coordinates of points on unit sphere centered at the origo ),
            # 3xN array:
            dipoleVector = \
                pointDistributionOnSphere.getUnitOrientationVectors_r_theta_phi(
                   self.dipoleVector_numberOfPoints, "full sphere, uniform")
                
            return dipoleVector
    
    class arbitrary:
        # This object returns dipole orientations with user user set spherical
        # angles. 

        def __init__(self,
                     dipoleVector_numberOfPoints,
                     theta,
                     phi):
            self.dipoleVector_numberOfPoints = dipoleVector_numberOfPoints
            self.theta = theta
            self.phi = phi


        def getOrientations(self):

            
            # dipole vectors, pointing to evenly distributed directions ( acquired
            # from the coordinates of points on unit sphere centered at the origo ),
            # 3xN array:
            dipoleVector = \
                pointDistributionOnSphere.getUnitOrientationVectorsFromAngles(self.theta * self.dipoleVector_numberOfPoints, self.phi * self.dipoleVector_numberOfPoints)
            
            
            return dipoleVector
    
    class perpendicular:
        # This object returns dipole orientations that are perpendicular to the
        # nanoparticle surface.

        def __init__(self, dipoleVector_numberOfPoints):
            self.dipoleVector_numberOfPoints = dipoleVector_numberOfPoints


        def getOrientations(self):

            
            # dipole vectors, pointing to evenly distributed directions ( acquired
            # from the coordinates of points on unit sphere centered at the origo ),
            # 3xN array:
            dipoleVector = \
                pointDistributionOnSphere.getUnitOrientationVectors_r_theta_phi(self.dipoleVector_numberOfPoints, "radial unit vector")
            
            
            return dipoleVector
        
    class parallel_theta:
        # This object returns dipole orientations that are parallel to the
        # nanoparticle surface and to the theta unit vector, i.e. pointing
        # "upwards" (has paralellel component to the octical axis).

        def __init__(self, dipoleVector_numberOfPoints):
            self.dipoleVector_numberOfPoints = dipoleVector_numberOfPoints


        def getOrientations(self):

            
            # dipole vectors, pointing to evenly distributed directions ( acquired
            # from the coordinates of points on unit sphere centered at the origo ),
            # 3xN array:
            dipoleVector = \
                pointDistributionOnSphere.getUnitOrientationVectors_r_theta_phi(self.dipoleVector_numberOfPoints, "theta unit vector")
            
            return dipoleVector
        
    class parallel_phi:
        # This object returns dipole orientations that are parallel to the
        # nanoparticle surface and to the phi unit vector, i.e. they are in the
        # horizontal plane.

        def __init__(self, dipoleVector_numberOfPoints):
            self.dipoleVector_numberOfPoints = dipoleVector_numberOfPoints


        def getOrientations(self):

            
            # dipole vectors, pointing to evenly distributed directions ( acquired
            # from the coordinates of points on unit sphere centered at the origo ),
            # 3xN array:
            dipoleVector = \
                pointDistributionOnSphere.getUnitOrientationVectors_r_theta_phi(self.dipoleVector_numberOfPoints, "phi unit vector")
            
            return dipoleVector
        
    class tilting_theta:
        
        def __init__(self,
                     tiltingNumber):
            self.tiltingNumber = tiltingNumber
            self.tiltingIndex = 0
            self.dipoleVector_numberOfPoints = 1
            
        def getOrientations(self):

            
            # dipole vectors, pointing to evenly distributed directions ( acquired
            # from the coordinates of points on unit sphere centered at the origo ),
            # 3xN array:
            theta = 0+(np.pi/2-0)/(self.tiltingNumber-1) * self.tiltingIndex
            phi = 0
            dipoleVector = \
                pointDistributionOnSphere.getUnitOrientationVectorsFromAngles(theta, phi)
            
            self.tiltingIndex += 1
            
            return dipoleVector
        
    class tilting_phi:
        
        def __init__(self,
                     tiltingNumber):
            self.tiltingNumber = tiltingNumber
            self.tiltingIndex = 0
            self.dipoleVector_numberOfPoints = 1
            
        def getOrientations(self):

            
            # dipole vectors, pointing to evenly distributed directions ( acquired
            # from the coordinates of points on unit sphere centered at the origo ),
            # 3xN array:
            theta = 0+(np.pi/2-0)/(self.tiltingNumber-1) * self.tiltingIndex
            phi = 90 * np.pi/180
            dipoleVector = \
                pointDistributionOnSphere.getUnitOrientationVectorsFromAngles(theta, phi)
            
            self.tiltingIndex += 1
            
            return dipoleVector
