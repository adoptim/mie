import math as math
import numpy as np
from scipy import special

import Mie_calculation.auxiliary.Mie_physicalConstants as Mie_physicalConstants

import Mie_calculation.Mie_coefficients as Mie_coefficients

import Mie_calculation.auxiliary.Mie_summationAccessaries as Mie_summationAccessaries
import Mie_calculation.auxiliary.sphericalFunctions as sphericalFunctions

from Mie_calculation.Mie_absorptionIntegrals import absorptionIntegrals

# TODO: this is currently unused and is propably outdated
# from Mie_calculation.auxilary.sphericalDotProduct import sphericalDot


def Mie_dipoleEmitter_rates(
    emissionWavelength,
    R,
    dipolePosition_sphericalComponents,
    dipoleVector,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    n_max
):
    # This function calculates the scattering/absorption rate  of a
    # dipole placed near a sphere (dielectric or conductive) compared
    # to the free space dipole radiation rate

    # calculationType = "absorption integral"
    # calculationType = "absorption integral, special cases"
    calculationType = "Green formalism"

    # CONSTANTS

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    sigma_conductivity = Mie_physicalConstants.conductivity(c_0, k_0, epsilon_0, refractiveIndex_sphere)

    # polar and azimuth angles of the dipole position vector:
    theta = dipolePosition_sphericalComponents[1, :]
    fi = dipolePosition_sphericalComponents[2, :]

    # dimenionless sizes:
    r = dipolePosition_sphericalComponents[0, :]
    kr = k_medium * r
    kR = k_medium * R

    # dipole emission power in free space:
    W_R0 = freeSpaceRadiationRate(k_medium, c_0, mu_0, dipoleVector)

    # RATE CALCULATION

    j_vect, j_der_vect = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_vect, h_der_vect = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_vect, j_der_vect)

    # sequence of associated Legendre polynomials and their derivates:
    [P_sequence, P_m_per_sinTheta_sequence, P_der_sequence] = sphericalFunctions.associatedLegendrePolynomials(n_max, theta)

    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    if(calculationType == "absorption integral" or calculationType == "absorption integral, special cases"):
        int_absorbed_sequence = absorptionIntegrals(n_max, R, k_sphere)

    totalPower = 0
    radiationPower = 0
    absorptionPower = 0

    radiativePower_free = 0

    indexCount_n = 0
    linearIndex_n = 0
    indexCount_n_m = 0
    linearIndex_n_m = 0
    indexCount_n_m_sigma = 0
    linearIndex_n_m_sigma = 0
    for n in range(1, n_max + 1):
        radiationPower_n = 0
        absorptionPower_n = 0

        indexCount_n += 1
        linearIndex_n = indexCount_n-1

        # get the Mie coefficients
        [a_n, b_n, alpha_n, beta_n] = Mie_coefficients.Mie_coefficients(kR, refr_rel, n)

        j_n = j_vect[:, linearIndex_n]
        j_n_der = j_der_vect[:, linearIndex_n]
        h_n = h_vect[:, linearIndex_n]
        h_n_der = h_der_vect[:, linearIndex_n]

        if(calculationType == "absorption integral" or calculationType == "absorption integral, special cases"):
            absoptionIntegralIndex_n = linearIndex_n+1;
            int_absorbed_n__1 = int_absorbed_sequence[absoptionIntegralIndex_n-1]
            int_absorbed_n = int_absorbed_sequence[absoptionIntegralIndex_n]
            int_absorbed_n_1 = int_absorbed_sequence[absoptionIntegralIndex_n+1]

        for m in range(0, n + 1):
            indexCount_n_m += 1
            linearIndex_n_m = indexCount_n_m-1

            # getting the required associated Legendre polynomials:
            P_m_n = P_sequence[:, linearIndex_n_m]
            P_m_per_sinTheta_m_n = P_m_per_sinTheta_sequence[:, linearIndex_n_m]
            P_m_n_der = P_der_sequence[:, linearIndex_n_m]

            for sigma in range(1, 3):
                indexCount_n_m_sigma += 1
                linearIndex_n_m_sigma = indexCount_n_m_sigma-1

                D_sigma_m_n = D_sequence[linearIndex_n_m_sigma]

                s_sigma_m_n, t_sigma_m_n, p_sigma_m_n, q_sigma_m_n = \
                    Mie_coefficients.expansionCoefficients_dipoleField(
                        k_medium, epsilon_0, n, m, sigma, kr, theta, fi,
                        j_n, j_n_der, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der,
                        dipoleVector)

                # expansion coefficients of the scattered field:
                u_sigma_m_n = a_n * p_sigma_m_n
                v_sigma_m_n = b_n * q_sigma_m_n
                # expansion coefficients of the transmitted field:
                f_sigma_m_n = alpha_n * p_sigma_m_n
                g_sigma_m_n = beta_n * q_sigma_m_n

                radiationPower_n += (
                    np.sqrt(epsilon_0 / mu_0) * np.pi / k_medium ** 2
                ) * 1 / 2 * D_sigma_m_n * (
                    np.abs(s_sigma_m_n + u_sigma_m_n) ** 2
                    + np.abs(t_sigma_m_n + v_sigma_m_n) ** 2
                )

                if(calculationType == "absorption integral"):
                    absorptionPower_n +=\
                        np.pi * sigma_conductivity / 8 * 4 * D_sigma_m_n * \
                        (np.abs(f_sigma_m_n) ** 2 * int_absorbed_n \
                        + 1 / (2 * n + 1) * np.abs(g_sigma_m_n) ** 2 \
                        * ((n + 1) * int_absorbed_n__1 + n * int_absorbed_n_1)
                        )

        # derivatives of Riccati-Bessel functions
        # denoted as Greek "zeta" in Merstens(2007) and as Greek "xi" in
        # Wikipedia ("zeta" denotes something else in the Wikipedia)
        zeta_n_der = h_n + kr * h_n_der

        if(calculationType == "Green formalism"):
            totalPower_perpendicular_n = W_R0 * \
                3 / 2 * (2 * n + 1) * n * (n + 1) * \
                np.real( b_n * (h_n / kr) ** 2 )
            totalPower_parallel_n = W_R0 * \
                3 / 2 * np.real( (n + 1 / 2) * \
                (b_n * (zeta_n_der / kr) ** 2 + a_n * (h_n) ** 2 )
                )
                    
            c_perpendicular = np.zeros([1, np.size(dipoleVector, axis=1)])
            c_perpendicular[0,:] = dipoleVector[0]**2
            c_parallel = np.zeros([1, np.size(dipoleVector, axis=1)])
            c_parallel[0,:] = dipoleVector[1]**2 + dipoleVector[2]**2
            totalPower_n = c_perpendicular * totalPower_perpendicular_n + c_parallel * totalPower_parallel_n

            if False:
                # get the Mie coefficients with relative refractive index of 1 (free space radiation):
                [a_n_free, b_n_free, alpha_n_free, beta_n_free] = Mie_coefficients.Mie_coefficients(kR, 1, n)

                # derivatives of Riccati-Bessel functions
                # denoted as Greek "zeta" in Merstens(2007) and as Greek "xi" in
                # Wikipedia ("zeta" denotes something else in the Wikipedia)
                zeta_n_der = h_n + kr * h_n_der
                # denoted as Greek "psi" Merstens(2007) and as "S" in Wikipedia
                psi_n_der = j_n + kr * j_n_der

                radiativePower_perpendicular_n_free = W_R0 * \
                    3 / 2 * (2 * n + 1) * n * (n + 1) * \
                    np.abs((j_n + b_n_free * h_n) / kr) ** 2

                radiativePower_parallel_n_free = W_R0 * \
                    3 / 4 * (2 * n + 1) * \
                    (np.abs(j_n + a_n_free * h_n) ** 2 + np.abs((psi_n_der + b_n_free * zeta_n_der ) / kr) ** 2 )

                radiativePower_n_free = c_perpendicular * radiativePower_perpendicular_n_free + c_parallel * radiativePower_parallel_n_free

                radiativePower_free += radiativePower_n_free

                totalPower_n += radiativePower_n_free

            else:
                if n == 1:

                    # TODO: the contribution of the free radiation should be addded (distributed amond the different "n" indices)
                    totalPower_n += 1 * W_R0

            totalPower += totalPower_n
            radiationPower += radiationPower_n
            absorptionPower += totalPower_n - radiationPower_n

        elif(calculationType == "absorption integral"):
            totalPower_n = radiationPower_n[0] + absorptionPower_n[0]

            totalPower += totalPower_n
            radiationPower += radiationPower_n[0]
            absorptionPower += absorptionPower_n[0]

        elif(calculationType == "absorption integral, special cases"):

            # derivatives of Riccati-Bessel functions
            # denoted as Greek "zeta" in Merstens(2007) and as Greek "xi" in
            # Wikipedia ("zeta" denotes something else in the Wikipedia)
            zeta_n_der = h_n + kr * h_n_der
            # denoted as Greek "psi" Merstens(2007) and as "S" in Wikipedia
            psi_n_der = j_n + kr * j_n_der

            radiativePower_perpendicular_n = W_R0 * \
                3 / 2 * (2 * n + 1) * n * (n + 1) * \
                np.abs((j_n + b_n * h_n) / kr) ** 2

            absorptionPower_perpendicular_n = W_R0 * \
            3 / 2 * np.sqrt(mu_0/epsilon_0) *sigma_conductivity * \
            ( n * ( n + 1 ) * np.abs( beta_n * h_n)**2 / r**2 * \
            ((n+1) * int_absorbed_n__1 + n * int_absorbed_n_1) )

            radiativePower_parallel_n = W_R0 * \
                3 / 4 * (2 * n + 1) * \
                ( np.abs(j_n + a_n * h_n) ** 2 + np.abs( ( psi_n_der + b_n * zeta_n_der ) / kr) ** 2 )

            # there is a typo in the Ruppin, 1982 paper, (kr * h_n) -> (kr * h_n)' = zeta_n_der
            absorptionPower_parallel_n = W_R0 * \
            3 / 4 * np.sqrt(mu_0/epsilon_0) * sigma_conductivity * k_medium**2 * (2 * n + 1 ) * \
            ( np.abs( alpha_n * h_n)**2 * int_absorbed_n + np.abs(beta_n/kr*(zeta_n_der))**2 * 1 / (2 * n + 1) * \
            ( (n+1) * int_absorbed_n__1 + n * int_absorbed_n_1) )

            totalPower_perpendicular_n = radiativePower_perpendicular_n + absorptionPower_perpendicular_n
            totalPower_parallel_n = radiativePower_parallel_n + absorptionPower_parallel_n
            
            c_perpendicular = np.zeros([1, np.size(dipoleVector, axis=1)])
            c_perpendicular[0,:] = dipoleVector[0]**2
            c_parallel = np.zeros([1, np.size(dipoleVector, axis=1)])
            c_parallel[0,:] = dipoleVector[1]**2 + dipoleVector[2]**2
            totalPower_n = c_perpendicular * totalPower_perpendicular_n + c_parallel * totalPower_parallel_n

            totalPower += totalPower_n
            radiationPower += radiationPower_n
            absorptionPower += totalPower_n - radiationPower_n

    # the relative rates:
    absorptionRate = np.transpose(absorptionPower / W_R0)
    scatteringRate = np.transpose(radiationPower / W_R0)
    totalRate = np.transpose(totalPower / W_R0)

    # return absorption, scattering and total (Purcell factor) rates relative
    # to the free space emission rate:
    return absorptionRate, scatteringRate, totalRate


def freeSpaceRadiationRate(k_medium, c_0, mu_0, dipoleVector):

    W_R0 = np.zeros([1, np.size(dipoleVector, axis=1)])

    W_R0[0,:] = (
        (k_medium * c_0) ** 4 / 12 / np.pi * mu_0 / c_0
        * np.transpose(np.sum(dipoleVector * np.conjugate(dipoleVector), axis = 0))
        )

    # incorrect formula (Ruppin 1982):
    # omega = k_0 * c_0
    # W_R0 = omega ** 4 / 12 / np.pi * mu_0 / c_0
    # * np.matmul(np.transpose(dipoleVector), np.conjugate(dipoleVector))

    return W_R0
