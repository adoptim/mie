import numpy as np


def coverslipCropping_sphericalCoodinates(dipolePositions, sphereRadius, dipoleDistance):
   # This function discards the dipole positions that lie within the cover slip. The cover slip considered as the negative half-infinite space along the axis "z" starting at "-sphereRadius". It calculates the "theta" angle threshold above which the dipole positions (located at distance detemined by the linker length "dipoleDistance" from the nanosphere surface) and discard avery position that exceeds this. The supplied dipole positions must be in shperical coordinates ([r, thete, phi]).

    # calculate the angle threshold for the cropping:
    theta_cropping = np.pi-np.arccos(sphereRadius/(sphereRadius+dipoleDistance))

    # delete position that exceed the angle threshold:
    dipolePositions = np.delete(dipolePositions, dipolePositions[1, :] > theta_cropping, axis=1)

    # number of positions after the cropping:
    croppedPositionNumber = dipolePositions.shape[1]

    return dipolePositions, croppedPositionNumber


def axialConeCropping_sphericalVectors(orientationVectors, theta_cropping):
    # This function discards the dipole orientations within the cones along the "z" (or "e_r") axis facing the negative and positive directions. The orientation vectors must be in Descartes coordinates ([x, y, z]), or in the bases of the spherical unit vector [e_r, e_theta, e_phi]
    
    # the "theta" angles of the orientation vectors:
    theta  = np.arctan2(np.sqrt(orientationVectors[1, :]**2+orientationVectors[2, :]**2), orientationVectors[0, :])
    
    # the lower angle bound of the cropping below which the orientations should be discarded:
    theta_cropping_lower = theta_cropping
    # the upper angle bound of the cropping above which the orientations should be discarded:
    theta_cropping_upper = np.pi-theta_cropping

    # delete orientation vectors that exceed the angle thresholds:
    orientationVectors = np.delete(orientationVectors, (theta < theta_cropping_lower) | (theta > theta_cropping_upper), axis=1)

    # number of positions after the cropping:
    croppedPositionNumber = orientationVectors.shape[1]

    return orientationVectors, croppedPositionNumber
