import numpy as np


def electromagnetic():

    # electromagnetic constants of the vacuum in H/m, in F/m and in m/s,
    # respectively:
    mu_0 = 1.25663706212 * 10 ** -6
    epsilon_0 = 8.8541878128 * 10 ** -12
    c_0 = 1 / np.sqrt(epsilon_0 * mu_0)

    return mu_0, epsilon_0, c_0


def optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium):

    # relative refractive index of the sphere and the embedding medium:
    refr_rel = refractiveIndex_sphere / refractiveIndex_medium

    # vacuum wave number of the dipole radiation in 1/m:
    k_0 = 2 * np.pi / emissionWavelength

    # wave numbers inside the sphere and in the embedding medium:
    k_medium = refractiveIndex_medium * k_0
    k_sphere = refractiveIndex_sphere * k_0

    # electromagnetic constants of the embedding medium:
    mu_medium = 1
    epsilon_medium = refractiveIndex_medium ** 2 / mu_medium

    return k_0, refr_rel, k_medium, k_sphere


def conductivity(c_0, k_0, epsilon_0, refractiveIndex_sphere):

    # electromagnetic constants of the sphere:
    mu_sphere = 1
    epsilon_sphere = refractiveIndex_sphere ** 2 / mu_sphere

    # conductivity of the sphere:
    sigma_conductivity = np.imag(epsilon_sphere) * c_0 * k_0 * epsilon_0

    return sigma_conductivity
