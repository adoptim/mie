import numpy as np


def sphericalCoordinatesToDescartes(positionVector_spherical):
    # This function converts a vector (3xN) from spherical coordinates into
    # Descartes coordinates.

    # x, y and z coordinates, respectively:
    positionVector_Descartes=np.array([
    positionVector_spherical[0,:]*np.sin(positionVector_spherical[1,:])*np.cos(positionVector_spherical[2,:]),
    positionVector_spherical[0,:]*np.sin(positionVector_spherical[1,:])*np.sin(positionVector_spherical[2,:]),
    positionVector_spherical[0,:]*np.cos(positionVector_spherical[1,:])
    ])

    return positionVector_Descartes


def DescartesCoordinatesToSpherical(positionVector_Descartes):
    # This function convert a vector (3xN) from Descartes coordinates into
    # spherical coordinates.

    r=np.sqrt(positionVector_Descartes[0,:]**2+positionVector_Descartes[1,:]**2+positionVector_Descartes[2,:]**2)

    # pi in [0, 2*pi] range
    phi = np.arctan2(positionVector_Descartes[1,:], positionVector_Descartes[0,:])
    phi[phi<0]=2*np.pi+phi[phi<0]

    # the r, theta and phi coordinates, respectively:
    positionVector_spherical=np.array([
    r,
    np.arccos(positionVector_Descartes[2,:]/r),
    phi,
    ])

    return positionVector_spherical;

def getRotationMatrix_X(alpha):
    # This function returns the rotation matrix of "alpha" angle around the axis "x".
    # https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations
    # conventions:
    # - right handed coordinate system
    # - viewing from the positive half of the coordinate system
    # - counter clockwise rotation
    
    # matrix of rotation around the "x" axis with alpha angle
    rotationMatrix=np.array([
    [1, 0, 0],
    [0, np.cos(alpha), -np.sin(alpha)],
    [0, np.sin(alpha), np.cos(alpha)]
    ])
    
    return rotationMatrix

def getRotationMatrix_Y(alpha):
    # This function returns the rotation matrix of "alpha" angle around the axis "y".
    # https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations
    # conventions:
    # - right handed coordinate system
    # - viewing from the positive half of the coordinate system
    # - counter clockwise rotation
    
    # matrix of rotation around the "y" axis with alpha angle
    rotationMatrix=np.array([
    [np.cos(alpha), 0, np.sin(alpha)],
    [0, 1, 0],
    [-np.sin(alpha), 0, np.cos(alpha)]
    ])
    
    return rotationMatrix
    
def getRotationMatrix_Z(alpha):
    # This function returns the rotation matrix of "alpha" angle around the axis "z".
    # https://en.wikipedia.org/wiki/Rotation_matrix#Basic_rotations
    # conventions:
    # - right handed coordinate system
    # - viewing from the positive half of the coordinate system
    # - counter clockwise rotation
    
    # matrix of rotation around the "z" axis with alpha angle
    rotationMatrix=np.array([
    [np.cos(alpha), -np.sin(alpha), 0],
    [np.sin(alpha), np.cos(alpha), 0],
    [0, 0, 1]
    ])
    
    return rotationMatrix


def getRotationMatrix(theta, phi):
    # TODO!!
    
    # rotation matrix of rotating around the "z" axis with phi angle
    Z_rotation=np.array([
    [np.cos(phi), np.sin(phi), 0],
    [-np.sin(phi), np.cos(phi), 0],
    [0, 0, 1]
    ])
    
    # rotation matrix of rotating around the "z" axis with phi angle
    Z_InverseRotation=np.array([
    [np.cos(-phi), -np.sin(phi), 0],
    [np.sin(phi), np.cos(phi), 0],
    [0, 0, 1]
    ])

    # rotation matrix of rotating around the "y" axis with theta angle
    Y_rotation=np.array([
    [np.cos(theta), 0, np.sin(theta)],
    [0, 1, 0],
    [-np.sin(theta), 0, np.cos(theta)]
    ])

    # rotation matrix of first rotating around the "z" axis with phi angle and then rotation around the "y" axis with theta angle
    """ZY_rotation=np.array([
    [np.cos(theta)*np.cos(phi), np.cos(theta)*np.sin(phi), np.sin(theta)],
    [-np.sin(phi), np.cos(phi), 0],
    [-np.sin(theta)*np.cos(phi), -np.sin(theta)*np.sin(phi), np.cos(theta)]
    ])"""
    
    rotationMatrix = np.matmul(Z_InverseRotation, np.matmul(Y_rotation, Z_rotation))
    
    return rotationMatrix
    
    

def rotateSphericalCoordinates(positionVector_spherical, theta, phi):
    # This function rotates the position vector given in spherical coordinates TODO!!!!!!!!!!
    # in what order??? does the order matter??????

    # convert to Descartes
    positionVector_Descartes = sphericalCoordinatesToDescartes(positionVector_spherical)

    rotationMatrix = getRotationMatrix(theta, phi)

    # TODO: the phi rotation has no effect
    rotatedVector_Descartes = np.matmul(rotationMatrix, positionVector_Descartes)

    rotatedVector_spherical = DescartesCoordinatesToSpherical(rotatedVector_Descartes)

    return rotatedVector_spherical

def rotateY_sphericalComponents(electricField, phi):
    # TODO
    
    conversionMatrix=np.array([
    [1, 0, 0],
    [0, np.cos(phi), -np.sin(phi)],
    [0, np.sin(phi), np.cos(phi)]
    ])
    
    electricField_rotated = np.matmul(electricField, conversionMatrix)
    
    return electricField_rotated


def sphericalComponentsToDescartes(positionVector_spherical, vector_sphericalBasis):
    # This function converts a vector given in spherical unit vectors
    # (e_r, e_theta, e_phi) at position defined by spherical coordinates
    # [r, theta, phi] to Descartes vector with basis of (e_x, e_y, e_z).
    # It is useful for converting the electric filed vector calculated in
    # spherical coordinate system to Descartes coordinate system.
    
    # The "T" matrix of the coordinate transformation vector "V",
    # V_Descartes = T * V_spherical:
    # T = np.array([
    #     [np.sin(theta) * np.cos(phi), np.cos(theta) * np.cos(phi), (-np.sin(phi))],
    #     [np.sin(theta) * np.sin(phi), np.cos(theta) * np.sin(phi), np.cos(phi)],
    #     [np.cos(theta), -np.sin(theta), 0]
    #     ]) = np.array([e_x_spherical, e_y_spherical, e_z_spherical]),
    # where e_x_spherical, e_y_spherical and e_z_spherical row vectors are the
    # Descates unit vectors expressed with the spherical unit vectors
    # the colums are the components of the spherical unit vectors in Descartes
    # coordinates
    
    # r = np.ones(positionVector_spherical.shape[0])
    theta = positionVector_spherical[1, :]
    phi = positionVector_spherical[2, :]
    
    # Nx3 vectors used for converting the given vector in spherical bais to Descartes basis:
    # the "x" unit vector expressed with the "r", "theta" and "phi" spherical unit vectors:
    e_x_spherical = np.stack(
            (
                np.sin(theta) * np.cos(phi),
                np.cos(theta) * np.cos(phi),
                (-np.sin(phi))
            ),
            axis = -1)
    # the "y" unit vector expressed with the "r", "theta" and "phi" spherical unit vectors:
    e_y_spherical = np.stack(
            (
                np.sin(theta) * np.sin(phi),
                np.cos(theta) * np.sin(phi),
                np.cos(phi)
            ),
            axis = -1)
    # the "z" unit vector expressed with the "r", "theta" and "phi" spherical unit vectors:
    e_z_spherical = np.stack(
            (
                np.cos(theta),
                -np.sin(theta),
                np.zeros(theta.shape)
            ),
            axis = -1)

    # the conversion of the vector from the spherical basis to the Descartes basis:
    vector_DescartesBasis = np.zeros(vector_sphericalBasis.shape, vector_sphericalBasis.dtype)
    vector_DescartesBasis[:, 0] = np.sum(vector_sphericalBasis * e_x_spherical, axis=1)
    vector_DescartesBasis[:, 1] = np.sum(vector_sphericalBasis * e_y_spherical, axis=1)
    vector_DescartesBasis[:, 2] = np.sum(vector_sphericalBasis * e_z_spherical, axis=1)
    
    return vector_DescartesBasis

def DescartesComponentsToSpherical(positionVector_spherical, vector_DescartesBasis):
    # This function converts a vector given in Descartes unit vectors
    # (e_x, e_y, e_z) at position defined by spherical coordinates
    # [r, theta, phi] to spherical vector with basis of
    # (e_r, e_theta, e_phi).
    
    # The "T_inverse" matrix of the coordinate transformation vector "V",
    # V_spherical = T_inverse * V_Descartes:
    # T_inverse = np.array([
    #     [np.sin(theta) * np.cos(phi), np.sin(theta) * np.sin(phi), np.cos(theta)], 
    #     [np.cos(theta) * np.cos(phi), np.cos(theta) * np.sin(phi), -np.sin(theta)], 
    #     [ - np.sin(phi),  np.cos(phi), 0]
    #     ]) = np.array([e_r_Descartes, e_theta_Descartes, e_phi_Descartes]),
    # where e_r_Descartes, e_theta_Descartes and e_phi_Descartes row vectors
    # are the spherical unit vectors expressed with the Descartes unit vectors
    # the colums are the components of the Descartes unit vectors in spherical
    # coordinates

    # r = np.ones(positionVector_spherical.shape[0])
    theta = positionVector_spherical[1, :]
    phi = positionVector_spherical[2, :]
    
    # Nx3 vectors used for converting the given vector in Descartes bais to spherical basis:
    # the "r" unit vector expressed with the "x", "y" and "z" Descartes unit vectors:
    e_r_Descartes = np.stack(
            (
                np.sin(theta) * np.cos(phi),
                np.sin(theta) * np.sin(phi),
                np.cos(theta)
            ),
            axis = -1)
    # the "theta" unit vector expressed with the "x", "y" and "z" Descartes unit vectors:
    e_theta_Descartes = np.stack(
            (
                np.cos(theta) * np.cos(phi),
                np.cos(theta) * np.sin(phi),
                -np.sin(theta)
            ),
            axis = -1)
    # the "phi" unit vector expressed with the "x", "y" and "z" Descartes unit vectors:
    e_phi_Descartes = np.stack(
            (
                - np.sin(phi),
                np.cos(phi),
                np.zeros(theta.shape)
            ),
            axis = -1)

    # the conversion of the vector from the Descartes basis to the spherical basis:
    vector_sphericalBasis = np.zeros(vector_DescartesBasis.shape, vector_DescartesBasis.dtype)
    vector_sphericalBasis[:, 0] = np.sum(vector_DescartesBasis * e_r_Descartes, axis=1)
    vector_sphericalBasis[:, 1] = np.sum(vector_DescartesBasis * e_theta_Descartes, axis=1)
    vector_sphericalBasis[:, 2] = np.sum(vector_DescartesBasis * e_phi_Descartes, axis=1)
    
    return vector_sphericalBasis

def rotateElectricField(positionVector_spherical, electricField_spherical, theta, phi, positionVector_rotated_spherical):
    

    # TODO
    rotationMatrix = np.transpose(getRotationMatrix(theta, phi))

    electricField_Descartes = sphericalComponentsToDescartes(positionVector_spherical, electricField_spherical)

    electricField_rotated_Descates = np.matmul(electricField_Descartes, rotationMatrix)

    electricField_rotated_spherical = DescartesComponentsToSpherical(positionVector_rotated_spherical, electricField_rotated_Descates)
    
    return electricField_rotated_spherical


def rotateCoordinates_Y_spherical(positionVector_spherical, rotationAngle):
    
    positionVector_Descartes = sphericalCoordinatesToDescartes(positionVector_spherical)
    
    rotationMatrix_Y = getRotationMatrix_Y(rotationAngle)
    
    positionVector_rotated_Descartes = np.matmul(rotationMatrix_Y, positionVector_Descartes)
    
    positionVector_rotated_spherical = DescartesCoordinatesToSpherical(positionVector_rotated_Descartes)
    
    return positionVector_rotated_spherical
    
    

def rotateVectorField_Y_spherical(positionVector_spherical, vectorField_spherical, rotationAngle, positionVector_rotated_spherical):
    # the positionsarray should be a 3xN dimensions one, where N is the number of sampling points
    # the vector field array should be a Nx3 dimensions one, where N is the number of sampling points
    
    vectorField_Descartes = sphericalComponentsToDescartes(positionVector_spherical, vectorField_spherical)
    
    rotationMatrix_Y_transposed = np.transpose(getRotationMatrix_Y(rotationAngle))
    
    vectorField_rotated_Descartes = np.matmul(vectorField_Descartes, rotationMatrix_Y_transposed)

    vectorField_rotated_spherical = DescartesComponentsToSpherical(positionVector_rotated_spherical, vectorField_rotated_Descartes)
    
    return vectorField_rotated_spherical
