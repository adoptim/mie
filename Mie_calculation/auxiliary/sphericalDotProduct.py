import numpy as np

def sphericalDot(vect1, vect2):
	r_1=vect1[0]
	r_2=vect2[0]
	theta_1=vect1[1]
	theta_2=vect2[1]
	fi_1=vect1[2]
	fi_2=vect2[2]
	
	
	
	dotProduct=r_1*r_2*(np.cos(fi_1-fi_2)*np.sin(theta_1)*np.sin(theta_2)+np.cos(theta_1)*np.cos(theta_2))
	
	print(vect1)
	print(dotProduct)
	print()
	
	return dotProduct
