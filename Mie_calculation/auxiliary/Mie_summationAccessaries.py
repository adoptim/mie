import numpy as np
import math as math

import warnings

def indexNumber_n(n_max):
    # The total number of n indices when n goes from 1 to n_max.

    linearIndexNumber = n_max;
    return linearIndexNumber


def indexMapping_n(n):
    # Convert the n index to the linear index calculated as:
    """
    indexCount = 0
    linearIndex = 0
    for n in range(1, n_max+1):
        indexCount++
        linearIndex = indexCount-1
    """

    linearIndex = n-1
    return linearIndex


def indexNumber_n_m(n_max):
    # The total number of n,m index combinations when n goes from 1 to
    # n_max and m goes from 0 to n.

    temp = (2+(n_max+1))/2*n_max
    
    if temp%1 != 0:
        warnings.warn("The calcualted index is not integer. Check it!")
    
    linearIndexNumber = int(temp);
    return linearIndexNumber


def indexMapping_n_m(n, m):
    # Convert the n,m index combination to the linear index
    # calculated as:
    """
    indexCount = 0
    linearIndex = 0
    for n in range(1, n_max+1):
        for m in range(0, n+1):
            indexCount++
            linearIndex = indexCount-1
    """

    linearIndex = (indexNumber_n_m(n-1)+(1+m)-1)
    return linearIndex


def indexNumber_n_m_sigma(n_max):
    # The total number of n,m,sigma index combinations when n goes from 1 to
    # n_max, m goes from 0 to n and sigma can be either 1 or 2.

    temp = (2+(n_max+1))/2*n_max*2
    
    if temp%1 != 0:
        warnings.warn("The calcualted index is not integer. Check it!")
    
    linearIndexNumber = int(temp);
    return linearIndexNumber


def indexMapping_n_m_sigma(n, m, sigma):
    # Convert the n,m,sigma index combination to the linear index
    # calculated as:
    """
    indexCount = 0
    linearIndex = 0
    for n in range(1, n_max+1):
        for m in range(0, n+1):
            for sigma in range(1, 3):
                indexCount++
                linearIndex = indexCount-1
    """

    linearIndex = (indexNumber_n_m_sigma(n-1)+(m*2+1)+(sigma-1)-1)
    return linearIndex

def get_D_sequence(n_max):
    # This function calculates a constant, only summation index dependent, multiplication factor for the summation of the dipole field calculation.
    
    indexNumber = indexNumber_n_m_sigma(n_max)
    D_sequence = np.zeros([indexNumber])

    indexCount = 0
    linearIndex = 0
    for n in range(1, n_max+1):
        for m in range(0, n+1):
            
            if m == 0:
                epsilon_m = 1
            elif m > 0:
                epsilon_m = 2
                
            for sigma in range(1, 3):
                indexCount += 1
                linearIndex = indexCount-1

                D_sequence[linearIndex] = (
                    epsilon_m
                    * ((2 * n + 1) * math.factorial(n - m))
                    / (4 * n * (n + 1) * math.factorial(n + m))
                )

    return D_sequence
