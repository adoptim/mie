import numpy as np
from scipy import special

import Mie_calculation.auxiliary.Mie_summationAccessaries as Mie_summationAccessaries

def sphericalBessel_firstOrder(n_max, x):
    # This functions returns an NxV array of first order Bessel functions and their derivates.
    # The first dimension (N) corresponds to the argument dimension, which needs to be a 1D aray.
    # The second dimension (V) corresponds to the "index" of the Bessel function (starting from 1 and going to n_max).
    
    j_array = np.zeros([x.size, n_max])
    j_der_array = np.zeros([x.size, n_max])

    for n in range(1, n_max + 1):
        index=Mie_summationAccessaries.indexMapping_n(n)
        
        j_array[:, index] = special.spherical_jn(n, x, derivative=False)
        
        if n==1:
            j_n__1 = special.spherical_jn(0, x, derivative=False)
            j_der_array[:, index] = j_n__1-(n+1)/x*j_array[:, index]
        else:
            j_der_array[:, index] = j_array[:, index-1]-(n+1)/x*j_array[:, index]
        
        # alternative calculation:
        #j_der_array[:, index] = special.spherical_jn(n, x, derivative=True)

    return j_array, j_der_array
    
    
def sphericalHankel_firstOrder(n_max, x, j_array, j_der_array):
    # This functions returns an NxV array of first order Hankel functions and their derivates.
    # The first dimension (N) corresponds ro the argument dimension, which needs to be a 1D aray.
    # The second dimension (V) corresponds to the "index" of the Hankel function (starting from 1 and going to n_max).
    
    h_array=np.zeros([x.size, n_max], dtype=np.cdouble)
    h_der_array=np.zeros([x.size, n_max], dtype=np.cdouble)
    
    y_n = special.spherical_yn(0, x, derivative=False)
    for n in range(1, n_max + 1):
        
        index=Mie_summationAccessaries.indexMapping_n(n)
        
        y_n__1 = y_n
        y_n = special.spherical_yn(n, x, derivative=False)
        
        y_n_der = y_n__1-(n+1)/x*y_n
        
        # alternative calculation:
        # y_n_der = special.spherical_yn(n, x, derivative=True)
        
        h_array[:, index] = j_array[:, index] + 1j * y_n
        h_der_array[:, index] = j_der_array[:, index] +1j * y_n_der
    
    return h_array, h_der_array

    
def associatedLegendrePolynomials(n_max, theta):
    # This functions returns an NxV array of associated Legendre polynomials and their derivates with respect to theta (instead of cos(theta)).
    # The second dimension (N) corresponds to the argument dimension, which needs to be a 1D aray.
    # The first dimension (V) corresponds to the "linearized  n and m index" of the associated Legendre polynomials (starting from 1 and going to n_max).
    
    # source: 
    # https://en.wikipedia.org/wiki/Associated_Legendre_polynomials#Recurrence_formula
    # https://www.mat.univie.ac.at/~westra/associatedlegendrefunctions.pdf
    
    indexNumber = Mie_summationAccessaries.indexNumber_n_m(n_max)

    P_array = np.zeros([theta.size, indexNumber])
    P_m_per_sinTheta_array = np.zeros([theta.size, indexNumber])
    P_der_array = np.zeros([theta.size, indexNumber])

    cos_theta=np.cos(theta)

    # get the array of the associated Legendre polynomials:
    indexCount = 0
    linearIndex = 0
    for n in range(1, n_max + 1):
        for m in range(0, n + 1):
            indexCount += 1
            linearIndex = indexCount-1
            
            P_array[:, linearIndex] = special.lpmv(m, n, cos_theta)

    # get the array of the  Legendre polynomials devided by sin(theta) using one of the recurrence formulas:
    indexCount = 0
    linearIndex = 0
    for n in range(1, n_max + 1):
        for m in range(0, n + 1):
            indexCount += 1
            linearIndex = indexCount-1

            if True:
                # wikipedia 4th recurrence relation
                
                if n==n_max:
                    P_m_1_n_1 = special.lpmv(m+1, n+1, cos_theta)
                else:
                    linearIndex_m_1_n_1 = Mie_summationAccessaries.indexMapping_n_m(n+1, m+1)
                    P_m_1_n_1 = P_array[:, linearIndex_m_1_n_1]

                if n==n_max:
                    P_m__1_n_1 = special.lpmv(m-1, n+1, cos_theta)
                elif m==0:
                    P_m__1_n_1 = special.lpmv(-1, n+1, cos_theta)
                else:
                    linearIndex_m__1_n_1 = Mie_summationAccessaries.indexMapping_n_m(n+1, m-1)
                    P_m__1_n_1 =  P_array[:, linearIndex_m__1_n_1]

                P_m_per_sinTheta_array[:, linearIndex] = -0.5*(P_m_1_n_1+(n-m+1)*(n-m+2)*P_m__1_n_1)
                
            elif False:
                
                P_m_1_n_1 = special.lpmv(m+1, n+1, cos_theta)

                # TODO: is it really needed? (either the scipy does not calculate it correctly, or wrong formula is on Wikipedia)
                if m==-1:
                    P_m__1_n_1 = special.lpmv(1, n+1, cos_theta)
                else:
                    P_m__1_n_1 = special.lpmv(m-1, n+1, cos_theta)

                P_m_per_sinTheta_array[:, linearIndex] = -0.5*(P_m_1_n_1+(n-m+1)*(n-m+2)*P_m__1_n_1)
                
            elif False:
                
                P_m_per_sinTheta_array[:, linearIndex] = m * P_array[:, linearIndex] / np.sin(theta)
                

    # get the array of the derivates of the associated Legendre polynomials using one of the recurrence formulas:
    indexCount = 0
    linearIndex = 0
    for n in range(1, n_max + 1):
        for m in range(0, n + 1):
            indexCount += 1
            linearIndex = indexCount-1
            
            if True:
                # the n, m-1 index case:
                if m==0:
                    P_m__1_n = special.lpmv(-1, n, cos_theta)
                else:
                    linearIndex_m__1 = Mie_summationAccessaries.indexMapping_n_m(n, m-1)
                    P_m__1_n = P_array[:, linearIndex_m__1]
                    
                # the n, m+1 index case:
                if m==n:
                    # should be zero in this case (|m|>n)
                    P_m_1_n = special.lpmv(m+1, n, cos_theta)
                else:
                    linearIndex_m_1 = Mie_summationAccessaries.indexMapping_n_m(n, m+1)
                    P_m_1_n = P_array[:, linearIndex_m_1]
                
                P_der_array[:, linearIndex] = -0.5*((n+m)*(n-m+1)*P_m__1_n-P_m_1_n)
            
            elif False:
                
                # TODO: is it really needed? (either the scipy does not calculate it correctly, or wrong formula is on Wikipedia)
                if m==0:
                    P_m__1_n = special.lpmv(1, n, cos_theta)
                else:
                    P_m__1_n = special.lpmv(m-1, n, cos_theta)
                
                P_m_1_n = special.lpmv(m+1, n, cos_theta)
                
                P_der_array[:, linearIndex] = -0.5*((n+m)*(n-m+1)*P_m__1_n-P_m_1_n)

    return P_array, P_m_per_sinTheta_array, P_der_array
    

def sphericalBesselIndices(n_max):
    # for testing

    indexNumber = Mie_summationAccessaries.indexNumber_n(n_max)

    indices = np.zeros([indexNumber, 1])

    for n in range(1, n_max + 1):
        index=Mie_summationAccessaries.indexMapping_n(n)
        indices[index, 0] = n

    return indices

def associatedLegendreIndices(n_max):
    # for testing

    indexNumber = Mie_summationAccessaries.indexNumber_n_m(n_max)

    indices = np.zeros([indexNumber, 2])

    indexCount = 0
    linearIndex = 0
    for n in range(1, n_max + 1):
        for m in range(0, n + 1):
            indexCount += 1
            linearIndex = indexCount-1

            indices[linearIndex, 0] = n
            indices[linearIndex, 1] = m
    return indices
    
