import numpy as np

import matplotlib.pyplot as plt
import matplotlib as mpl

def polarizationDegreeHistogram(polarizationDegree, sphereRadius, emissionWavelength):

    fig = plt.figure()
    x, bins, p = plt.hist(polarizationDegree, bins=55, range=(-1, 1), edgecolor = "black")
    for item in p:
        item.set_height(item.get_height()/max(x))
    plt.xlabel('Polarization degree', size = 16)
    plt.ylabel('Normalized blinking number', size = 16)
    plt.ylim(0,1.1)
    #plt.title('SphereDiameter: {:.0f} nm, Wavelength: {:.0f} nm'.format(sphereRadius*2E9, emissionWavelength*1E9))

    fig.show()

    return fig

def degreeOfPolarizationHistogram(degreeOfPolarization, sphereRadius, emissionWavelength):

    fig = plt.figure()
    x, bins, p = plt.hist(degreeOfPolarization, bins=55, range=(0, 1), edgecolor = "black")
    for item in p:
        item.set_height(item.get_height()/max(x))
    plt.xlabel('Degree of polarization', size = 16)
    plt.ylabel('Normalized blinking number', size = 16)
    plt.ylim(0,1.1)
    #plt.title('SphereDiameter: {:.0f} nm, Wavelength: {:.0f} nm'.format(sphereRadius*2E9, emissionWavelength*1E9))

    fig.show()

    return fig


def polarizationDegreeDoubleHistogram(polarizationDegree_simulated, polarizationDegree_measured, sphereRadius, emissionWavelength, colortheme = 'blue'):

    color_simulated = None
    color_measured = None
    if colortheme == 'blue':
        color_simulated = 'blue'
        color_measured = 'lightsteelblue'
    elif colortheme == 'red':
        color_simulated = 'red'
        color_measured = 'rosybrown'
    else:
        raise Exception('Invalid color theme (\"{}\") was passed to the double histogram visualization.'.format(colortheme))
        

    fig = plt.figure()
    x, bins, p = plt.hist(polarizationDegree_simulated, bins=20, range=(-1, 0), edgecolor = "black", label='simulated', color=color_simulated)
    for item in p:
        item.set_height(item.get_height()/np.mean(x))
    x2, bins2, p2 = plt.hist(polarizationDegree_measured, bins=20, range=(0, 1), edgecolor = "black", label='measured', color=color_measured)
    for item in p2:
        item.set_height(item.get_height()/np.mean(x2))
    plt.xlabel('Polarization degree', size = 16)
    plt.ylabel('Normalized blinking number', size = 16)
    #plt.ylim(0,1.1)
    ax = plt.gca()
    # recompute the ax.dataLim
    ax.relim()
    # update ax.viewLim using the new dataLim
    ax.autoscale_view()
    #plt.title('SphereDiameter: {:.0f} nm, Wavelength: {:.0f} nm'.format(sphereRadius*2E9, emissionWavelength*1E9))

    ax.set_yticks([])

    plt.legend()
    fig.show()

    return fig


def brightnessHistogram(brightness, sphereRadius, emissionWavelength):

    fig = plt.figure()
    x, bins, p = plt.hist(brightness, bins=55, range=(0, np.max(brightness)))
    plt.xlabel('Brightness')
    plt.ylabel('Rel # of blinkings')
    plt.title('SphereDiameter: {:.0f} nm, Wavelength: {:.0f} nm'.format(sphereRadius*2E9, emissionWavelength*1E9))
    fig.show()


    return fig

def polarizationOrientationDependence(polarizationDegree, theta_vect, phi, position, sphereRadius, emissionWavelength):

    theta_vect_angle = theta_vect*180/np.pi

    fig = plt.figure()
    plt.plot(theta_vect_angle, polarizationDegree)
    plt.xlabel('Polar angle [°]')
    plt.ylabel('Pol deg')
    plt.ylim(-1.0,1.0)
    plt.title('SphereDiameter: {:.0f} nm, Wavelength: {:.0f} nm'.format(sphereRadius*2E9, emissionWavelength*1E9))
    fig.show()

    return fig

def polarizedPFS(PSF, sphereRadius, emissionWavelength):

    # TODO multiple PSFs

    polarizationPSF_x = PSF[0]
    polarizationPSF_y = PSF[1]

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10,5))
    fig.suptitle('SphereDiameter: {:.0f} nm, Wavelength: {:.0f} nm'.format(sphereRadius*2E9, emissionWavelength*1E9))
    ax1.pcolormesh(polarizationPSF_x)
    ax2.pcolormesh(polarizationPSF_y)
    plt.show()

    return fig


def sphere_scatter3D(samplingPositions_spherical, polarizationDegree):

    # https://matplotlib.org/stable/gallery/mplot3d/scatter3d.html


    r = samplingPositions_spherical[0, :]
    theta = samplingPositions_spherical[1, :]
    phi = samplingPositions_spherical[2, :]

    # mathematician convention:
    """x, y, z = (
        r * np.cos(theta) * np.sin(phi),
        r * np.sin(theta) * np.sin(phi),
        r * np.cos(phi)
        )"""
    # physicist convention:
    x, y, z = (
        r * np.cos(phi) * np.sin(theta),
        r * np.sin(phi) * np.sin(theta),
        r * np.cos(theta))

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    # 'hot','gist_rainbow'
    s = ax.scatter(x, y, z, cmap='jet', c=polarizationDegree)

    fig.colorbar(s, ax=ax)

    plt.show()

    return fig


"""
import pyvista as pv
def testing_visualize_surface3D(samplingPositions_spherical, polarizationDegree):

    r = samplingPositions_spherical[0, :]
    theta = samplingPositions_spherical[1, :]
    phi = samplingPositions_spherical[2, :]

    # physicist convention:
    x, y, z = (
        r * np.cos(phi) * np.sin(theta),
        r * np.sin(phi) * np.sin(theta),
        r * np.cos(theta))

    points=np.array([x, y, z])

    # points is a 3D numpy array (n_points, 3) coordinates of a sphere
    cloud = pv.PolyData(points)
    cloud.plot()

    volume = cloud.delaunay_3d(alpha=2.)
    shell = volume.extract_geometry()
    shell.plot()
"""

import matplotlib.tri as mtri
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

def sphere_mesh3D(samplingPositions_spherical, values, colorbarText):

    # https://discourse.matplotlib.org/t/trisurf-plots-with-independent-color-data/19033/2

    r = samplingPositions_spherical[0, :]
    theta = samplingPositions_spherical[1, :]
    phi = samplingPositions_spherical[2, :]

    r = np.pad(r, (0,1), 'constant', constant_values=(0, 0))
    theta = np.pad(theta, (0,1), 'constant', constant_values=(0, 0))
    phi = np.pad(phi, (0,1), 'constant', constant_values=(0, 0))
    values = np.pad(values, (0,1), 'constant', constant_values=(0, 0))

    r[-1] = r[0]
    theta[-1] = theta[0]
    phi[-1] = phi[0]

    # Triangulate parameter space to determine the triangles
    tri = mtri.Triangulation(phi, theta)

    x, y, z = (
        0.5 * np.cos(phi) * np.sin(theta) +0.5,
        0.5 * np.sin(phi) * np.sin(theta)+0.5,
        0.5 * np.cos(theta)+0.5)

    triangle_values = \
        [np.array((values[T[0]]+
                values[T[1]]+
                values[T[2]])/3) for T in tri.triangles]

    triangle_vertices = np.array(
        [np.array([[x[T[0]], y[T[0]], z[T[0]]],
                [x[T[1]], y[T[1]], z[T[1]]],
                [x[T[2]], y[T[2]], z[T[2]]]]) for T in tri.triangles])

    vmin=np.min(triangle_values)
    vmax=np.max(triangle_values)
    #vmin = -1
    #vmax = 1
    norm =mpl.colors.Normalize(vmin=vmin, vmax=vmax)

    cmap=plt.get_cmap('jet')
    facecolors = [cmap.__call__((value-vmin)/(vmax-vmin)) for value in triangle_values]

    coll = Poly3DCollection(triangle_vertices, facecolors=facecolors, norm = norm)

    # Plot the surface.  The triangles in parameter space determine which x, y, z
    # points are connected by an edge.
    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    ax.add_collection(coll)

    mappable = mpl.cm.ScalarMappable(norm = norm, cmap=cmap)

    cbar = fig.colorbar(mappable = mappable, ax=ax, shrink=0.85)
    cbar.ax.set_ylabel(colorbarText, fontsize=14)
    #plt.draw()

    ax.view_init(30, -60-90)

    ax.set(xlabel="y", ylabel="x", zlabel="z")
    ax.xaxis.get_label().set_fontsize(14)
    ax.yaxis.get_label().set_fontsize(14)
    ax.zaxis.get_label().set_fontsize(14)

    plt.show()

    return fig
