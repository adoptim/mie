import matplotlib.pyplot as plt
import numpy as np

# This contains function that place points or orients vectors on a sphere surface with given distribution. 


def getUnitOrientationVectorsFromAngles(theta, phi):
    
    if np.size(theta) != np.size(phi):
        raise ValueError("The data number of \"theta\" and \"phi\" angles must be equal.")
    
    unitVector = np.zeros([3, np.size(theta)])

    # let the "r" component be:
    unitVector[0, :] = np.cos(theta)
    # let the "theta" component be:
    unitVector[1, :] = np.sin(theta) * np.cos(phi)
    # let the "phi" component be:
    unitVector[2, :] = np.sin(theta) * np.sin(phi)
    
    return unitVector


def getUnitOrientationVectors_r_theta_phi(pointNumber, distributionType):
    # This function returns the components of unit vectors in spherical unit
    # vector basis (at some position in the spherical coordinate system). These
    # unit vectors point to a surface of a unit sphere in evenly distibuted
    # manner. Since it is ambigious how the angles should be measured, the
    # convention is the following: the theta angle is measured from "e_r", and
    # the phi angle is measured from "e_theta" basis unit vectors.
    # It is mostly useful for getting orientation vectors for the dipole moment.

    if distributionType == "full sphere, even":
        theta, phi = getSphericalAngles_evenDistribution(pointNumber)
    elif distributionType == "full sphere, uniform":
        theta, phi = getSphericalAngles_uniformDistribution(pointNumber)
    elif distributionType == "radial unit vector":
        theta = 0 * np.ones(pointNumber)
        phi = 0 * np.ones(pointNumber)
    elif distributionType == "theta unit vector":
        theta = 90 * np.pi/180 * np.ones(pointNumber)
        phi = 0 * np.ones(pointNumber)
    elif distributionType == "phi unit vector":
        theta = 90 * np.pi/180 * np.ones(pointNumber)
        phi = 90 * np.pi/180 * np.ones(pointNumber)
    else:
        raise ValueError("Invalid pont distribution type on sphere (\"{}\") was given.".format(distributionType));

    unitVector = getUnitOrientationVectorsFromAngles(theta, phi)

    return unitVector


def getPositionVectorsWithinCone_sphericalCoordinates(pointNumber_4PI, r, coneAngle, distributionType):
    # This function returns the position vectors distributed evenly on a
    # spherical cone. The cone is aligned to the "z" axis.

    # position vectors on the full sphere evenly distributed:
    positionVector_4PI = getPositionVectors_sphericalCoordinates(pointNumber_4PI, r, distributionType)

    # where the points are within the cone, the  polar angle ("theta") is
    # smaller than the cone half angle:
    withinConeBoolean = positionVector_4PI[1, :] <= coneAngle

    # position vectors within the cone:
    positionVector_cone = positionVector_4PI[:, withinConeBoolean]

    return positionVector_cone


def getPositionVectors_sphericalCoordinates(pointNumber, r, distributionType):
    # It returns the coordinates of evenly distributed points on a sphere in
    # spherical coordiante system (r, theta, phi). The coordinates are stored
    # in a 3xn array, where the first dimension correponds to the different
    # component and the second to the different points.

    positionVector = np.zeros([3, pointNumber])

    positionVector[0, :] = r

    if distributionType == "even":
        positionVector[1, :], positionVector[2, :] = getSphericalAngles_evenDistribution(pointNumber)
    elif distributionType == "uniform":
        positionVector[1, :], positionVector[2, :] = getSphericalAngles_uniformDistribution(pointNumber)
    elif distributionType == "z axis":
        # theta polar angle:
        positionVector[1, :] = np.zeros([pointNumber])
        # phi azimuth angle: 
        positionVector[2, :] = np.zeros([pointNumber])
    else:
        raise ValueError("Invalid pont distribution type on sphere (\"{}\") was given.".format(distributionType));

    return positionVector


def getSphericalAngles_evenDistribution(pointNumber):
    # This function calculates the azimuthal and polar angles of points 
    # placed roughly evenly on a sphere surface (in prededetermined
    # positions not in a random one).
    # source:
    # http://extremelearning.com.au/evenly-distributing-points-on-a-sphere/
    # be aware: the source uses the "mathematician" convention and not the
    # "physicist", the theta and phi are swapped

    i = np.arange(0, pointNumber, dtype=float) + 0.5
    theta = np.arccos(1 - 2*i/pointNumber)
    goldenRatio = (1 + 5**0.5)/2
    phi = np.mod(2 * np.pi * i / goldenRatio, np.pi*2)

    return theta, phi

def getSphericalAngles_uniformDistribution(n):
    # This function calculates the azimuthal and polar angles of points 
    # placed with random unifrom distribution on a sphere surface.

    theta = np.arccos(1 - 2*np.random.rand(n))
    phi = np.random.rand(n) * 2 * np.pi

    return theta, phi
