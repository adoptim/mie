import numpy as np
from Mie_calculation.Mie_planeWave_efficiencies import Mie_planeWave_efficiencies
from Mie_calculation.Mie_dimensionlessQuantities import sizeParameter


def Mie_planeWave_crossSections(lambda_0, r, refr_sphere, refr_medium):


	[Q_a, Q_s, Q_e]=Mie_planeWave_efficiencies(lambda_0, r, refr_sphere, refr_medium)

	x=sizeParameter(lambda_0, r, refr_sphere, refr_medium)

	# the cross sections
	sigma_s=Q_s*r**2*np.pi
	sigma_e=Q_e*r**2*np.pi
	sigma_a=Q_a*r**2*np.pi


	return sigma_a, sigma_s, sigma_e
