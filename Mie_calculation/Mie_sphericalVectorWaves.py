import numpy as np

# These functions return the different spherical vector wave functions "at the
# point of investigation" which is either the dipole position or the point
# where the value of the electric phield is calculated.
# These function return 3xN array where "N" is the number of elements of "kr" ("kr" and the other non-index inputs must be 1D numpy arrays with he same number of elements).

# sources of the formulas:
# Straton: Electromagnetic theory, Chapter VII, page 416
# 1961: Cruzan: TRANSLATIONALADDITION THEOREMS FOR SPHERICAL VECTOR WAVE FUNCTIONS
# 1982 Kerker

# m: index of the sperical harmonics and of the associsated legendre
# polynomials, goes from 0 to n
# n: index of the sperical harmonics and of the associsated legendre
# polynomials, goes from 1 to infinity
# sigma: index of the sperical harmonics and of the associsated legendre
# polynomials, can only have two values , either odd (1) or even (2)
# theta: polar angle of the point of investigation in the spherical coordinate system
# phi: azimuth angle of the point of investigation in the spherical coordinate system
# kr: dimensionless distance of the point of investigation
# j_n: spherical Bessel function of the phirst kind and order n
# j_n_der: derivate of the spherical Bessel function with respect to kr
# h_n: spherical Hankel function of the phirst kind
# h_n_der: derivate of the spherical Hankel function with respect to kr
# P_m_n: associated Legendre polynimial
# P_m_n_der: derivate of the associated Legendre polynimial with respect to theta


def getAll(n, m, sigma, kr, theta, phi, j_n, j_n_der, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der):

    M_1_sigma_m_n = M_1(n, m, sigma, theta, phi, j_n, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)
    N_1_sigma_m_n = N_1(n, m, sigma, kr, theta, phi, j_n, j_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)
    M_3_sigma_m_n = M_3(n, m, sigma, theta, phi, h_n, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)
    N_3_sigma_m_n = N_3(n, m, sigma, kr, theta, phi, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)

    return (M_1_sigma_m_n, N_1_sigma_m_n, M_3_sigma_m_n, N_3_sigma_m_n)


def M_1(n, m, sigma, theta, phi, j_n, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der):
    # Returns an Nx3 array of spherical vector waves.
    # The first dimension (N) corresponds to the argument dimension, which needs to be a 1D aray.
    # The second dimension (3) correspond to the componens (e_r, e_theta, e_fi) of the vector wave.
    
    if sigma == 1:
        # odd

        M_1_sigma_m_n = np.stack(
            (
                np.zeros([theta.size]),
                j_n  * (+1 * np.cos(m * phi)) * P_m_per_sinTheta_m_n,
                -j_n * np.sin(m * phi) * P_m_n_der
            ),
            axis=-1)
    elif sigma == 2:
        # even
        M_1_sigma_m_n = np.stack(
            (
                np.zeros([theta.size]),
                j_n  * (-1 * np.sin(m * phi)) * P_m_per_sinTheta_m_n,
                -j_n * np.cos(m * phi) * P_m_n_der,
            ),
            axis=-1)
    return M_1_sigma_m_n


def N_1(n, m, sigma, kr, theta, phi, j_n, j_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der):
    # Returns an Nx3 array of spherical vector waves.
    # The first dimension (N) corresponds to the argument dimension, which needs to be a 1D aray.
    # The second dimension (3) correspond to the componens (e_r, e_theta, e_fi) of the vector wave.
    
    if sigma == 1:
        # odd
        N_1_sigma_m_n = np.stack(
            (
                n * (n + 1) / kr * j_n * P_m_n * np.sin(m * phi),
                (j_n / kr + j_n_der) * P_m_n_der * np.sin(m * phi),
                +1 * (j_n / kr + j_n_der) * P_m_per_sinTheta_m_n * np.cos(m * phi),
            ),
            axis=-1)
    elif sigma == 2:
        # even
        N_1_sigma_m_n = np.stack(
            (
                n * (n + 1) / kr * j_n * P_m_n * np.cos(m * phi),
                (j_n / kr + j_n_der) * P_m_n_der * np.cos(m * phi),
                -1 * (j_n / kr + j_n_der) * P_m_per_sinTheta_m_n * np.sin(m * phi),
            ),
            axis=-1)
    return N_1_sigma_m_n


def M_3(n, m, sigma, theta, phi, h_n, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der):
    # Returns an Nx3 array of spherical vector waves.
    # The first dimension (N) corresponds to the argument dimension, which needs to be a 1D aray.
    # The second dimension (3) correspond to the componens (e_r, e_theta, e_fi) of the vector wave.
    
    if sigma == 1:
        # odd
        M_3_sigma_m_n = np.stack(
            (
                np.zeros([theta.size]),
                h_n  * (+1 * np.cos(m * phi)) * P_m_per_sinTheta_m_n,
                -h_n * np.sin(m * phi) * P_m_n_der,
            ),
            axis=-1)
    elif sigma == 2:
        # even
        M_3_sigma_m_n = np.stack(
            (
                np.zeros([theta.size]),
                h_n  * (-1 * np.sin(m * phi)) * P_m_per_sinTheta_m_n,
                -h_n * np.cos(m * phi) * P_m_n_der,
            ),
            axis=-1)
    return M_3_sigma_m_n


def N_3(n, m, sigma, kr, theta, phi, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der):
    # Returns an Nx3 array of spherical vector waves.
    # The first dimension (N) corresponds to the argument dimension, which needs to be a 1D aray.
    # The second dimension (3) correspond to the componens (e_r, e_theta, e_fi) of the vector wave.
    
    if sigma == 1:
        # odd
        N_3_sigma_m_n = np.stack(
            (
                n * (n + 1) / kr * h_n * P_m_n * np.sin(m * phi),
                (h_n / kr + h_n_der) * P_m_n_der * np.sin(m * phi),
                +1 * (h_n / kr + h_n_der) * P_m_per_sinTheta_m_n * np.cos(m * phi),
            ),
            axis=-1)
    elif sigma == 2:
        # even
        N_3_sigma_m_n = np.stack(
            (
                n * (n + 1) / kr * h_n * P_m_n * np.cos(m * phi),
                (h_n / kr + h_n_der) * P_m_n_der * np.cos(m * phi),
                -1 * (h_n / kr + h_n_der) * P_m_per_sinTheta_m_n * np.sin(m * phi),
            ),
            axis=-1)
    return N_3_sigma_m_n


import Mie_calculation.auxiliary.Mie_summationAccessaries as Mie_summationAccessaries
import Mie_calculation.auxiliary.sphericalFunctions as sphericalFunctions

def arraysForDipoleElectricField(positionVector_electricField, emissionWavelength, refractiveIndex_medium, n_max):
    # This function calculates the vector wave functions for the dipole source 
    # electric field calculation. The values are stored in an array, where the 
    # first dimension corresponds to the electric field components, the second
    # to the sampling points and the third to the summation indices.

    # number of sampling points:
    electricField_numberOfPoints = positionVector_electricField.shape[1]

    # number of summation indices:
    indexNumber=Mie_summationAccessaries.indexNumber_n_m_sigma(n_max)

    # arrays for storing the vector wave functions:
    M_3_sigma_m_n = np.zeros([electricField_numberOfPoints, indexNumber, 3], dtype=np.cdouble)
    N_3_sigma_m_n = np.zeros([electricField_numberOfPoints, indexNumber, 3], dtype=np.cdouble)

    k_0 = 2 * np.pi / emissionWavelength
    k_medium = refractiveIndex_medium * k_0

    kr = k_medium * positionVector_electricField[0,:]
    theta = positionVector_electricField[1,:]
    phi = positionVector_electricField[2,:]
    j_array, j_der_array = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_array, h_der_array = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_array, j_der_array)
    P_array, P_m_per_sinTheta_sequence, P_der_array = sphericalFunctions.associatedLegendrePolynomials(n_max, theta)

    indexCount_n = 0
    linearIndex_n = 0
    indexCount_n_m = 0
    linearIndex_n_m = 0
    indexCount_n_m_sigma = 0
    linearIndex_n_m_sigma = 0
    for n in range(1, n_max+1):
        indexCount_n+=1
        linearIndex_n = indexCount_n-1

        h_n = h_array[:, linearIndex_n]
        h_n_der = h_der_array[:, linearIndex_n]

        for m in range(0, n+1):
            indexCount_n_m+=1
            linearIndex_n_m = indexCount_n_m-1

            P_m_n = P_array[:, linearIndex_n_m]
            P_m_per_sinTheta_m_n = P_m_per_sinTheta_sequence[:, linearIndex_n_m]
            P_m_n_der = P_der_array[:, linearIndex_n_m]

            for sigma in range(1, 3):
                indexCount_n_m_sigma+=1
                linearIndex_n_m_sigma = indexCount_n_m_sigma-1

                # the spherical vector waves for the given summation index:
                M_3_sigma_m_n[:,linearIndex_n_m_sigma, :] = M_3(n, m, sigma, theta, phi, h_n, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)
                N_3_sigma_m_n[:,linearIndex_n_m_sigma, :] = N_3(n, m, sigma, kr, theta, phi, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)

    return M_3_sigma_m_n, N_3_sigma_m_n
