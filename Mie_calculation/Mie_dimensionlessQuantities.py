import numpy as np

# TODO: review this code

def sizeParameter(lambda_0, r, refr_sphere, refr_medium):
    # constants
    k_0=2*np.pi/lambda_0            #1/m

    mu_0=1.25663706212*10**-6            #H/m
    epsilon_0=8.8541878128*10**-12        # F/m
    c_0=1/np.sqrt(epsilon_0*mu_0)

    refr_rel=relativeRefractiveIndex(refr_sphere, refr_medium)
    k_medium=refr_medium*k_0


    mu_sphere=1
    epsilon_sphere=refr_sphere**2/mu_sphere

    mu_medium=1
    epsilon_medium=refr_medium**2/mu_medium


    #x=2*np.pi*r/lambda_0   # is using the vacuum wavelength correct here?
    x=r*k_medium
    
    return x

def relativeRefractiveIndex(refr_sphere, refr_medium):
    
    refr_rel=refr_sphere/refr_medium
    
    return refr_rel
