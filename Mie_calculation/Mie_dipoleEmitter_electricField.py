import math as math
import numpy as np
from scipy import special

import Mie_calculation.auxiliary.Mie_physicalConstants as Mie_physicalConstants

import Mie_calculation.Mie_coefficients as Mie_coefficients

import Mie_calculation.Mie_sphericalVectorWaves as Mie_sphericalVectorWaves

import Mie_calculation.auxiliary.Mie_summationAccessaries as Mie_summationAccessaries
import Mie_calculation.auxiliary.sphericalFunctions as sphericalFunctions

def getElectricField_selectedMode(
    D_sequence,
    s_sequence, t_sequence,
    u_sequence, v_sequence,
    M_3_array, N_3_array, n):
    

    # the expansion coefficients, must be 1D numpy arrays:
    D_s_sequence = D_sequence * s_sequence
    D_t_sequence = D_sequence * t_sequence
    D_u_sequence = D_sequence * u_sequence
    D_v_sequence = D_sequence * v_sequence


    dipoleField = np.matmul(D_s_sequence, M_3_array) + np.matmul(D_t_sequence, N_3_array)

    """for idx in range(4, D_sequence.size):
        D_u_sequence[idx] = 0
        M_3_array[idx] = 0
        D_v_sequence[idx] = 0
        N_3_array[idx] = 0"""

    
    linearIndex_n_0_1 = Mie_summationAccessaries.indexMapping_n_m_sigma(n, 0, 1)
    linearIndex_n_n_2 = Mie_summationAccessaries.indexMapping_n_m_sigma(n, n, 2)


    D_u_sequence_selectedMode = np.zeros(D_u_sequence.shape, dtype=complex)
    D_u_sequence_selectedMode[linearIndex_n_0_1:linearIndex_n_n_2+1] = D_u_sequence[linearIndex_n_0_1:linearIndex_n_n_2+1]
    #D_u_sequence_selectedMode[0:linearIndex_n_n_2+1] = D_u_sequence[0:linearIndex_n_n_2+1]
    #D_u_sequence_selectedMode = D_u_sequence
    
    D_v_sequence_selectedMode = np.zeros(D_v_sequence.shape, dtype=complex)
    D_v_sequence_selectedMode[linearIndex_n_0_1:linearIndex_n_n_2+1] = D_v_sequence[linearIndex_n_0_1:linearIndex_n_n_2+1]
    #D_v_sequence_selectedMode[0:linearIndex_n_n_2+1] = D_v_sequence[0:linearIndex_n_n_2+1]
    #D_v_sequence_selectedMode = D_v_sequence

    scatteredField = np.matmul(D_u_sequence_selectedMode, M_3_array) + np.matmul(D_v_sequence_selectedMode, N_3_array)
    # TODO: the formula in the Kerker 1982 paper is probably wrong:
    # scatteredField = np.matmul(u_sequence, M_3_array) + np.matmul(v_sequence, N_3_array)

    """
    # matrix multiplicatins that were used previously:
    dipoleField=np.zeros([M_3_array.shape[0], 3], dtype=np.cdouble)
    
    scatteredField=np.zeros([3, M_3_array.shape[1]], dtype=np.cdouble)
    scatteredField[0, :] = np.matmul(M_3_array[0, :, :], u_sequence) + np.matmul(N_3_array[0, :, :], v_sequence)
    scatteredField[1, :] = np.matmul(M_3_array[1, :, :], u_sequence) + np.matmul(N_3_array[1, :, :], v_sequence)
    scatteredField[2, :] = np.matmul(M_3_array[2, :, :], u_sequence) + np.matmul(N_3_array[2, :, :], v_sequence)
    """

    # the aforementioned matrix multiplications can be written in a more concise form:
    # https://numpy.org/doc/stable/reference/generated/numpy.matmul.html

    # dipoleField = np.matmul(M_3_array, D_s_sequence) + np.matmul(N_3_array, D_t_sequence)

    # scatteredField = np.matmul(M_3_array, u_sequence) + np.matmul(N_3_array, v_sequence)


    # the total electric field:
    totalField = dipoleField + scatteredField

    return scatteredField


def getElectricField_dipole(
    D_sequence,
    s_sequence, t_sequence,
    u_sequence, v_sequence,
    M_3_array, N_3_array):
    # This function calculates the electric field values at sampling points
    # evenly distributed on a sphere surface. The dipole orientation (and
    # position) is fixed, only the sampling point coordinate dependece remains
    # (summing along the summation indices using matrix multiplication...)

    # the expansion coefficients, must be 1D numpy arrays:
    D_s_sequence = D_sequence * s_sequence
    D_t_sequence = D_sequence * t_sequence
    D_u_sequence = D_sequence * u_sequence
    D_v_sequence = D_sequence * v_sequence


    dipoleField = np.matmul(D_s_sequence, M_3_array) + np.matmul(D_t_sequence, N_3_array)

    """for idx in range(4, D_sequence.size):
        D_u_sequence[idx] = 0
        M_3_array[idx] = 0
        D_v_sequence[idx] = 0
        N_3_array[idx] = 0"""

    """
    # matrix multiplicatins that were used previously:
    dipoleField=np.zeros([M_3_array.shape[0], 3], dtype=np.cdouble)
    """

    # the aforementioned matrix multiplications can be written in a more concise form:
    # https://numpy.org/doc/stable/reference/generated/numpy.matmul.html

    # dipoleField = np.matmul(M_3_array, D_s_sequence) + np.matmul(N_3_array, D_t_sequence)

    return dipoleField


def getElectricField_scattered(
    D_sequence,
    s_sequence, t_sequence,
    u_sequence, v_sequence,
    M_3_array, N_3_array):
    # This function calculates the electric field values at sampling points
    # evenly distributed on a sphere surface. The dipole orientation (and
    # position) is fixed, only the sampling point coordinate dependece remains
    # (summing along the summation indices using matrix multiplication...)

    # the expansion coefficients, must be 1D numpy arrays:
    D_s_sequence = D_sequence * s_sequence
    D_t_sequence = D_sequence * t_sequence
    D_u_sequence = D_sequence * u_sequence
    D_v_sequence = D_sequence * v_sequence

    """for idx in range(4, D_sequence.size):
        D_u_sequence[idx] = 0
        M_3_array[idx] = 0
        D_v_sequence[idx] = 0
        N_3_array[idx] = 0"""

    scatteredField = np.matmul(D_u_sequence, M_3_array) + np.matmul(D_v_sequence, N_3_array)
    # TODO: the formula in the Kerker 1982 paper is probably wrong:
    # scatteredField = np.matmul(u_sequence, M_3_array) + np.matmul(v_sequence, N_3_array)

    """
    
    scatteredField=np.zeros([3, M_3_array.shape[1]], dtype=np.cdouble)
    scatteredField[0, :] = np.matmul(M_3_array[0, :, :], u_sequence) + np.matmul(N_3_array[0, :, :], v_sequence)
    scatteredField[1, :] = np.matmul(M_3_array[1, :, :], u_sequence) + np.matmul(N_3_array[1, :, :], v_sequence)
    scatteredField[2, :] = np.matmul(M_3_array[2, :, :], u_sequence) + np.matmul(N_3_array[2, :, :], v_sequence)
    """

    # the aforementioned matrix multiplications can be written in a more concise form:
    # https://numpy.org/doc/stable/reference/generated/numpy.matmul.html

    # scatteredField = np.matmul(M_3_array, u_sequence) + np.matmul(N_3_array, v_sequence)

    return scatteredField


def getElectricField_total(
    D_sequence,
    s_sequence, t_sequence,
    u_sequence, v_sequence,
    M_3_array, N_3_array):
    # This function calculates the electric field values at sampling points
    # evenly distributed on a sphere surface. The dipole orientation (and
    # position) is fixed, only the sampling point coordinate dependece remains
    # (summing along the summation indices using matrix multiplication...)

    # the expansion coefficients, must be 1D numpy arrays:
    D_s_sequence = D_sequence * s_sequence
    D_t_sequence = D_sequence * t_sequence
    D_u_sequence = D_sequence * u_sequence
    D_v_sequence = D_sequence * v_sequence


    dipoleField = np.matmul(D_s_sequence, M_3_array) + np.matmul(D_t_sequence, N_3_array)

    """for idx in range(4, D_sequence.size):
        D_u_sequence[idx] = 0
        M_3_array[idx] = 0
        D_v_sequence[idx] = 0
        N_3_array[idx] = 0"""

    scatteredField = np.matmul(D_u_sequence, M_3_array) + np.matmul(D_v_sequence, N_3_array)
    # TODO: the formula in the Kerker 1982 paper is probably wrong:
    # scatteredField = np.matmul(u_sequence, M_3_array) + np.matmul(v_sequence, N_3_array)

    """
    # matrix multiplicatins that were used previously:
    dipoleField=np.zeros([M_3_array.shape[0], 3], dtype=np.cdouble)
    
    scatteredField=np.zeros([3, M_3_array.shape[1]], dtype=np.cdouble)
    scatteredField[0, :] = np.matmul(M_3_array[0, :, :], u_sequence) + np.matmul(N_3_array[0, :, :], v_sequence)
    scatteredField[1, :] = np.matmul(M_3_array[1, :, :], u_sequence) + np.matmul(N_3_array[1, :, :], v_sequence)
    scatteredField[2, :] = np.matmul(M_3_array[2, :, :], u_sequence) + np.matmul(N_3_array[2, :, :], v_sequence)
    """

    # the aforementioned matrix multiplications can be written in a more concise form:
    # https://numpy.org/doc/stable/reference/generated/numpy.matmul.html

    # dipoleField = np.matmul(M_3_array, D_s_sequence) + np.matmul(N_3_array, D_t_sequence)

    # scatteredField = np.matmul(M_3_array, u_sequence) + np.matmul(N_3_array, v_sequence)


    # the total electric field:
    totalField = dipoleField + scatteredField

    return totalField

def integratedPower(electricField, R, numberOfPoints, emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium):

    intensity = electricFieldToIntensity(electricField, emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)

    delta_f = 4*R**2*np.pi/numberOfPoints
    
    # summ the intensity components for all sampling point and polarization:
    power = np.sum(intensity) * delta_f
    
    return power

def apertureSamplingCoordinates(positionVector_electricField):
    # This function imply takes the "x" and "y" projections of the sampling points on the sphere.
    
    
    samplingPoints_aperture = np.zeros((2, positionVector_electricField.shape[1]))
    samplingPoints_aperture[0, :] = positionVector_electricField[0, :] * np.sin(positionVector_electricField[1, :]) * np.cos(positionVector_electricField[2, :])
    samplingPoints_aperture[1, :] = positionVector_electricField[0, :] * np.sin(positionVector_electricField[1, :]) * np.sin(positionVector_electricField[2, :])
    
    return samplingPoints_aperture

def collimation(positionVector_electricField, electricField_spherical):
    # This function collimates the emitted electric field. It first has to "collimate the beam" as an
    # ideal lens would do, then this "collimated beam's" polarization
    # components are determined in a Descates coordinate system.
    # The "electricField_spherical" must be a 3xN numpy array, where
    # the first dimension corresponds to the polarization components, and
    # the second to the sampling points ("electricField_numberOfPoints").


    # precalculating the trigonometric function of phi, should be electricField_numberOfPoints element 1D numpy arrays:
    sin_phi = np.sin(positionVector_electricField[2, :])
    cos_phi = np.cos(positionVector_electricField[2, :])

    electricField_collimated = np.zeros([electricField_spherical.shape[0], 3], dtype=np.cdouble)
    # x component:
    electricField_collimated[:, 0] =\
        electricField_spherical[:, 1] * cos_phi - electricField_spherical[:, 2] * sin_phi
    # y component:
    electricField_collimated[:, 1] =\
        electricField_spherical[:, 1] * sin_phi + electricField_spherical[:, 2] * cos_phi
    # z component:
    electricField_collimated[:, 2] = electricField_spherical[:, 0]
    
    return electricField_collimated

def polarizationChannelSignals(positionVector_electricField, electricField_spherical, collectionAngle, refractiveIndex_medium):
    # This function integrates the intensity distibution of the different
    # polarization components. It first has to "collimate the beam" as an
    # ideal lens would do, then this "collimated beam's" polarization
    # components are determined in a Descates coordinate system.
    # The "electricField_spherical" must be a 3xN numpy array, where
    # the first dimension corresponds to the polarization components, and
    # the second to the sampling points.

    # formulas used for the projection:
    # e_x = e_r*0 + e_theta*cos(phi) + e_phi*cos(phi+90°) = e_theta*cos(phi) - e_phi*sin(phi)
    # e_y = e_r*0 + e_theta*sin(phi) + e_phi*sin(phi+90°) = e_theta*sin(phi) + e_phi*cos(phi)
    # e_z = e_r*1 + e_theta*0 + e_phi*0 = e_r

    # raidus of the sphere of the sampling, should be the same for all sampling points:
    distance = positionVector_electricField[0, 0]
    # area of unit surface element, required for integration over the sphere:
    unitSurfaceElementArea = 2 * np.pi * distance**2 * (1 - np.cos(collectionAngle)) / np.size(positionVector_electricField)

    electricField_collimated = collimation(positionVector_electricField, electricField_spherical)

    # calculate the "intensities" of the sampling points from the
    # sampled electric field, 3xN numpy array:
    intensityDistribution_Descartes =\
        electricFieldToIntensity(electricField_collimated, refractiveIndex_medium)

    # summing along the sampling points (2nd dimension) and getting the the polarization
    # signal components for the given dipole orientation and dipole position,
    # the result is a 3 element 1D numpy array:
    channelSignalVect = np.sum(intensityDistribution_Descartes, axis=0) * unitSurfaceElementArea

    return channelSignalVect

def unnormalized_Stokes(positionVector_electricField, electricField_spherical, collectionAngle, refractiveIndex_medium):

    # raidus of the sphere of the sampling, should be the same for all sampling points:
    distance = positionVector_electricField[0, 0]
    # area of unit surface element, required for integration over the sphere:
    unitSurfaceElementArea = 2 * np.pi * distance**2 * (1 - np.cos(collectionAngle)) / np.size(positionVector_electricField)

    electricField_collimated = collimation(positionVector_electricField, electricField_spherical)
    
    E_x = electricField_collimated[:,0];
    E_y = electricField_collimated[:,1];
    
    S_0 = np.sum(np.abs(E_x)**2+np.abs(E_y)**2) * unitSurfaceElementArea
    S_1 = np.sum(np.abs(E_x)**2-np.abs(E_y)**2) * unitSurfaceElementArea
    S_2 = np.sum(np.real(E_x*np.conjugate(E_y))) * unitSurfaceElementArea
    S_3 = np.sum(np.imag(E_x*np.conjugate(E_y))) * unitSurfaceElementArea
    
    Stokes_parameters=np.array([S_0, S_1, S_2, S_3])
    
    return Stokes_parameters

def electricFieldToIntensity(electricField, refractiveIndex_medium):
    
    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()

    epsilon=np.real(refractiveIndex_medium**2)
    
    # TODO:check the refractive index, why does it only give right result when it is squared??? (by the way, it equals to epsilon....)
    intensity=epsilon_0*epsilon*c_0/refractiveIndex_medium**2/2*np.real(electricField*np.conjugate(electricField))
    #intensity=epsilon_0*epsilon*c_0/refractiveIndex_medium/2*np.real(electricField_4PI*np.conjugate(electricField))
    
    return intensity
