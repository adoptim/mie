import numpy as np

def refractiveIndex_composite(refr_1, refr_2, compositionRatio):
	# Calculating the composite refractive index as the linearily weighted average of the two complex dielectric constants
	# Source (silver and gold composite): https://doi.org/10.1021/jp062536y
	
	mu_1=1
	mu_2=1
	mu_composition=1
	epsilon_1=refr_1**2/mu_1
	epsilon_2=refr_2**2/mu_2
	epsilon_composition=compositionRatio*epsilon_1+(1-compositionRatio)*epsilon_2
	refr_composition=np.sqrt(epsilon_composition*mu_composition)
	
	# The square root is an ambiguous operation, checking the physical validity of the results:
	nonneg_ref_number=np.sum(np.real(refr_composition)>0)
	if nonneg_ref_number==refr_composition.size:
		return refr_composition
	elif nonneg_ref_number==0:
		return -refr_composition
	else:
		raise SyntaxError('There is a problem with the composite refractive index calculation')
