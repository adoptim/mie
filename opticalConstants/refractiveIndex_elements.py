import numpy as np
from scipy import interpolate

def gold(wavelength):
	[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/gold_Johnson.csv',delimiter=',',skiprows=1, unpack=True)
	lambdaData=refrData[0,:]/10**6
	nData=refrData[1,:]
	kData=refrData[2,:]
	
	
	n = interpolate.interp1d(lambdaData, nData, kind='cubic')
	k = interpolate.interp1d(lambdaData, kData, kind='cubic')
	
	n_complex=n(wavelength)+1j*k(wavelength)
	# The calculation is consistent with the simulation when using the "n-ik" formalism. It is unbeleivable that no one can write down which formalism they use...
	
	return n_complex

def silver(wavelength):
	[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Johnson.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Babar.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Ciesielski_Ge.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Ciesielski_Ni.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Ciesielski.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_McPeak.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Rakic_Brendel_Bormann.csv',delimiter=',',skiprows=1, unpack=True)
	##[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Rakic_Lorentz_Drude.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Werner_DFT.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Werner.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Wu.csv',delimiter=',',skiprows=1, unpack=True)
	#[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/silver_Yang.csv',delimiter=',',skiprows=1, unpack=True)
	
	lambdaData=refrData[0,:]/10**6
	nData=refrData[1,:]
	kData=refrData[2,:]
	
	
	n = interpolate.interp1d(lambdaData, nData, kind='cubic')
	k = interpolate.interp1d(lambdaData, kData, kind='cubic')
	
	n_complex=n(wavelength)+1j*k(wavelength)
	# The calculation is consistent with the simulation when using the "n-ik" formalism. It is unbeleivable that no one can write down which formalism they use...
	
	return n_complex


def water(wavelength):
	
	
	[lambda_um,n,k] = refrData=np.loadtxt('opticalConstants/data/water_Hale.csv',delimiter=',',skiprows=1, unpack=True)
	lambdaData=refrData[0,:]/10**6
	nData=refrData[1,:]
	kData=refrData[2,:]
	
	n = interpolate.interp1d(lambdaData, nData, kind='cubic')
	k = interpolate.interp1d(lambdaData, kData, kind='cubic')
	n_complex=n(wavelength)+1j*k(wavelength)
	return n_complex

def vacuum(wavelength):

    n_complex = np.ones(np.shape(wavelength), dtype=complex)
    return n_complex
