import numpy as np
import matplotlib.pyplot as plt
import math as math

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite
from Mie_calculation.Mie_dipoleEmitter_rates import Mie_dipoleEmitter_rates
from Mie_calculation.Mie_dipoleEmitter_rates import freeSpaceRadiationRate


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors
import Mie_calculation.auxiliary.Mie_physicalConstants as Mie_physicalConstants

# SWEEP PARAMETERS

n_max = 15

N = 100
sphereRadius = 1 / 2 * np.linspace(60, 200, num=N) * 10 ** -9
#N = 1
#sphereRadius = 1 / 2 * np.linspace(80, 80, num=N) * 10 ** -9

# integrate cos(theta)^2/(a*cos(theta)^2 + b*sin(theta)^2) * sin(thetax) dtheta
def integrateEmissionProbability_perpendicular_slowRotation(sigma_rad_perp, sigma_tot_perp, sigma_tot_par, theta):

    weight = sigma_rad_perp * 2 * np.pi * (
        -np.sqrt(sigma_tot_perp-sigma_tot_par)*np.cos(theta) + 
        np.sqrt(sigma_tot_par)*np.arctan((np.sqrt(sigma_tot_perp-sigma_tot_par)-np.sqrt(sigma_tot_perp)*np.tan(theta/2))/(np.sqrt(sigma_tot_par))) + 
        np.sqrt(sigma_tot_par)*np.arctan((np.sqrt(sigma_tot_perp-sigma_tot_par)+np.sqrt(sigma_tot_perp)*np.tan(theta/2))/(np.sqrt(sigma_tot_par)))
        )/(sigma_tot_perp-sigma_tot_par)**(3/2)/(4*np.pi)
    
    return weight

# integrate sin(theta)^2/(a*cos(theta)^2 + b*sin(theta)^2) * sin(theta) dtheta
def integrateEmissionProbability_parallel_slowRotation(sigma_rad_par, sigma_tot_perp, sigma_tot_par, theta):

    weight = sigma_rad_par * np.pi * (
        np.sqrt(sigma_tot_par) * np.sqrt(sigma_tot_perp-sigma_tot_par)*np.cos(theta) - 
        sigma_tot_perp*np.arctan((np.sqrt(sigma_tot_perp-sigma_tot_par)-np.sqrt(sigma_tot_perp)*np.tan(theta/2))/(np.sqrt(sigma_tot_par))) - 
        sigma_tot_perp*np.arctan((np.sqrt(sigma_tot_perp-sigma_tot_par)+np.sqrt(sigma_tot_perp)*np.tan(theta/2))/(np.sqrt(sigma_tot_par)))
        )/(np.sqrt(sigma_tot_par) * (sigma_tot_perp-sigma_tot_par)**(3/2))/(4*np.pi)
    
    return weight


# integrate cos(x)^2*sin(x)
def integrateEmissionProbability_perpendicular_fastRotation(sigma_rad_perp, theta):

    weight = - sigma_rad_perp * 1/3 * np.cos(theta)**3
    # Should be normalized to the integrated total rate to really serve the probability, but the calculated polarization degree is correct...

    return weight

# integrate sin(x)^2*sin(x)
def integrateEmissionProbability_parallel_fastRotation(sigma_rad_par, theta):

    weight = sigma_rad_par * 1/12*(np.cos(3*theta)-9*np.cos(theta))

    return weight

def rate_poldeg_calulation(fluorophore, sphereRadius, collectionAngle):
    # Returns the rate enhancements of an ideal dipole (the intrinsic quantum
    # yield of the fluorophore is not taken into account here...).
    
    N = np.size(sphereRadius)
    
    emissionWavelength = np.linspace(fluorophore['emissionWavelength'], fluorophore['emissionWavelength'], num=N) # m
    
    
    dipoleDistance = np.linspace(18, 18, num=N) * 10 ** -9
    
    # OTHER PARAMETERS
    
    # refractive indices of the nanosphere and the immersion medium:
    compositionRatio = 0.3
    refractiveIndex_sphere = refractiveIndex_composite(
        refractiveIndex.gold(emissionWavelength),
        refractiveIndex.silver(emissionWavelength),
        compositionRatio
    )
    # refractiveIndex_sphere = refractiveIndex.gold(emissionWavelength)
    refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
    # refractiveIndex_medium = np.real(refractiveIndex.vacuum(emissionWavelength))
    # refractiveIndex_sphere = refractiveIndex_medium
    
    # position of the dipole
    # the radial coordinate of the dipole position ( initializing here as 0 as it
    # should be set in the loop over the sweep parameters:
    dipolePosition_r = 0.
    # the polar angle of the dipole position:
    dipolePosition_theta = 90. * np.pi/180
    # the azimuthal angle of the dipole position:
    dipolePosition_fi = 0. * np.pi/180
    # the position of the dipole emitter in spherical coordiante system centered at the sphere center:
    dipolePosition_sphericalComponents = np.array([
        [dipolePosition_r],
        [dipolePosition_theta],
        [dipolePosition_fi]])
    
    # PERPEDICULAR RATE CALCULATION
    
    # strength and orientation of the dipole (components of the dipole vector in
    # the spherical coordiante system at the dipole position, the "r", "theta"
    # and "fi" unit vectors, respectively):
    dipoleStrength = 1.0
    dipoleVector_theta = 0. * np.pi /180
    dipoleVector_fi = 0. * np.pi /180
    dipoleVector = dipoleStrength * np.array(
        [
            [np.cos(dipoleVector_theta)],
            [np.sin(dipoleVector_theta) * np.cos(dipoleVector_fi)],
            [np.sin(dipoleVector_theta) * np.sin(dipoleVector_fi)],
        ]
    )
    
    # alternatively:
    # perpendicular to the nanosphere surface:
    # dipoleVector = np.array([[1.0], [0.0], [0.0]])
    # parallel to the nanosphere surface:
    # dipoleVector=np.array([[0.], [1.], [0.]])
    # dipoleVector=np.array([[0.], [0.], [1.]])
    
    absorptionRate_perpendicular = np.zeros(emissionWavelength.shape)
    radiativeRate_perpendicular = np.zeros(emissionWavelength.shape)
    totalRate_perpendicular = np.zeros(emissionWavelength.shape)
    
    for idx in range(0, N):
    
        # update the diple radial position coordinate:
        dipolePosition_r = sphereRadius[idx] + dipoleDistance[idx]
        dipolePosition_sphericalComponents[0] = dipolePosition_r
    
        # calculate the modified dipole absorption and scattering rates:
        [sigma_a, sigma_s, sigma_t] = Mie_dipoleEmitter_rates(
           emissionWavelength[idx],
           sphereRadius[idx],
           dipolePosition_sphericalComponents,
           dipoleVector,
           refractiveIndex_sphere[idx],
           refractiveIndex_medium[idx],
           n_max
        )
    
        absorptionRate_perpendicular[idx] = sigma_a[0][0]
        radiativeRate_perpendicular[idx] = sigma_s[0][0]
        totalRate_perpendicular[idx] = sigma_t[0][0]
        
    # PARALLEL RATE CALCULATION
    
    # strength and orientation of the dipole (components of the dipole vector in
    # the spherical coordiante system at the dipole position, the "r", "theta"
    # and "fi" unit vectors, respectively):
    dipoleStrength = 1.0
    dipoleVector_theta = 90. * np.pi /180
    dipoleVector_fi = 0. * np.pi /180
    dipoleVector = dipoleStrength * np.array(
        [
            [np.cos(dipoleVector_theta)],
            [np.sin(dipoleVector_theta) * np.cos(dipoleVector_fi)],
            [np.sin(dipoleVector_theta) * np.sin(dipoleVector_fi)],
        ]
    )
    
    # alternatively:
    # perpendicular to the nanosphere surface:
    # dipoleVector = np.array([[1.0], [0.0], [0.0]])
    # parallel to the nanosphere surface:
    # dipoleVector=np.array([[0.], [1.], [0.]])
    # dipoleVector=np.array([[0.], [0.], [1.]])
    
    absorptionRate_parallel = np.zeros(emissionWavelength.shape)
    radiativeRate_parallel = np.zeros(emissionWavelength.shape)
    totalRate_parallel = np.zeros(emissionWavelength.shape)
    
    #[mu_0, epsilon_0, c_0] = Mie_physicalConstants.electromagnetic()
    
    for idx in range(0, N):
    
        # update the diple radial position coordinate:
        dipolePosition_r = sphereRadius[idx] + dipoleDistance[idx]
        dipolePosition_sphericalComponents[0] = dipolePosition_r
    
        # calculate the modified dipole absorption and scattering rates:
        [sigma_a, sigma_s, sigma_t] = Mie_dipoleEmitter_rates(
           emissionWavelength[idx],
           sphereRadius[idx],
           dipolePosition_sphericalComponents,
           dipoleVector,
           refractiveIndex_sphere[idx],
           refractiveIndex_medium[idx],
           n_max
        )
    
        #[k_0, refr_rel, k_medium, k_sphere] = Mie_physicalConstants.optical(emissionWavelength[idx], refractiveIndex_sphere[0], refractiveIndex_medium[0])
        #W_0 = freeSpaceRadiationRate(k_medium, c_0, mu_0, dipoleVector)
    
        # rate enhancements of a non ideal dipole moment
        absorptionRate_parallel[idx] = sigma_a[0][0]
        radiativeRate_parallel[idx] = sigma_s[0][0]
        totalRate_parallel[idx] = sigma_t[0][0] + 1/fluorophore['quantumYield'] - 1

    QE_perpendicular = radiativeRate_perpendicular/totalRate_perpendicular
    QE_parallel = radiativeRate_parallel/totalRate_parallel

    # POLARIZATION DEGREE

    # slow rotation
    emissionProbability_perpendicular = integrateEmissionProbability_perpendicular_slowRotation(radiativeRate_perpendicular, totalRate_perpendicular, totalRate_parallel, np.pi) - integrateEmissionProbability_perpendicular_slowRotation(radiativeRate_perpendicular, totalRate_perpendicular, totalRate_parallel, 0)
    emissionProbability_parallel = integrateEmissionProbability_parallel_slowRotation(radiativeRate_parallel, totalRate_perpendicular, totalRate_parallel, np.pi) - integrateEmissionProbability_parallel_slowRotation(radiativeRate_parallel, totalRate_perpendicular, totalRate_parallel, 0)
    

    # fast rotation:
    #emissionProbability_perpendicular = integrateEmissionProbability_perpendicular_fastRotation(radiativeRate_perpendicular, np.pi) - integrateEmissionProbability_perpendicular_fastRotation(radiativeRate_perpendicular, 0)
    #emissionProbability_parallel = integrateEmissionProbability_parallel_fastRotation(radiativeRate_parallel, np.pi) - integrateEmissionProbability_parallel_fastRotation(radiativeRate_parallel, 0)

    # TODO
    CE_lateral = 1.0
    CE_axial = 0.86
    #CE_axial = 0.0

    I_1 = CE_lateral * 0.96 * emissionProbability_perpendicular + CE_lateral * 0.04 * emissionProbability_parallel + 0.5 * CE_axial * emissionProbability_parallel
    I_2 = CE_lateral * 0.04 * emissionProbability_perpendicular + CE_lateral * 0.96 * emissionProbability_parallel + 0.5 * CE_axial * emissionProbability_parallel

    polarizationDegree_model = (I_1 - I_2) / (I_1 + I_2)


    # POLARIZATION DEGREE

    polarizationDegree_x_numerical = np.zeros(emissionWavelength.shape)
    polarizationDegree_y_numerical = np.zeros(emissionWavelength.shape)

    for idx in range(0, N):

        sphereRadius_act = sphereRadius[idx]

        dipoleDistance_act = dipoleDistance[idx]

        # OTHER PARAMETERS

        # number of different dipole positions:
        dipolePosition_numberOfPoints = 1

        # number of different dipole orientations:
        dipoleVector_numberOfPoints = 20
        #dipoleVector_numberOfPoints = 1

        # number of points for the electric field sampling:
        electricField_numberOfPoints_4PI = 1000

        # distance of the electric field sampling from the center, let it be the
        # experimental objective focal length:
        samplingDistance = 2*10**-3

        dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        #dipolePositionObject = Mie_dipoleVectors.dipole_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
        #dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideY(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        #dipolePositionObject = Mie_dipoleVectors.dipolePosition.top(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        #dipolePositionObject = Mie_dipoleVectors.dipolePosition.arbitrary(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints, 90*np.pi/180, 20*np.pi/180)
        #dipolePositionObject = Mie_dipoleVectors.dipolePosition.arbitrary(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints, 90*np.pi/180, 0*np.pi/180)

        dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
        #dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.perpendicular(dipoleVector_numberOfPoints)
        #dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.parallel_theta(dipoleVector_numberOfPoints)

        # slow rotation:
        excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
        emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
        #excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
        #emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()

        # fast rotation:
        #excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_fastRotation()
        #emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_fastRotation(fluorophore['quantumYield'])

        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree_orientation()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationChannelSignals()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizedPSF(32)
        intensityFilteringRatio = 0.0
        farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.sumSignal()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationChannelSignals()

        #signalNormalizationBoolean = True
        signalNormalizationBoolean = False

        farfieldQuantity, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )
        
        polarizationDegree_x_numerical[idx] = farfieldQuantity[0]
        
        dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideY(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        farfieldQuantity, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )
        
        polarizationDegree_y_numerical[idx] = -farfieldQuantity[0]

    return polarizationDegree_x_numerical, polarizationDegree_y_numerical, polarizationDegree_model

# collection (half) angle of the objective:
collectionAngle = math.asin(1.49/1.52)
#collectionAngle = np.pi

fluorophore = {}

fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92
#collectionAngle = 0.3
polarizationDegree_numerical_520_x, polarizationDegree_numerical_520_y, polarizationDegree_model_520 = rate_poldeg_calulation(fluorophore, sphereRadius, collectionAngle)

fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65
polarizationDegree_numerical_670_x, polarizationDegree_numerical_670_y, polarizationDegree_model_670 = rate_poldeg_calulation(fluorophore, sphereRadius, collectionAngle)


# VISUALIZATION

# The "x" quantity in case of wavelength sweep:
#x = emissionWavelength*10**9				# plot as the function of wavelength in nm
#xlabel = 'wavelength [nm]'

# The "x" quantity in case of particle radius sweep:

#x = sphereRadius*10**9
#xlabel = 'particle radius'

# The "x" quantity in case of particle diameter sweep:
x = 2 * sphereRadius * 10 ** 9
xlabel = "particle diameter [nm]"
# The "x" quantity in case of particle radius sweep, dimensionless:
# x = 2*np.pi*sphereRadius/emissionWavelength			# plot as the function of the
# dimensionless diffraction parameter
# xlabel = 'dimensionless diffraction parameter'

# The "x" quantity in case of dipole distance sweep:
# x = dipoleDistance*10**9				# plot as the function of wavelength in nm
# xlabel = 'dipole distance [nm]'

fig, ax = plt.subplots()

plt.plot(x, polarizationDegree_numerical_520_x, label='AF488, numerical simulation', color = 'blue', linestyle='dashed', linewidth=2)
plt.plot(x, polarizationDegree_numerical_520_y, color = 'blue', linestyle='dashed', linewidth=2)
ax.fill_between(x, polarizationDegree_numerical_520_x, polarizationDegree_numerical_520_y, alpha=0.4)
plt.plot(x, polarizationDegree_model_520, label='AF488, simplified model', color = 'blue', linewidth=2)
plt.plot(x, polarizationDegree_numerical_670_x, label='Atto647N, numerical simulation', color = 'red', linestyle='dashed', linewidth=2)
plt.plot(x, polarizationDegree_numerical_670_y, color = 'red', linestyle='dashed', linewidth=2)
ax.fill_between(x, polarizationDegree_numerical_670_x, polarizationDegree_numerical_670_y, alpha=0.4)
plt.plot(x, polarizationDegree_model_670, label='Atto647N, simplified model', color = 'red', linewidth=2)

plt.gca().set_ylim(top=1.0)

plt.legend()
#plt.legend(loc='upper left')

plt.xlabel(xlabel, fontsize=14)
plt.ylabel("degree of polarization", fontsize=14)

plt.show()
