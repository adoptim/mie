import numpy as np
import matplotlib.pyplot as plt
import math as math

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite
from Mie_calculation.Mie_dipoleEmitter_rates import Mie_dipoleEmitter_rates
import Mie_calculation.Mie_dipoleEmitter_rates_testing \
    as Mie_dipoleEmitter_rates_testing


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

# SWEEP PARAMETERS

n_max = 10

N = 1200

sphereRadius = 1 / 2 * np.linspace(40, 200, num=N) * 10 ** -9

def rate_poldeg_calulation(fluorophore, sphereRadius):
    
    N = np.size(sphereRadius)
    
    emissionWavelength = np.linspace(fluorophore['emissionWavelength'], fluorophore['emissionWavelength'], num=N) # m
    
    
    dipoleDistance = np.linspace(18, 18, num=N) * 10 ** -9
    
    # OTHER PARAMETERS
    
    # refractive indices of the nanosphere and the immersion medium:
    compositionRatio = 0.3
    refractiveIndex_sphere = refractiveIndex_composite(
        refractiveIndex.gold(emissionWavelength),
        refractiveIndex.silver(emissionWavelength),
        compositionRatio
    )
    # refractiveIndex_sphere = refractiveIndex.gold(emissionWavelength)
    refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
    # refractiveIndex_medium = np.real(refractiveIndex.vacuum(emissionWavelength))
    # refractiveIndex_sphere = refractiveIndex_medium
    
    # position of the dipole
    # the radial coordinate of the dipole position ( initializing here as 0 as it
    # should be set in the loop over the sweep parameters:
    dipolePosition_r = 0.
    # the polar angle of the dipole position:
    dipolePosition_theta = 90. * np.pi/180
    # the azimuthal angle of the dipole position:
    dipolePosition_fi = 0. * np.pi/180
    # the position of the dipole emitter in spherical coordiante system centered at the sphere center:
    dipolePosition_sphericalComponents = np.array([
        [dipolePosition_r],
        [dipolePosition_theta],
        [dipolePosition_fi]])
    
    #PERPENDICULAR
    # strength and orientation of the dipole (components of the dipole vector in
    # the spherical coordiante system at the dipole position, the "r", "theta"
    # and "fi" unit vectors, respectively):
    dipoleStrength = 1.0
    dipoleVector_theta = 0. * np.pi /180
    dipoleVector_fi = 0. * np.pi /180
    dipoleVector = dipoleStrength * np.array(
        [
            [np.cos(dipoleVector_theta)],
            [np.sin(dipoleVector_theta) * np.cos(dipoleVector_fi)],
            [np.sin(dipoleVector_theta) * np.sin(dipoleVector_fi)],
        ]
    )
    
    # alternatively:
    # perpendicular to the nanosphere surface:
    # dipoleVector = np.array([[1.0], [0.0], [0.0]])
    # parallel to the nanosphere surface:
    # dipoleVector=np.array([[0.], [1.], [0.]])
    # dipoleVector=np.array([[0.], [0.], [1.]])
    
    # RATE CALCULATION
    
    absorptionRate_perp = np.zeros(emissionWavelength.shape)
    scatteringRate_perp = np.zeros(emissionWavelength.shape)
    totalRate_perp = np.zeros(emissionWavelength.shape)
    
    for idx in range(0, N):
    
        # update the diple radial position coordinate:
        dipolePosition_r = sphereRadius[idx] + dipoleDistance[idx]
        dipolePosition_sphericalComponents[0] = dipolePosition_r
    
        # calculate the modified dipole absorption and scattering rates:
        [W_a, W_s, W_t] = Mie_dipoleEmitter_rates(
           emissionWavelength[idx],
           sphereRadius[idx],
           dipolePosition_sphericalComponents,
           dipoleVector,
           refractiveIndex_sphere[idx],
           refractiveIndex_medium[idx],
           n_max
        )
    
        absorptionRate_perp[idx] = W_a
        scatteringRate_perp[idx] = W_s
        totalRate_perp[idx] = W_t
        
    #PARALLEL
    # strength and orientation of the dipole (components of the dipole vector in
    # the spherical coordiante system at the dipole position, the "r", "theta"
    # and "fi" unit vectors, respectively):
    dipoleStrength = 1.0
    dipoleVector_theta = 90. * np.pi /180
    dipoleVector_fi = 0. * np.pi /180
    dipoleVector = dipoleStrength * np.array(
        [
            [np.cos(dipoleVector_theta)],
            [np.sin(dipoleVector_theta) * np.cos(dipoleVector_fi)],
            [np.sin(dipoleVector_theta) * np.sin(dipoleVector_fi)],
        ]
    )
        
    # alternatively:
    # perpendicular to the nanosphere surface:
    # dipoleVector = np.array([[1.0], [0.0], [0.0]])
    # parallel to the nanosphere surface:
    # dipoleVector=np.array([[0.], [1.], [0.]])
    # dipoleVector=np.array([[0.], [0.], [1.]])
    
    # RATE CALCULATION
    
    absorptionRate_par = np.zeros(emissionWavelength.shape)
    scatteringRate_par = np.zeros(emissionWavelength.shape)
    totalRate_par = np.zeros(emissionWavelength.shape)
    
    for idx in range(0, N):
    
        # update the diple radial position coordinate:
        dipolePosition_r = sphereRadius[idx] + dipoleDistance[idx]
        dipolePosition_sphericalComponents[0] = dipolePosition_r
    
        # calculate the modified dipole absorption and scattering rates:
        [W_a, W_s, W_t] = Mie_dipoleEmitter_rates(
            emissionWavelength[idx],
            sphereRadius[idx],
            dipolePosition_sphericalComponents,
            dipoleVector,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            n_max
        )
    
        absorptionRate_par[idx] = W_a
        scatteringRate_par[idx] = W_s
        totalRate_par[idx] = W_t
    
    K = 1./fluorophore['quantumYield'] - 1.
    QE_perp = scatteringRate_perp / (totalRate_perp + K)
    QE_par = scatteringRate_par / (totalRate_par + K)
    
    #radiativeRate = (QE_perp*scatteringRate_perp - QE_par*scatteringRate_par) / (QE_perp*scatteringRate_perp + 2*QE_par*scatteringRate_par)
    radiativeRate = (scatteringRate_perp - scatteringRate_par) / (scatteringRate_perp + 2*scatteringRate_par)
    #radiativeRate = (QE_perp*scatteringRate_perp - QE_par*scatteringRate_par)  / (QE_perp+2*QE_par)
    
    
    # POLARIZATION DEGREE
    
    polarizationDegree = np.zeros(emissionWavelength.shape)
    
    for idx in range(0, N):
    
        sphereRadius_act = sphereRadius[idx]
    
        dipoleDistance_act = dipoleDistance[idx]
    
        # OTHER PARAMETERS
    
        # number of different dipole positions:
        dipolePosition_numberOfPoints = 1
    
        # number of different dipole orientations:
        dipoleVector_numberOfPoints = 200
        #dipoleVector_numberOfPoints = 1
    
        # number of points for the electric field sampling:
        electricField_numberOfPoints_4PI = 500
    
        # distance of the electric field sampling from the center, let it be the
        # experimental objective focal length:
        samplingDistance = 2*10**-3
    
        # collection (half) angle of the objective
        # TODO: water vs immersion layer
        collectionAngle = math.asin(1.49/1.52)
        # collectionAngle = np.pi
    
    
        dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)

        dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
        #dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.perpendicular(dipoleVector_numberOfPoints)
        #dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.parallel_theta(dipoleVector_numberOfPoints)
    
        # slow rotation:
        excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
        #excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
        emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
        #emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()
    
        # fast rotation:
        #excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_fastRotation()
        #emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_fastRotation(fluorophore['quantumYield'])
        
    
        # fast rotation:
        #excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
        #emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()
    
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree_orientation()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationChannelSignals()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizedPSF(32)
        intensityFilteringRatio = 0.0
        farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.sumSignal()
    
        #signalNormalizationBoolean = True
        signalNormalizationBoolean = False
    
        farfieldQuantity, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )
    
        polarizationDegree[idx] = farfieldQuantity
        
    return radiativeRate, polarizationDegree


fluorophore = {}

fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92
radiativeRate_520, polarizationDegree_520 = rate_poldeg_calulation(fluorophore, sphereRadius)

fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65
radiativeRate_670, polarizationDegree_670 = rate_poldeg_calulation(fluorophore, sphereRadius)


# VISUALIZATION

# The "x" quantity in case of wavelength sweep:
#x = emissionWavelength*10**9				# plot as the function of wavelength in nm
#xlabel = 'wavelength [nm]'

# The "x" quantity in case of particle radius sweep:

#x = sphereRadius*10**9
#xlabel = 'particle radius'

# The "x" quantity in case of particle diameter sweep:
x = 2 * sphereRadius * 10 ** 9
xlabel = "particle diameter"
# The "x" quantity in case of particle radius sweep, dimensionless:
# x = 2*np.pi*sphereRadius/emissionWavelength			# plot as the function of the
# dimensionless diffraction parameter
# xlabel = 'dimensionless diffraction parameter'

# The "x" quantity in case of dipole distance sweep:
# x = dipoleDistance*10**9				# plot as the function of wavelength in nm
# xlabel = 'dipole distance [nm]'

fig, ax1 = plt.subplots()

#ax1.plot(x, totalRate, label='Total rate')
ax1.plot(x, radiativeRate_520, label='interaction parameter, AF488', color = 'darkcyan', linestyle='dashed', linewidth=2)
#ax1.plot(x, absorptionRate, label='Absorption rate')
ax1.set_xlabel(xlabel, size = 14)
ax1.set_ylabel("interaction parameter", color = 'darkcyan', size = 14)
ax1.tick_params(axis ='y', labelcolor = 'darkcyan')
#plt.ylim(0, 40)

#ax1.plot(x, totalRate, label='Total rate')
ax1.plot(x, radiativeRate_670, label='interaction parameter, Atto647N', color = 'darkcyan', linestyle='dotted', linewidth=2)
#ax1.plot(x, absorptionRate, label='Absorption rate')
#plt.ylim(0, 40)

ax1.legend(loc='lower right')

ax2 = ax1.twinx()

ax2.set_ylabel('degree of polarization', color = 'darkmagenta', size = 14)
ax2.plot(x, polarizationDegree_520, label='degree of pol., AF488', color = 'darkmagenta', linestyle='dashed', linewidth=2)
ax2.tick_params(axis ='y', labelcolor = 'darkmagenta')

ax2.plot(x, polarizationDegree_670, label='degree of pol., Atto647N', color = 'darkmagenta', linestyle='dotted', linewidth=2)
ax2.tick_params(axis ='y', labelcolor = 'darkmagenta')

ax2.legend(loc='upper right', bbox_to_anchor=(0.886, 0.3))

plt.show()

plt.figure()
#plt.scatter(polarizationDegree_520, radiativeRate_520, c=x, cmap='viridis')
plt.scatter(polarizationDegree_520, radiativeRate_520, c=x, cmap='jet')
cbar = plt.colorbar()
cbar.ax.set_ylabel('particle diameter (nm)', size = 14)
plt.xlabel('polarization degree', size = 14)
plt.ylabel('interaction parameter', size = 14)
plt.show()

plt.figure()
#plt.scatter(polarizationDegree_670, radiativeRate_670, c=x, cmap='viridis')
plt.scatter(polarizationDegree_670, radiativeRate_670, c=x, cmap='jet')
cbar = plt.colorbar()
cbar.ax.set_ylabel('particle diameter (nm)', size = 14)
plt.xlabel('polarization degree', size = 14)
plt.ylabel('interaction parameter', size = 14)
plt.show()
