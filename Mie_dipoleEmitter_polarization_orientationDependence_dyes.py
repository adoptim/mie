import numpy as np
import math as math

import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

n_max = 10

sphereRadius = 1 / 2 * 80 * 10 ** -9

# OTHER PARAMETERS

dipoleDistance = 18 * 10 ** -9

# number of different dipole positions:
dipolePosition_numberOfPoints = 100

# number of different dipole orientations:
tiltingNumber = dipolePosition_numberOfPoints

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)
# collectionAngle = np.pi



intensityFilteringRatio = 0.0
farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)

signalNormalizationBoolean = False

fluorophore = {}
fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92


# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
#refractiveIndex_sphere = refractiveIndex_medium

# no need to weighting the excitation or the emission as there is no orientation averaging
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()

dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_520_phi, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_theta(tiltingNumber)
farfieldQuantity_520_theta, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)


fluorophore = {}
fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65


# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
#refractiveIndex_sphere = refractiveIndex_medium

# no need to weighting the excitation or the emission as there is no orientation averaging
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()

dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_670_phi, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_theta(tiltingNumber)
farfieldQuantity_670_theta, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)


dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
refractiveIndex_sphere = refractiveIndex_medium
farfieldQuantity_controll_phi, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_theta(tiltingNumber)
refractiveIndex_sphere = refractiveIndex_medium
farfieldQuantity_controll_theta, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)

angles = np.linspace(0, 90, tiltingNumber)

plt.figure()
plt.xlabel('angle of tilt [°]', fontsize=14)
#plt.xlabel('Polar angle [°]')
plt.ylabel('$S_1$ Stokes parameter', fontsize=14)
plt.plot(angles, farfieldQuantity_520_phi, label='520 nm emission, polar tilting', linewidth=2, linestyle='dashed', color='blue')
plt.plot(angles, farfieldQuantity_520_theta, label='520 nm emission, azimuth tilting', linewidth=2, linestyle='dotted', color='blue')
plt.plot(angles, farfieldQuantity_670_phi, label='670 nm emission, polar tilting', linewidth=2, linestyle='dashed', color='red')
plt.plot(angles, farfieldQuantity_670_theta, label='670 nm emission, azimuth tilting', linewidth=2, linestyle='dotted', color='red')
plt.plot(angles, farfieldQuantity_controll_phi, label='without nanoparticle, polar tilting', linewidth=2, linestyle='dashed', color='black')
plt.plot(angles, farfieldQuantity_controll_theta, label='without nanoparticle, azimuth tilting', linewidth=2, linestyle='dotted', color='black')
plt.legend()

plt.xticks(ticks=np.array([0, 15, 30, 45, 60, 75, 90]));

plt.show()

