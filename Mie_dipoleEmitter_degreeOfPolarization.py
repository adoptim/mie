import numpy as np
import math as math

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

import matplotlib.pyplot as plt
import os
from tkinter import filedialog

workingDirectory = filedialog.askdirectory()
#simulationName = "AF488_55nm"
simulationName = "Atto647N_55nm"


fluorophore = {}
fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65
#fluorophore['excitationWavelength'] = 488 * 10 ** -9
#fluorophore['emissionWavelength'] = 520 * 10 ** -9
#fluorophore['quantumYield'] = 0.92

n_max = 10

sphereRadius = 1 / 2 * 55 * 10 ** -9


dipoleDistance = 18 * 10 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
# refractiveIndex_medium = np.ones((refractiveIndex_medium.size))
# refractiveIndex_sphere = refractiveIndex_medium

# number of different dipole positions:
dipolePosition_numberOfPoints = 12345

# number of different dipole orientations:
dipoleVector_numberOfPoints = 1000

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)
# collectionAngle = np.pi


dipolePositionObject = Mie_dipoleVectors.dipolePosition.evenDistribution_4pi(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
# testing:
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.perpendicular(1)

# slow rotation:
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])


# fast rotation:
#excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_fastRotation()
#emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_fastRotation(fluorophore['quantumYield'])


intensityFilteringRatio = 0.0
farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.Stokes_parameters()

signalNormalizationBoolean = True

farfieldQuantity, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)



degreeOfPolarization = np.sqrt(farfieldQuantity[:,1]**2+farfieldQuantity[:,2]**2+farfieldQuantity[:,3]**2)/farfieldQuantity[:,0]

histogramFigure = visualization.degreeOfPolarizationHistogram(degreeOfPolarization, sphereRadius, fluorophore['emissionWavelength'])

histogramFigureName = os.path.join(workingDirectory, "degreeOfPolarizationHistogram_{}.png".format(simulationName))
meshFigureName = os.path.join(workingDirectory, "degreeOfPolarizationMesh_{}.png".format(simulationName))
histogramDataName = os.path.join(workingDirectory, "degreeOfPolarizationData_{}.csv".format(simulationName))

plt.savefig(histogramFigureName)
#plt.savefig(meshFigureName)
outputData = farfieldQuantity
np.savetxt(histogramDataName, outputData, delimiter=",")
