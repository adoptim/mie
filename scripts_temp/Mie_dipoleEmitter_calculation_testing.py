
def arrayLinearIndexCheck(
    emissionWavelength,
    sphereRadius,
    dipolePosition_sphericalCoordinates,
    positionVector_electricField,
    refractiveIndex_sphere,
    refractiveIndex_medium
):

    n_max=3;

    # expansion coefficients for all n, m, sigma index and for all dipole orientation:
    (s_array, t_array, u_array, v_array) = Mie_coefficients.expansionCoefficients_orientationArray(dipoleVector, dipolePosition_sphericalCoordinates, sphereRadius, emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium, n_max)

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    
    for n in range(1, n_max+1):

        for m in range(0, n+1):

            for sigma in range(1, 3):
    
                linearIndex_n_m_sigma = Mie_summationAccessaries.indexMapping_n_m_sigma(n, m, sigma)
                s_sigma_m_n = s_array[:,linearIndex_n_m_sigma]
                t_sigma_m_n = t_array[:,linearIndex_n_m_sigma]
                
                j_array_dipole, j_der_array_dipole = sphericalFunctions.sphericalBessel_firstOrder(n_max, k_medium*dipolePosition_sphericalCoordinates[0, :])
                h_array_dipole, h_der_array_dipole = sphericalFunctions.sphericalHankel_firstOrder(n_max, k_medium*dipolePosition_sphericalCoordinates[0, :], j_array_dipole, j_der_array_dipole)
                P_array_dipole, P_m_per_sinTheta_array_dipole, P_der_array_dipole = sphericalFunctions.associatedLegendrePolynomials(n_max, dipolePosition_sphericalCoordinates[1, :])
                
                linearIndex_n = Mie_summationAccessaries.indexMapping_n(n)
                linearIndex_n_m = Mie_summationAccessaries.indexMapping_n_m(n, m)
                j_n = j_array_dipole[:,linearIndex_n]
                j_n_der = j_der_array_dipole[:,linearIndex_n]
                h_n = h_array_dipole[:,linearIndex_n]
                h_n_der = h_der_array_dipole[:,linearIndex_n]
                P_m_n = P_array_dipole[:,linearIndex_n_m]
                P_m_per_sinTheta_m_n = P_m_per_sinTheta_array_dipole[:,linearIndex_n_m]
                P_m_n_der = P_der_array_dipole[:,linearIndex_n_m]
                s_sigma_m_n_2, t_sigma_m_n_2, p_sigma_m_n_2, q_sigma_m_n_2 = Mie_coefficients.expansionCoefficients_dipoleField(k_medium, epsilon_0, n, m, sigma, k_medium*dipolePosition_sphericalCoordinates[0, :], dipolePosition_sphericalCoordinates[1, :], dipolePosition_sphericalCoordinates[2, :], j_n, j_n_der, h_n, h_n_der, P_m_n, P_m_per_sinTheta_m_n, P_m_n_der, dipoleVector)
                
                print(n, m, sigma, s_sigma_m_n*D_sequence[linearIndex_n_m_sigma], s_sigma_m_n_2, D_sequence[linearIndex_n_m_sigma])
                print(n, m, sigma, t_sigma_m_n*D_sequence[linearIndex_n_m_sigma], t_sigma_m_n_2, D_sequence[linearIndex_n_m_sigma])
    
    
def sphericalVectorWave_normalization(
    emissionWavelength,
    sphereRadius,
    dipolePosition_sphericalCoordinates,
    positionVector_electricField,
    refractiveIndex_sphere,
    refractiveIndex_medium
):

    # CONSTANTS

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)

    n_max=1;


    kr=k_medium * positionVector_electricField[0,0]
    j_vect, j_der_vect = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_vect, h_der_vect = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr, j_vect, j_der_vect)

    # spherical vector waves for all n, m, sigma index and for all sampling position:
    (M_3_array, N_3_array) = Mie_sphericalVectorWaves.arraysForDipoleElectricField(positionVector_electricField, emissionWavelength, refractiveIndex_medium, n_max)

    for n in range(1, n_max+1):
        
        linearIndex_n = Mie_summationAccessaries.indexMapping_n(n)
        
        if n==1:
            h_n__1 = special.spherical_jn(n-1, kr)
        else:
            h_n__1 = h_vect[:, linearIndex_n-1]
        h_n = h_vect[:, linearIndex_n]
        if n==n_max:
            h_n_1 = special.spherical_jn(n+1, kr)
        else:
            h_n_1  = h_vect[:, linearIndex_n+1]
        
        for m in range(0, n+1):

            for sigma in range(1, 3):
    
                linearIndex_n_m_sigma = Mie_summationAccessaries.indexMapping_n_m_sigma(n, m, sigma)
                
                M_3 = M_3_array[:,linearIndex_n_m_sigma,:]
                N_3 = N_3_array[:,linearIndex_n_m_sigma,:]
                
                if m>0:
                    delta=0
                elif m==0:
                    delta=1
                
                normalization_M_3 = (1+delta)*2*np.pi/(2*n+1)*math.factorial(n+m)/math.factorial(n-m)*n*(n+1)*np.abs(h_n)**2
                # TODO: why is not it correct for the N_3 ( as in the Straton book)????
                normalization_N_3 = (1+delta)*2*np.pi/(2*n+1)**2*math.factorial(n+m)/math.factorial(n-m)*n*(n+1)*((n+1)*np.abs(h_n__1)**2 + n*np.abs(h_n_1)**2)
                
                # integrating to unit sphere (as in the Straton book)
                delta_f = 4*1**2*np.pi/electricField_numberOfPoints
                M_3_integrated = np.sum(M_3 * np.conjugate(M_3)) * delta_f / (normalization_M_3)
                N_3_integrated = np.sum(N_3 * np.conjugate(N_3)) * delta_f / (normalization_N_3)
                
                print(n, m, sigma, "M_3", M_3_integrated, np.sum(M_3 * np.conjugate(M_3)))
                print(n, m, sigma, "N_3", N_3_integrated, np.sum(N_3 * np.conjugate(N_3)))


def LegendreIntegral(n_1, n_2, m):

    N = 100
    theta = np.linspace(0.00, np.pi, N+1)

    d_theta = np.pi/N

    n_max = max(n_1, n_2)
    P_array, P_m_per_sinTheta_array, P_der_array = sphericalFunctions.associatedLegendrePolynomials(n_max, theta)

    linear_index_1 = Mie_summationAccessaries.indexMapping_n_m(n_1, m)
    linear_index_2 = Mie_summationAccessaries.indexMapping_n_m(n_2, m)

    P_m_n_1 = P_array[:, linear_index_1]
    P_m_n_2 = P_array[:, linear_index_2]

    P_der_m_n_1 = P_der_array[:, linear_index_1]
    P_der_m_n_2 = P_der_array[:, linear_index_2]

    P_m_per_sinTheta_m_n_1 = P_m_per_sinTheta_array[:, linear_index_1]
    P_m_per_sinTheta_m_n_2 = P_m_per_sinTheta_array[:, linear_index_2]

    # integration = np.sum((P_der_m_n_1*P_der_m_n_2 + m**2*P_m_n_1*P_m_n_2/np.sin(theta)**2)*np.sin(theta)*d_theta)
    integration = np.sum((P_der_m_n_1*P_der_m_n_2 + P_m_per_sinTheta_m_n_1*P_m_per_sinTheta_m_n_2)*np.sin(theta)*d_theta)

    if n_1 == n_2:
        n = n_1
        formula = 2/(2*n+1)*math.factorial(n+m)/math.factorial(n-m)*n*(n+1)
    else:
        formula = 0

    print(integration, formula)


def sphericalVectorWave_normalization2(
    emissionWavelength,
    sphereRadius,
    dipolePosition_sphericalCoordinates,
    refractiveIndex_sphere,
    refractiveIndex_medium
):

    # CONSTANTS

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = Mie_physicalConstants.optical(emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)

    n_max=2;

    N = 1000
    positionVector_electricField[0,:] = 2*10**-3 *np.ones(1000)
    positionVector_electricField[1,:] = np.linspace(0.00, np.pi, N)
    positionVector_electricField[2,:] = 70 * np.pi/180 *np.ones(1000)
    d_theta = np.pi/(N-1)

    kr=k_medium * positionVector_electricField[0,:]
    theta = positionVector_electricField[1,:]
    phi = positionVector_electricField[2,:]

    j_vect, j_der_vect = sphericalFunctions.sphericalBessel_firstOrder(n_max, kr[0])
    h_vect, h_der_vect = sphericalFunctions.sphericalHankel_firstOrder(n_max, kr[0], j_vect, j_der_vect)


    P_array, P_m_per_sinTheta_array, P_der_array = sphericalFunctions.associatedLegendrePolynomials(n_max, theta)

    # spherical vector waves for all n, m, sigma index and for all sampling position:
    #(M_3_array, N_3_array) = Mie_sphericalVectorWaves.arraysForDipoleElectricField(positionVector_electricField, emissionWavelength, refractiveIndex_medium, n_max)

    for n in range(1, n_max+1):

        linearIndex_n = Mie_summationAccessaries.indexMapping_n(n)

        if n==1:
            h_n__1 = special.spherical_jn(n-1, kr)
        else:
            h_n__1 = h_vect[:, linearIndex_n-1]
        h_n = h_vect[:, linearIndex_n]
        h_der_n = h_der_vect[:, linearIndex_n]
        if n==n_max:
            h_n_1 = special.spherical_jn(n+1, kr)
        else:
            h_n_1  = h_vect[:, linearIndex_n+1]

        for m in range(0, n+1):

            linearIndex_n_m = Mie_summationAccessaries.indexMapping_n_m(n, m)

            P_m_n = P_array[:, linearIndex_n_m]
            P_der_m_n = P_der_array[:, linearIndex_n_m]
            P_m_per_sinTheta_m_n = P_m_per_sinTheta_array[:, linearIndex_n_m]

            for sigma in range(1, 3):

                linearIndex_n_m_sigma = Mie_summationAccessaries.indexMapping_n_m_sigma(n, m, sigma)

                # M_3 = M_3_array[:,linearIndex_n_m_sigma,:]
                # N_3 = N_3_array[:,linearIndex_n_m_sigma,:]

                M_3 = Mie_sphericalVectorWaves.M_3(n, m, sigma, theta, phi, h_n, P_m_n, P_m_per_sinTheta_m_n, P_der_m_n)
                N_3 = Mie_sphericalVectorWaves.N_3(n, m, sigma, kr[0], theta, phi, h_n, h_der_n, P_m_n, P_m_per_sinTheta_m_n, P_der_m_n)

                if m>0:
                    delta=0
                elif m==0:
                    delta=1

                print(N_3.shape)
                # integrating along a line, discarding the radial component
                M_3_integrated = np.sum(M_3 * np.conjugate(M_3)) * d_theta / np.abs(h_n)**2
                N_3_integrated = np.sum(N_3 * np.conjugate(N_3)) * d_theta / np.abs(h_n / kr[0] + h_der_n)**2
                
                N_3_integrated = np.sum(N_3[:,0] * np.conjugate(N_3[:,0])+N_3[:,1] * np.conjugate(N_3[:,1])+N_3[:,2] * np.conjugate(N_3[:,2])) * d_theta / np.abs(h_n / kr[0] + h_der_n)**2

                M_3_formula = 2/(2*n+1)*math.factorial(n+m)/math.factorial(n-m)*n*(n+1)
                N_3_formula = 2/(2*n+1)*math.factorial(n+m)/math.factorial(n-m)*n*(n+1)

                print(n, m, sigma, "M_3", M_3_integrated, M_3_formula)
                print(n, m, sigma, "N_3", N_3_integrated, N_3_formula)
