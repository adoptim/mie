import numpy as np
import math as math

import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite


import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleFarField_derivedQuantities as Mie_dipoleFarField_derivedQuantities

emissionWavelength = 670 * 10 ** -9

sphereRadius = 1 / 2 * 80 * 10 ** -9

dipoleDistance = 18 * 10 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(emissionWavelength),
    refractiveIndex.silver(emissionWavelength),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
# refractiveIndex_medium = np.ones((refractiveIndex_medium.size))
# refractiveIndex_sphere = refractiveIndex_medium

# position of the dipole
# the radial coordinate of the dipole position ( initializing here as 0 as it
# should be set in the loop over the sweep parameters:
dipolePosition_r = sphereRadius + dipoleDistance
# the polar angle of the dipole position:
dipolePosition_theta = 7.3 * np.pi/180
# the azimuthal angle of the dipole position:
dipolePosition_fi = 24. * np.pi/180
# the position of the dipole emitter in spherical coordiante system centered at
# the sphere center:
dipolePosition_sphericalCoordinates = np.array([
    [dipolePosition_r],
    [dipolePosition_theta],
    [dipolePosition_fi]])

# number of different dipole positions:
dipolePosition_numberOfPoints = 1000

# number of different dipole orientations:
dipoleVector_numberOfPoints = 200

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)


# excitationWeightingFunction = Mie_dipoleFarField_derivedQuantities.excitationWeighting.none()
excitationWeightingFunction = Mie_dipoleFarField_derivedQuantities.excitationWeighting.planeWave_direction_X_polarization_Z()
emissionWeightingFunction = Mie_dipoleFarField_derivedQuantities.emissionWeighting.none()
# emissionWeightingFunction = Mie_dipoleFarField_derivedQuantities.emissionWeighting.excitationLimitedCase()


quantityDerivation = Mie_dipoleFarField_derivedQuantities.derivedQuantity.polarizationDegree()
#quantityDerivation = Mie_dipoleFarField_derivedQuantities.derivedQuantity.polarizedPSF(32)


farfieldQuantity, dipolePosition = Mie_dipoleFarField_derivedQuantities.getDistribution_position(
    emissionWavelength,
    sphereRadius,
    dipoleDistance,
    dipolePosition_r,
    dipolePosition_numberOfPoints,
    dipoleVector_numberOfPoints,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    quantityDerivation
)


    
# polarization degre calculation:
polarizationValues = (
    (farfieldQuantity[:, 0] - farfieldQuantity[:, 1])
    / (farfieldQuantity[:, 0] + farfieldQuantity[:, 1]))

# testing, intensity ratio of the channels:
#polarizationValues = channelSignal[0, :]/channelSignal[1, :]
#polarizationValues[polarizationValues>1]=1/polarizationValues[polarizationValues>1]

# plotting the polarization degree histogram:
x, bins, p = plt.hist(polarizationValues, bins=55, range=(-1, 1))
for item in p:
    item.set_height(item.get_height()/max(x))
plt.xlabel('Pol deg')
#plt.xlabel('channel signal ratio')
plt.ylabel('Rel # of blinkings')
plt.ylim(0,1.1)
plt.title('SphereDiameter: {:.0f} nm, Wavelength: {:.0f} nm'.format(sphereRadius*2E9, emissionWavelength*1E9))
plt.show()

# visualizing the polarization or intensity distribution of the blinking
# events on the spherical surface:
# visualization.sphere_mesh3D(dipolePosition, totalSignal)
# visualization.sphere_mesh3D(dipolePosition, polarizationValues)
