import numpy as np
import math as math

import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

from Mie_calculation.Mie_dipoleEmitter_rates import Mie_dipoleEmitter_rates

fluorophore = {}
fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65
#fluorophore['excitationWavelength'] = 488 * 10 ** -9
#fluorophore['emissionWavelength'] = 520 * 10 ** -9
#fluorophore['quantumYield'] = 0.92

n_max = 10

sphereRadius = 1 / 2 * 100 * 10 ** -9

dipoleDistance = 18 * 10 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
refractiveIndex_medium = np.ones((refractiveIndex_medium.size))
#refractiveIndex_sphere = refractiveIndex_medium

# number of different dipole positions:
dipolePosition_numberOfPoints = 100

# number of different dipole orientations:
tiltingNumber = dipolePosition_numberOfPoints

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)
# collectionAngle = np.pi


#dipolePositionObject = Mie_dipoleVectors.dipolePosition.top(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
#dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideY(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_theta(tiltingNumber)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
#testing:
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.perpendicular(1)
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.parallel_theta(1)
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.parallel_phi(1)

# slow rotation:
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()

fluorophore = {}
fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92
#quenchingCoeff = 8
#concentration = 1
#fluorophore['quantumYield'] = fluorophore['quantumYield'] / (concentration * quenchingCoeff)
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])

intensityFilteringRatio = 0.0
farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)

signalNormalizationBoolean = True

# dipole positions, evenly distributed on a sphere aroung the spherical
# particle, 3xN array:
dipolePosition = dipolePositionObject.getPositions()

QE_520 = np.zeros([tiltingNumber])
for idxOrientation in range(tiltingNumber):
    
    dipoleVector = dipoleOrientationObject.getOrientations()
    
    dipolePosition_actual = np.zeros([3, 1])
    dipolePosition_actual[:, 0] = dipolePosition[:, idxOrientation]
    
    # get the fluorescence rates (total, radiate, non-radiative) for
    # the quantum efficiency calculation:
    absorptionRate, scatteringRate, totalRate =  Mie_dipoleEmitter_rates(
        fluorophore['emissionWavelength'],
        sphereRadius,
        dipolePosition_actual,
        dipoleVector,
        refractiveIndex_sphere,
        refractiveIndex_medium,
        n_max
    )
    
    # calculate the quantum efficiency accounting for the instrinsic
    # quantum efficiency (QE_0) of the fluorophore:
    QE_520[idxOrientation] = scatteringRate / (totalRate + 1 / fluorophore['quantumYield'] - 1)




#dipolePositionObject = Mie_dipoleVectors.dipolePosition.top(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
#dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideY(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_theta(tiltingNumber)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
#testing:
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.perpendicular(1)
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.parallel_theta(1)
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.parallel_phi(1)

fluorophore = {}
fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65
#quenchingCoeff = 8
#concentration = 1
#fluorophore['quantumYield'] = fluorophore['quantumYield'] / (concentration * quenchingCoeff)
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])

QE_670 = np.zeros([tiltingNumber])
for idxOrientation in range(tiltingNumber):
    
    dipoleVector = dipoleOrientationObject.getOrientations()
    
    dipolePosition_actual = np.zeros([3, 1])
    dipolePosition_actual[:, 0] = dipolePosition[:, idxOrientation]
    
    # get the fluorescence rates (total, radiate, non-radiative) for
    # the quantum efficiency calculation:
    absorptionRate, scatteringRate, totalRate =  Mie_dipoleEmitter_rates(
        fluorophore['emissionWavelength'],
        sphereRadius,
        dipolePosition_actual,
        dipoleVector,
        refractiveIndex_sphere,
        refractiveIndex_medium,
        n_max
    )
    
    # calculate the quantum efficiency accounting for the instrinsic
    # quantum efficiency (QE_0) of the fluorophore:
    QE_670[idxOrientation] = scatteringRate / (totalRate + 1 / fluorophore['quantumYield'] - 1)


angles = np.linspace(0, 90, tiltingNumber)

plt.xlabel('Polar angle [°]')
plt.ylabel('Polarization degree')
plt.plot(angles, QE_520, label='520 nm emission')
plt.plot(angles, QE_670, label='670 nm distance')
plt.legend()
plt.show()

