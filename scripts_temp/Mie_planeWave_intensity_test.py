import numpy as np
import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite

import Mie_calculation.auxiliary.coordinateManagement as coordinateManagement
import Mie_calculation.Mie_electricField as Mie_electricField

import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

excitationWavelength = 647 * 1E-9

sphereRadius = 1 / 2 * 80 * 10 ** -9

distance = 18 * 10 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(excitationWavelength),
    refractiveIndex.silver(excitationWavelength),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(excitationWavelength))
# refractiveIndex_medium = np.ones((refractiveIndex_medium.size))
# refractiveIndex_sphere = refractiveIndex_medium

# number of different dipole positions:
position_numberOfPoints = 100


positionObject = Mie_dipoleVectors.dipolePosition.evenDistribution_4pi(sphereRadius, distance, position_numberOfPoints)


position = positionObject.getPositions()

excitationStrength = np.array([position_numberOfPoints])
for idxPosition in range(position_numberOfPoints):
    
    position_actual = np.zeros([3, 1])
    position_actual[:, 0] = position[:, idxPosition]
    
    k_medium = refractiveIndex_medium * 2 * np.pi / excitationWavelength
    refractiveIndex_relative = refractiveIndex_sphere / refractiveIndex_medium
    
    # calculate the excitation electric field in Descartes coordinates:
    excitationField_spherical = Mie_electricField.planeWave_direction_X_polarization_Z(position_actual, sphereRadius, k_medium, refractiveIndex_relative)
    excitationField_Descartes = coordinateManagement.sphericalComponentsToDescartes(position_actual, excitationField_spherical)
    
    # the excitation intensity (after averaging, the excitation rate is
    # linerarily proportional to this to this):
    excitationStrength[idxPosition] = np.sum(excitationField_Descartes * np.conjugate(excitationField_Descartes))

# visualizing the polarization or intensity distribution of the blinking
# events on the spherical surface:
visualization.sphere_mesh3D(position, excitationStrength)
