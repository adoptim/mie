import numpy as np
import math as math

import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite
import Mie_calculation.auxiliary.Mie_physicalConstants as Mie_physicalConstants

import Mie_calculation.Mie_coefficients as Mie_coefficients
import Mie_calculation.Mie_sphericalVectorWaves as Mie_sphericalVectorWaves

import Mie_calculation.auxiliary.pointDistributionOnSphere as \
    pointDistributionOnSphere
import Mie_calculation.auxiliary.coordinateManagement as coordinateManagement
import Mie_calculation.auxiliary.Mie_summationAccessaries as \
    Mie_summationAccessaries
import Mie_calculation.auxiliary.sphericalFunctions as sphericalFunctions

import Mie_calculation.Mie_dipoleEmitter_rates as Mie_dipoleEmitter_rates
import Mie_calculation.Mie_dipoleEmitter_electricField as \
    Mie_dipoleEmitter_electricField


def polarizationDegree_position(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium
):
    # This function calculates the polazitaion degree of a dipole emission near
    # a sphere when the dipole position is fixed but the dipole orientation
    # changes freely and averages within an exposure.

    n_max = 10

    dipolePosition_numberOfPoints = 1
    if dipolePosition.shape[1] > dipolePosition_numberOfPoints:
        print("More dipole position was given than should have been.")

    # number of different dipole orientations:
    dipoleVector_numberOfPoints = electricField_numberOfPoints_4PI
    # dipole vectors, evenly distributed on a sphere, 3xN array:
    dipoleVector = \
        pointDistributionOnSphere.getUnitOrientationVectors_r_theta_phi(
            dipoleVector_numberOfPoints, "even")

    # point coordinate for the elctric field sampling, evenly distributed on a
    # conical sphere, 3xN array:
    positionVector_electricField = \
        pointDistributionOnSphere.\
        getPositionVectorsWithinCone_sphericalCoordinates(
            electricField_numberOfPoints_4PI, samplingDistance,
            collectionAngle, "even")
    electricField_numberOfPoints = positionVector_electricField.shape[1]

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    # spherical vector waves for all n, m, sigma index and for all sampling
    # position:
    (M_3_array, N_3_array) = \
        Mie_sphericalVectorWaves.arraysForDipoleElectricField(
            positionVector_electricField, emissionWavelength,
            refractiveIndex_medium, n_max)

    channelSignal_orientations = np.zeros(
        [3, dipoleVector_numberOfPoints, dipolePosition_numberOfPoints])

    for idxPosition in range(0, dipolePosition_numberOfPoints):

        dipolePosition_act = np.zeros([3, 1])
        dipolePosition_act[:, 0] = dipolePosition[:, idxPosition]

        # expansion coefficients for all n, m, sigma index and for all dipole
        # orientation:
        (s_array, t_array, u_array, v_array) = \
            Mie_coefficients.expansionCoefficients_orientationArray(
                dipoleVector, dipolePosition_act, sphereRadius,
                emissionWavelength, refractiveIndex_sphere,
                refractiveIndex_medium, n_max)

        for idxOrientation in range(0, dipoleVector_numberOfPoints):
            # varying the dipole orientation

            # the summation index dependent expansion coefficients belonging to
            # the given dipole orientation:
            s_sequence_orientation = s_array[idxOrientation, :]
            t_sequence_orientation = t_array[idxOrientation, :]
            u_sequence_orientation = u_array[idxOrientation, :]
            v_sequence_orientation = v_array[idxOrientation, :]

            # electric field distribution on a sampling points within the cone
            # with fixed dipole orientation and dipole position:
            electricField = Mie_dipoleEmitter_electricField.getElectricField(
                    D_sequence,
                    s_sequence_orientation, t_sequence_orientation,
                    u_sequence_orientation, v_sequence_orientation,
                    M_3_array, N_3_array)

            # get the signal (~photon count) of the three polarization
            # components (the "z" component should be negligible) for the given
            # dipole orientation and dipole position:
            channelSignal_orientations[:, idxOrientation, idxPosition] =\
                Mie_dipoleEmitter_electricField.polarizationChannelSignals(
                    positionVector_electricField, electricField,
                    electricField_numberOfPoints, emissionWavelength,
                    refractiveIndex_sphere, refractiveIndex_medium)

        # sum the polarization channel signals along the dipole orientations
        # (2nd dimension):
        channelSignal = np.sum(channelSignal_orientations, axis=1)

        polarizationValues = (
            (channelSignal[0, :] - channelSignal[1, :])
            / (channelSignal[0, :] + channelSignal[1, :])
            )

        print("polarization degree: ", polarizationValues)


def intensityDistribution_rotateCollectionDirection(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    dipoleVector,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium
):
    # This function caclulates the intensity distribution for a dipole
    # with fixed orientation and rotation. The collection direction can be
    # freely changed (it is equivalent to changing the dipole position).

    # angle of the selected sampling orientation (which should be rotated to
    # the "z" axis):
    theta = 0 * np.pi / 180
    phi = 45 * np.pi / 180

    n_max = 10

    dipolePosition_numberOfPoints = 1
    if dipolePosition.shape[1] > dipolePosition_numberOfPoints:
        print("More dipole position was given than should have been.")

    dipoleVector_numberOfPoints = 1
    if dipoleVector.shape[1] > dipoleVector_numberOfPoints:
        print("More dipole vector was given than should have been.")

    # point coordinate for the elctric field sampling, evenly distributed on a
    # conical sphere, 3xN array:
    positionVector_electricField_4PI = \
        pointDistributionOnSphere.getPositionVectors_sphericalCoordinates(
            electricField_numberOfPoints_4PI, samplingDistance, "even")

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    # expansion coefficients for all n, m, sigma index and for all dipole
    # orientation:
    (s_array, t_array, u_array, v_array) = \
        Mie_coefficients.expansionCoefficients_orientationArray(
            dipoleVector, dipolePosition, sphereRadius, emissionWavelength,
            refractiveIndex_sphere, refractiveIndex_medium, n_max)

    # spherical vector waves for all n, m, sigma index and for all sampling
    # position:
    (M_3_array, N_3_array) = \
        Mie_sphericalVectorWaves.arraysForDipoleElectricField(
            positionVector_electricField_4PI, emissionWavelength,
            refractiveIndex_medium, n_max)

    channelSignal_array = np.zeros(
        [3, dipoleVector_numberOfPoints, electricField_numberOfPoints_4PI])

    for idxOrientation in range(0, dipoleVector_numberOfPoints):
        # varying the dipole orientation

        # the summation index dependent expansion coefficients belonging to the
        # given dipole orientation:
        s_sequence_orientation = s_array[idxOrientation, :]
        t_sequence_orientation = t_array[idxOrientation, :]
        u_sequence_orientation = u_array[idxOrientation, :]
        v_sequence_orientation = v_array[idxOrientation, :]

        # electric field distribution on a sphere with fixed dipole orientation
        # and dipole position:
        electricField_4PI = \
            Mie_dipoleEmitter_electricField.getElectricField_4PI(
                D_sequence,
                s_sequence_orientation, t_sequence_orientation,
                u_sequence_orientation, v_sequence_orientation,
                M_3_array, N_3_array)

        # get the samling points that are within the collection angle after the
        # rotation (theta<=collectionAngle):
        rotatedPositionVector_electricField = \
            coordinateManagement.rotateSphericalCoordinates(
                positionVector_electricField_4PI, -theta, -phi)
        boolean = rotatedPositionVector_electricField[1, :] <= collectionAngle

        # the electric field belonging the sampling points within the
        # collection angle:
        electricField_cone = electricField_4PI[boolean, :]
        """electricField_cone = \
            coordinateManagement.rotateY_sphericalComponents(
                electricField_cone, -theta)"""

        positionVector_cone_rotated = \
            rotatedPositionVector_electricField[:, boolean]
        positionVector_cone = positionVector_electricField_4PI[:, boolean]

        electricField_cone_rotated =\
            coordinateManagement.rotateElectricField(
                positionVector_cone, electricField_cone, -theta, -phi,
                positionVector_cone_rotated)

        # get the signal (~photon count) of the three polarization components
        # (the "z" component should be negligible) for the given dipole
        # orientation and for the chosen sampling direction collection angle:
        channelSignal_array = \
            Mie_dipoleEmitter_electricField.polarizationChannelSignals(
                positionVector_cone_rotated, electricField_cone_rotated,
                electricField_numberOfPoints_4PI, emissionWavelength,
                refractiveIndex_sphere, refractiveIndex_medium)

        print_rates_summed(
            s_sequence_orientation, t_sequence_orientation,
            u_sequence_orientation, v_sequence_orientation,
            D_sequence, electricField_4PI, samplingDistance,
            electricField_numberOfPoints_4PI, refractiveIndex_sphere,
            refractiveIndex_medium, emissionWavelength)

        # get the free space radiation rate:
        mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
        k_0, refr_rel, k_medium, k_sphere = \
            Mie_physicalConstants.optical(
                emissionWavelength,
                refractiveIndex_sphere, refractiveIndex_medium)
        W_R0 = \
            Mie_dipoleEmitter_rates.freeSpaceRadiationRate(
                k_medium, c_0, mu_0, dipoleVector)

        power = \
            Mie_dipoleEmitter_electricField.integratedPower(
                electricField_4PI, samplingDistance,
                electricField_numberOfPoints_4PI,
                emissionWavelength,
                refractiveIndex_sphere, refractiveIndex_medium)

        print("emission rate: ", power/W_R0)
        print("summed polarization emission rate: ",
              np.sum(channelSignal_array)/W_R0)
        print("polarization components: ",
              channelSignal_array/np.sum(channelSignal_array))
        print("polarization degree: ",
              (channelSignal_array[0]-channelSignal_array[1])
              / (channelSignal_array[0]+channelSignal_array[1]))

        electricField_visualization = np.zeros(
            electricField_cone.shape, electricField_cone.dtype)
        electricField_visualization[1, :] = electricField_cone_rotated[1, :]

        testing_visualize_scatter3D(
            positionVector_cone_rotated, electricField_visualization)


def intensityDistribution(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    dipoleVector,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium
):
    # This function caclulates the intensity distribution for a dipole
    # with fixed orientation and rotation. The collection direction is fixed
    # alongside the "z" axis.

    n_max = 10

    dipolePosition_numberOfPoints = 1
    if dipolePosition.shape[1] > dipolePosition_numberOfPoints:
        print("More dipole position was given than should have been.")

    dipoleVector_numberOfPoints = 1
    if dipoleVector.shape[1] > dipoleVector_numberOfPoints:
        print("More dipole vector was given than should have been.")

    # point coordinate for the elctric field sampling, evenly distributed on a
    # conical sphere, 3xN array:
    positionVector_electricField = \
        pointDistributionOnSphere.\
        getPositionVectorsWithinCone_sphericalCoordinates(
            electricField_numberOfPoints_4PI,
            samplingDistance, collectionAngle, "even")

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    # spherical vector waves for all n, m, sigma index and for all sampling
    # position:
    (M_3_array, N_3_array) = \
        Mie_sphericalVectorWaves.arraysForDipoleElectricField(
            positionVector_electricField, emissionWavelength,
            refractiveIndex_medium, n_max)

    channelSignal_array = np.zeros(
        [3, dipoleVector_numberOfPoints, dipolePosition_numberOfPoints])

    for idxPosition in range(0, dipolePosition_numberOfPoints):

        dipolePosition_act = np.zeros([3, 1])
        dipolePosition_act[:, 0] = dipolePosition[:, idxPosition]

        # expansion coefficients for all n, m, sigma index and for all dipole
        # orientation:
        (s_array, t_array, u_array, v_array) = \
            Mie_coefficients.expansionCoefficients_orientationArray(
                dipoleVector, dipolePosition_act, sphereRadius,
                emissionWavelength, refractiveIndex_sphere,
                refractiveIndex_medium, n_max)

        for idxOrientation in range(0, dipoleVector_numberOfPoints):
            # varying the dipole orientation

            # the summation index dependent expansion coefficients belonging to
            # the given dipole orientation:
            s_sequence_orientation = s_array[idxOrientation, :]
            t_sequence_orientation = t_array[idxOrientation, :]
            u_sequence_orientation = u_array[idxOrientation, :]
            v_sequence_orientation = v_array[idxOrientation, :]

            # electric field distribution on a sampling points within the cone
            # with fixed dipole orientation and dipole position:
            electricField = \
                Mie_dipoleEmitter_electricField.getElectricField(
                    D_sequence,
                    s_sequence_orientation, t_sequence_orientation,
                    u_sequence_orientation, v_sequence_orientation,
                    M_3_array, N_3_array)

            # get the signal (~photon count) of the three polarization
            # components (the "z" component should be negligible) for the
            # given dipole orientation
            # and dipole position:
            channelSignal_array =\
                Mie_dipoleEmitter_electricField.polarizationChannelSignals(
                    positionVector_electricField, electricField,
                    electricField_numberOfPoints_4PI, emissionWavelength,
                    refractiveIndex_sphere, refractiveIndex_medium)

            mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
            k_0, refr_rel, k_medium, k_sphere = \
                Mie_physicalConstants.optical(
                    emissionWavelength, refractiveIndex_sphere,
                    refractiveIndex_medium)
            W_R0 = \
                Mie_dipoleEmitter_rates.freeSpaceRadiationRate(
                    k_medium, c_0, mu_0, dipoleVector)

            power = \
                Mie_dipoleEmitter_electricField.integratedPower(
                    electricField, samplingDistance,
                    electricField_numberOfPoints_4PI, emissionWavelength,
                    refractiveIndex_sphere, refractiveIndex_medium)

            print("emission rate: ", power/W_R0)
            print("summed polarization emission rate: ",
                  np.sum(channelSignal_array)/W_R0)
            print("polarization components: ",
                  channelSignal_array/np.sum(channelSignal_array))
            print("polarization degree: ",
                  (channelSignal_array[0]-channelSignal_array[1])
                  / (channelSignal_array[0]+channelSignal_array[1]))

            testing_visualize_scatter3D(
                positionVector_electricField, electricField)


def intensityDistribution_singleExpansionIndex(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    dipoleVector,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    refractiveIndex_sphere,
    refractiveIndex_medium
):
    # This function caclulates the intensity distribution contribution of a
    # single Mie expansion term for a dipole with fixed orientation and
    # rotation.  It is useful for testing or investigating the higher order
    # terms.

    n = 1
    m = 0
    sigma = 2

    n_max = 1

    # point coordinate for the elctric field sampling, evenly distributed on a
    # full sphere, 3xN array:
    positionVector_electricField = \
        pointDistributionOnSphere.getPositionVectors_sphericalCoordinates(
            electricField_numberOfPoints_4PI, samplingDistance, "even")

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    # expansion coefficients for all n, m, sigma index and for all dipole
    # orientation:
    (s_array, t_array, u_array, v_array) = \
        Mie_coefficients.expansionCoefficients_orientationArray(
            dipoleVector, dipolePosition,
            sphereRadius, emissionWavelength, refractiveIndex_sphere,
            refractiveIndex_medium, n_max)

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = \
        Mie_physicalConstants.optical(
            emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)

    r = positionVector_electricField[0, 0]
    kr = k_medium * positionVector_electricField[0, :]
    theta = positionVector_electricField[1, :]
    phi = positionVector_electricField[2, :]

    j_array, j_der_array = \
        sphericalFunctions.sphericalBessel_firstOrder(n_max, kr)
    h_array, h_der_array = \
        sphericalFunctions.sphericalHankel_firstOrder(
            n_max, kr, j_array, j_der_array)
    P_array, P_m_per_sinTheta_sequence, P_der_array = \
        sphericalFunctions.associatedLegendrePolynomials(n_max, theta)

    linearIndex_n = Mie_summationAccessaries.indexMapping_n(n)
    j_n = j_array[:, linearIndex_n]
    j_n_der = j_der_array[:, linearIndex_n]
    h_n = h_array[:, linearIndex_n]
    h_n_der = h_der_array[:, linearIndex_n]

    linearIndex_n_m = Mie_summationAccessaries.indexMapping_n_m(n, m)
    P_m_n = P_array[:, linearIndex_n_m]
    P_m_per_sinTheta_m_n = P_m_per_sinTheta_sequence[:, linearIndex_n_m]
    P_m_n_der = P_der_array[:, linearIndex_n_m]

    linearIndex_n_m_sigma = \
        Mie_summationAccessaries.indexMapping_n_m_sigma(n, m, sigma)
    s_sigma_m_n = s_array[:, linearIndex_n_m_sigma]
    t_sigma_m_n = t_array[:, linearIndex_n_m_sigma]
    D_sigma_m_n = D_sequence[linearIndex_n_m_sigma]

    M_3_field = \
        Mie_sphericalVectorWaves.M_3(
            n, m, sigma, theta, phi, h_n,
            P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)
    N_3_field = \
        Mie_sphericalVectorWaves.N_3(
            n, m, sigma, kr, theta, phi, h_n, h_n_der,
            P_m_n, P_m_per_sinTheta_m_n, P_m_n_der)

    M_3_rateInput = np.zeros(N_3_field.shape, dtype=np.cdouble)
    M_3_rateInput = M_3_field
    N_3_rateInput = np.zeros(N_3_field.shape, dtype=np.cdouble)
    N_3_rateInput = N_3_field

    print_rates_single(
        n, m, sigma, s_sigma_m_n, t_sigma_m_n, 0, 0,
        D_sigma_m_n, M_3_rateInput, N_3_rateInput, r,
        electricField_numberOfPoints_4PI, refractiveIndex_sphere,
        refractiveIndex_medium, emissionWavelength)

    vectorWaveField = np.zeros(N_3_field.shape, dtype=np.cdouble)
    vectorWaveField = N_3_rateInput

    testing_visualize_scatter3D(positionVector_electricField, vectorWaveField)


def print_rates_summed(
    s_sequence, t_sequence,
    u_sequence, v_sequence,
    D_sequence,
    electricField_4PI,
    samplingDistance, numberOfPoints,
    refractiveIndex_sphere, refractiveIndex_medium,
    emissionWavelength
):

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = \
        Mie_physicalConstants.optical(
            emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    sigma_conductivity = \
        Mie_physicalConstants.conductivity(
            c_0, k_0, epsilon_0, refractiveIndex_sphere)

    # dipole emission power in free space:
    W_R0 = (
        (k_medium * c_0) ** 4 / 12 / np.pi * mu_0 / c_0
        * 1)

    power_coefficients = (
        np.sqrt(epsilon_0 / mu_0) * np.pi / k_medium ** 2
    ) * 1 / 2 * np.sum(
        D_sequence*np.abs((s_sequence + u_sequence)) ** 2
        + D_sequence*np.abs((t_sequence + v_sequence)) ** 2)

    power_integrated = \
        Mie_dipoleEmitter_electricField.integratedPower(
            electricField_4PI, samplingDistance, numberOfPoints,
            emissionWavelength, refractiveIndex_sphere,
            refractiveIndex_medium)

    print("rate, coefficients", power_coefficients/W_R0)

    print("rate, integrated", power_integrated/W_R0)


def print_rates_single(
    n, m, sigma,
    s_sigma_m_n, t_sigma_m_n,
    u_sigma_m_n, v_sigma_m_n,
    D_sigma_m_n,
    M_3, N_3,
    samplingDistance, numberOfPoints,
    refractiveIndex_sphere, refractiveIndex_medium,
    emissionWavelength
):

    mu_0, epsilon_0, c_0 = Mie_physicalConstants.electromagnetic()
    k_0, refr_rel, k_medium, k_sphere = \
        Mie_physicalConstants.optical(
            emissionWavelength, refractiveIndex_sphere, refractiveIndex_medium)
    sigma_conductivity = \
        Mie_physicalConstants.conductivity(
            c_0, k_0, epsilon_0, refractiveIndex_sphere)

    # dipole emission power in free space:
    W_R0 = ( (k_medium * c_0) ** 4 / 12 / np.pi * mu_0 / c_0 * 1)

    power_coefficients = (
        np.sqrt(epsilon_0 / mu_0) * np.pi / k_medium ** 2
    ) * 1 / 2 * np.sum(
        D_sigma_m_n*np.abs((s_sigma_m_n + u_sigma_m_n)) ** 2
        + D_sigma_m_n*np.abs((t_sigma_m_n + v_sigma_m_n)) ** 2)

    electricField_4PI = D_sigma_m_n*(s_sigma_m_n*M_3 + t_sigma_m_n*N_3)

    power_integrated = \
        Mie_dipoleEmitter_electricField.integratedPower(
            electricField_4PI, samplingDistance, numberOfPoints,
            emissionWavelength, refractiveIndex_sphere,
            refractiveIndex_medium)

    print(n, m, sigma, "rate, coefficients", power_coefficients/W_R0)
    print(n, m, sigma, "rate, integrated", power_integrated/W_R0)

    print(n, m, sigma, "M_3 2", np.sum(M_3 * np.conjugate(M_3)))
    print(n, m, sigma, "N_3 2", np.sum(N_3 * np.conjugate(N_3)))


def testing_visualize_scatter3D(samplingPositions_spherical, electricField):

    # https://matplotlib.org/stable/gallery/mplot3d/scatter3d.html

    intensity = \
        np.real(np.sum(electricField*np.conjugate(electricField), axis=1))

    r = samplingPositions_spherical[0, :]
    theta = samplingPositions_spherical[1, :]
    phi = samplingPositions_spherical[2, :]

    # mathematician convention:
    """x, y, z = (
        r * np.cos(theta) * np.sin(phi),
        r * np.sin(theta) * np.sin(phi),
        r * np.cos(phi)
        )"""
    # physicist convention:
    x, y, z = (
        r * np.cos(phi) * np.sin(theta),
        r * np.sin(phi) * np.sin(theta),
        r * np.cos(theta))

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    s = ax.scatter(x, y, z, cmap='hot', c=intensity)

    fig.colorbar(s, ax=ax)

    plt.show()


emissionWavelength = 670 * 10 ** -9

sphereRadius = 1 / 2 * 300 * 10 ** -9

dipoleDistance = 10 * 18 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.30
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(emissionWavelength),
    refractiveIndex.silver(emissionWavelength),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
# refractiveIndex_medium = np.ones(refractiveIndex_medium.size)
# refractiveIndex_sphere = refractiveIndex_medium

# position of the dipole
# the radial coordinate of the dipole position ( initializing here as 0 as it
# should be set in the loop over the sweep parameters:
dipolePosition_r = sphereRadius + dipoleDistance
# dipolePosition_r = 18*10**-9
# the polar angle of the dipole position:
dipolePosition_theta = 0. * np.pi/180
# the azimuthal angle of the dipole position:
dipolePosition_fi = 0. * np.pi/180
# the position of the dipole emitter in spherical coordiante system centered at
# the sphere center:
dipolePosition = np.array([
    [dipolePosition_r],
    [dipolePosition_theta],
    [dipolePosition_fi]])

# number of different dipole orientations:
dipoleVector_numberOfPoints = 1

# strength and orientation of the dipole (components of the dipole vector in
# the spherical coordiante system at the dipole position, the "r", "theta"
# and "fi" unit vectors, respectively):
dipoleStrength = 1.0
dipoleVector_theta = 0. * np.pi / 180
dipoleVector_fi = 0. * np.pi / 180
dipoleVector = dipoleStrength * np.array(
    [
        [np.cos(dipoleVector_theta)],
        [np.sin(dipoleVector_theta) * np.cos(dipoleVector_fi)],
        [np.sin(dipoleVector_theta) * np.sin(dipoleVector_fi)],
    ]
)
# alternatively:
# perpendicular to the nanosphere surface:
# dipoleVector = np.array([[1.0], [0.0], [0.0]])
# parallel to the nanosphere surface:
# dipoleVector=np.array([[0.], [1.], [0.]])
# dipoleVector=np.array([[0.], [0.], [1.]])

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 1000

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)


polarizationDegree_position(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium
)

"""intensityDistribution_rotateCollectionDirection(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    dipoleVector,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium
)"""

"""intensityDistribution(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    dipoleVector,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium
)"""

"""intensityDistribution_singleExpansionIndex(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    dipoleVector,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    refractiveIndex_sphere,
    refractiveIndex_medium
)"""
