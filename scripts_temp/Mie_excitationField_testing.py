import numpy as np

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite

import Mie_calculation.Mie_electricField as \
    Mie_electricField

import Mie_calculation.auxiliary.pointDistributionOnSphere as \
    pointDistributionOnSphere
import Mie_calculation.auxiliary.coordinateManagement as coordinateManagement

import Mie_calculation.auxiliary.visualization as visualization

def planeWave_direction_Z_polarization_X(
    emissionWavelength,
    sphereRadius,
    refractiveIndex_sphere,
    refractiveIndex_medium
        ):

    # distace of the sampling points from the center
    samplingDistance = sphereRadius + 10*1E-9
    
    electricField_numberOfPoints_4PI = 1000
    
    # point coordinate for the elctric field sampling, evenly distributed on a
    # sphere, 3xN array:
    positionVector_electricField = \
        pointDistributionOnSphere.\
        getPositionVectors_sphericalCoordinates(
            electricField_numberOfPoints_4PI, samplingDistance, "even")

    # TODO: test it!!!
    k_medium = refractiveIndex_medium * 2 * np.pi / emissionWavelength
    refractiveIndex_relative = refractiveIndex_sphere / refractiveIndex_medium
    excitationField =  Mie_electricField.planeWave_direction_Z_polarization_X(sphereRadius, k_medium, refractiveIndex_relative, positionVector_electricField)
    
    excitationField_Descartes = coordinateManagement.sphericalComponentsToDescartes(positionVector_electricField, excitationField)
    
    # "r" component
    # valueToVisualize = np.abs(excitationField[:, 0])
    # "theta" component
    # valueToVisualize = np.abs(excitationField[:, 1])
    # "fi" component
    # valueToVisualize = np.abs(excitationField[:, 2])
    
    # "x" component:
    valueToVisualize = np.abs(excitationField_Descartes[:, 0])
    # "y" component:
    # valueToVisualize = np.real(excitationField_Descartes[:, 1])
    # "z" component:
    # valueToVisualize = np.real(excitationField_Descartes[:, 2])
    # valueToVisualize = np.abs(excitationField_Descartes[:, 2])

    # intensity:
    valueToVisualize = np.abs(excitationField_Descartes[:, 0])**2 + np.abs(excitationField_Descartes[:, 1])**2 + np.abs(excitationField_Descartes[:, 2])**2
    
    return positionVector_electricField, valueToVisualize


def planeWave_direction_X_polarization_Z(
    emissionWavelength,
    sphereRadius,
    refractiveIndex_sphere,
    refractiveIndex_medium
        ):

    # distace of the sampling points from the center
    samplingDistance = sphereRadius + 18*1E-9
    
    electricField_numberOfPoints_4PI = 1000
    
    # point coordinate for the elctric field sampling, evenly distributed on a
    # sphere, 3xN array:
    positionVector_electricField = \
        pointDistributionOnSphere.\
        getPositionVectors_sphericalCoordinates(
            electricField_numberOfPoints_4PI, samplingDistance, "even")

    # TODO: test it!!!
    k_medium = refractiveIndex_medium * 2 * np.pi / emissionWavelength
    refractiveIndex_relative = refractiveIndex_sphere / refractiveIndex_medium
    excitationField =  Mie_electricField.planeWave_direction_X_polarization_Z(positionVector_electricField, sphereRadius, k_medium, refractiveIndex_relative)
    
    excitationField_Descartes = coordinateManagement.sphericalComponentsToDescartes(positionVector_electricField, excitationField)
    
    # "r" component
    # valueToVisualize = np.abs(excitationField[:, 0])
    # "theta" component
    # valueToVisualize = np.abs(excitationField[:, 1])
    # "fi" component
    # valueToVisualize = np.abs(excitationField[:, 2])
    
    # perpendicular intensity:
    # valueToVisualize = np.abs(excitationField[:, 0])**2
    
    # parallel intensity:
    #valueToVisualize = np.abs(excitationField[:, 1])**2 + np.abs(excitationField[:, 2])**2
    
    # ratio of the perpendicular and parallel:
    #valueToVisualize = np.abs(excitationField[:, 0])**2 / ( np.abs(excitationField[:, 0])**2 + np.abs(excitationField[:, 1])**2 + np.abs(excitationField[:, 2])**2)
    
    # "x" component:
    # valueToVisualize = np.abs(excitationField_Descartes[:, 0])
    # "y" component:
    # valueToVisualize = np.real(excitationField_Descartes[:, 1])
    # "z" component:
    # valueToVisualize = np.real(excitationField_Descartes[:, 2])
    # valueToVisualize = np.abs(excitationField_Descartes[:, 2])

    # intensity:
    valueToVisualize = np.abs(excitationField_Descartes[:, 0])**2 + np.abs(excitationField_Descartes[:, 1])**2 + np.abs(excitationField_Descartes[:, 2])**2
    
    return positionVector_electricField, valueToVisualize


emissionWavelength = 520 * 10 ** -9

sphereRadius = 1 / 2 * 100 * 10 ** -9


# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 1.0
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(emissionWavelength),
    refractiveIndex.silver(emissionWavelength),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
#refractiveIndex_medium = np.ones(refractiveIndex_sphere.shape)
#refractiveIndex_sphere = refractiveIndex_medium

"""positionVector_electricField, valueToVisualize = planeWave_direction_Z_polarization_X(
    emissionWavelength,
    sphereRadius,
    refractiveIndex_sphere,
    refractiveIndex_medium
    )"""

positionVector_electricField, valueToVisualize = planeWave_direction_X_polarization_Z(
    emissionWavelength,
    sphereRadius,
    refractiveIndex_sphere,
    refractiveIndex_medium
    )


visualization.sphere_mesh3D(positionVector_electricField, valueToVisualize)