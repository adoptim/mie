import numpy as np
import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as n
from opticalConstants.refractiveIndex_composite import refractiveIndex_composite
from Mie_calculation.Mie_planeWave_crossSections import Mie_planeWave_crossSections

# constants

N=41

lambda_0=np.linspace(550, 550, num=N)*10**-9 			# m

r=np.linspace(60, 200, num=N)*10**-9/2


# The "x" quantity in case of wavelength sweep:
#x=lambda_0*10**9				# plot as the function of wavelength in nm
#xlabel='wavelength [nm]'
# The "x" quantity in case of particle radius sweep:
#x=r*10**9
#xlabel='particle radius'
# The "x" quantity in case of particle diameter sweep:
x=2*r*10**9
xlabel='particle diameter'
# The "x" quantity in case of particle radius sweep, dimensionless:
#x=2*np.pi*r/lambda_0			# plot as the function of the dimensionless diffraction parameter
#xlabel='dimensionless diffraction parameter'

compositionRatio=0.3
n_sphere=refractiveIndex_composite(n.gold(lambda_0), n.silver(lambda_0), compositionRatio)
n_medium=np.real(n.water(lambda_0))


#n_sphere=n.gold(lambda_0)
#n_medium=1*np.ones(lambda_0.shape)

Absorption=np.zeros(lambda_0.shape)
Scattering=np.zeros(lambda_0.shape)
Extinction=np.zeros(lambda_0.shape)


for idx in range(0,N):
	[sigma_a, sigma_s, sigma_e]=Mie_planeWave_crossSections(lambda_0[idx], r[idx], n_sphere[idx], n_medium[idx])
	
	Absorption[idx]=sigma_a
	Scattering[idx]=sigma_s
	Extinction[idx]=sigma_e
	

plt.plot(x, Absorption*1e18, x, Scattering*1e18, x, Extinction*1e18)
plt.legend(('Absorption', 'Scattering', 'Extinction'))
plt.xlabel(xlabel)
plt.ylabel('cross section [nm^2]')
plt.show()


