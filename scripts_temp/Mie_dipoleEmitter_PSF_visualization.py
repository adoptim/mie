import numpy as np
import math as math


import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite

import Mie_calculation.Mie_coefficients as Mie_coefficients
import Mie_calculation.Mie_sphericalVectorWaves as Mie_sphericalVectorWaves

import Mie_calculation.auxiliary.pointDistributionOnSphere as \
    pointDistributionOnSphere
import Mie_calculation.auxiliary.Mie_summationAccessaries as \
    Mie_summationAccessaries
import Mie_calculation.Mie_dipoleEmitter_electricField as \
    Mie_dipoleEmitter_electricField

import Mie_calculation.Mie_dipoleEmitter_PSF as Mie_dipoleEmitter_PSF


def polarizationDegree_position(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium
):
    # This function calculates the polazitaion degree of a dipole emission near
    # a sphere when the dipole position is fixed but the dipole orientation
    # changes freely and averages within an exposure.

    PSF_size = 32

    n_max = 10

    # number of different dipole orientations:
    dipoleVector_numberOfPoints = 100

    dipolePosition_numberOfPoints = 1
    if dipolePosition.shape[1] > dipolePosition_numberOfPoints:
        print("More dipole position was given than should have been.")

    # dipole vectors, evenly distributed on a sphere, 3xN array:
    dipoleVector = \
        pointDistributionOnSphere.getUnitOrientationVectors_r_theta_phi(
            dipoleVector_numberOfPoints, "even")

    # point coordinate for the elctric field sampling, evenly distributed on a
    # conical sphere, 3xN array:
    positionVector_electricField = \
        pointDistributionOnSphere.\
        getPositionVectorsWithinCone_sphericalCoordinates(
            electricField_numberOfPoints_4PI, samplingDistance,
            collectionAngle, "even")

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    # spherical vector waves for all n, m, sigma index and for all sampling
    # position:
    (M_3_array, N_3_array) = \
        Mie_sphericalVectorWaves.arraysForDipoleElectricField(
            positionVector_electricField, emissionWavelength,
            refractiveIndex_medium, n_max)

    polarizationPSF_x_orientations = np.zeros(
        [PSF_size, PSF_size, dipoleVector_numberOfPoints])
    polarizationPSF_y_orientations = np.zeros(
        [PSF_size, PSF_size, dipoleVector_numberOfPoints])

    for idxPosition in range(0, dipolePosition_numberOfPoints):

        dipolePosition_act = np.zeros([3, 1])
        dipolePosition_act[:, 0] = dipolePosition[:, idxPosition]

        # expansion coefficients for all n, m, sigma index and for all dipole
        # orientation:
        (s_array, t_array, u_array, v_array) = \
            Mie_coefficients.expansionCoefficients_orientationArray(
                dipoleVector, dipolePosition_act, sphereRadius,
                emissionWavelength, refractiveIndex_sphere,
                refractiveIndex_medium, n_max)

        for idxOrientation in range(0, dipoleVector_numberOfPoints):
            # varying the dipole orientation

            # the summation index dependent expansion coefficients belonging to
            # the given dipole orientation:
            s_sequence_orientation = s_array[idxOrientation, :]
            t_sequence_orientation = t_array[idxOrientation, :]
            u_sequence_orientation = u_array[idxOrientation, :]
            v_sequence_orientation = v_array[idxOrientation, :]

            # electric field distribution on a sampling points within the cone
            # with fixed dipole orientation and dipole position as if a dipole
            # source with unit dipole stregth would radiate it:
            electricField = \
                Mie_dipoleEmitter_electricField.getElectricField(
                    D_sequence,
                    s_sequence_orientation, t_sequence_orientation,
                    u_sequence_orientation, v_sequence_orientation,
                    M_3_array, N_3_array)

            # get the signal (~photon count) of the three polarization
            # components (the "z" component should be negligible) for the given
            # dipole orientation and dipole position:

            (polarizationPSF_x_orientations[:, :, idxOrientation],
            polarizationPSF_y_orientations[:, :, idxOrientation]) =\
                Mie_dipoleEmitter_PSF.polarizedPSF(
                    positionVector_electricField, electricField,
                    samplingDistance, collectionAngle, PSF_size)


        # sum the polarization channel signals along the dipole orientations
        # (2nd dimension):
        polarizationPSF_x = np.sum(polarizationPSF_x_orientations, axis=2)
        polarizationPSF_y = np.sum(polarizationPSF_y_orientations, axis=2)
        
        sumSignal = np.sum(polarizationPSF_x) + np.sum(polarizationPSF_y)
        print(sumSignal)

    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(10,5))
    fig.suptitle('SphereDiameter: {:.0f} nm, Wavelength: {:.0f} nm'.format(sphereRadius*2E9, emissionWavelength*1E9))
    ax1.pcolormesh(polarizationPSF_x)
    ax2.pcolormesh(polarizationPSF_y)
    plt.show()


emissionWavelength = 670 * 10 ** -9


sphereRadius = 1 / 2 * 300 * 10 ** -9

dipoleDistance = 18 * 10 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(emissionWavelength),
    refractiveIndex.silver(emissionWavelength),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
# refractiveIndex_medium = np.ones(refractiveIndex_medium.size)
# refractiveIndex_sphere = refractiveIndex_medium

# position of the dipole
# the radial coordinate of the dipole position ( initializing here as 0 as it
# should be set in the loop over the sweep parameters:
dipolePosition_r = sphereRadius + dipoleDistance
# dipolePosition_r = 18*10**-9
# the polar angle of the dipole position:
dipolePosition_theta = 00. * np.pi/180
# the azimuthal angle of the dipole position:
dipolePosition_fi = 90. * np.pi/180
# the position of the dipole emitter in spherical coordiante system centered at
# the sphere center:
dipolePosition = np.array([
    [dipolePosition_r],
    [dipolePosition_theta],
    [dipolePosition_fi]])

# number of different dipole orientations:
# dipoleVector_numberOfPoints = 1

# strength and orientation of the dipole (components of the dipole vector in
# the spherical coordiante system at the dipole position, the "r", "theta"
# and "fi" unit vectors, respectively):
dipoleStrength = 1.0
dipoleVector_theta = 0. * np.pi / 180
dipoleVector_fi = 0. * np.pi / 180
dipoleVector = dipoleStrength * np.array(
    [
        [np.cos(dipoleVector_theta)],
        [np.sin(dipoleVector_theta) * np.cos(dipoleVector_fi)],
        [np.sin(dipoleVector_theta) * np.sin(dipoleVector_fi)],
    ]
)
# alternatively:
# perpendicular to the nanosphere surface:
# dipoleVector = np.array([[1.0], [0.0], [0.0]])
# parallel to the nanosphere surface:
# dipoleVector=np.array([[0.], [1.], [0.]])
# dipoleVector=np.array([[0.], [0.], [1.]])

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 1000

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)


polarizationDegree_position(
    emissionWavelength,
    sphereRadius,
    dipolePosition,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium
)
