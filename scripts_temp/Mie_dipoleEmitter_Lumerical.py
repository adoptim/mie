import numpy as np
import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite
from Mie_calculation.Mie_dipoleEmitter_rates import Mie_dipoleEmitter_rates
import Mie_calculation.Mie_dipoleEmitter_rates_testing \
    as Mie_dipoleEmitter_rates_testing


# SWEEP PARAMETERS

n_max = 10;

N = 100

emissionWavelength = np.linspace(410, 690, num=N) * 10 ** -9  # m

sphereRadius = 1 / 2 * np.linspace(40, 40, num=N) * 10 ** -9


dipoleDistance = np.linspace(15, 15, num=N) * 10 ** -9
#dz = 5 * 10 ** -9
#fig_size = np.array([760, 435])

dz = np.linspace(-1, 1, 3) * 5 * 10 ** -9
fig_size = np.array([760, 400])


# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
refractiveIndex_sphere = refractiveIndex.gold(emissionWavelength)
#refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
refractiveIndex_medium = np.real(refractiveIndex.vacuum(emissionWavelength))

# position of the dipole
# the radial coordinate of the dipole position ( initializing here as 0 as it
# should be set in the loop over the sweep parameters:
dipolePosition_r = 0.
# the polar angle of the dipole position:
dipolePosition_theta = 56. * np.pi/180
# the azimuthal angle of the dipole position:
dipolePosition_fi = 67. * np.pi/180
# the position of the dipole emitter in spherical coordiante system centered at the sphere center:
dipolePosition_sphericalComponents = np.array([
    [dipolePosition_r],
    [dipolePosition_theta],
    [dipolePosition_fi]])

# strength and orientation of the dipole (components of the dipole vector in
# the spherical coordiante system at the dipole position, the "r", "theta"
# and "fi" unit vectors, respectively):
dipoleStrength = 1.0
dipoleVector_theta = 0. * np.pi /180
dipoleVector_fi = 0. * np.pi /180
dipoleVector = dipoleStrength * np.array(
    [
        [np.cos(dipoleVector_theta)],
        [np.sin(dipoleVector_theta) * np.cos(dipoleVector_fi)],
        [np.sin(dipoleVector_theta) * np.sin(dipoleVector_fi)],
    ]
)

# alternatively:
# perpendicular to the nanosphere surface:
# dipoleVector = np.array([[1.0], [0.0], [0.0]])
# parallel to the nanosphere surface:
# dipoleVector=np.array([[0.], [1.], [0.]])
# dipoleVector=np.array([[0.], [0.], [1.]])

# SWEEP OVER THE PARAMETERS

absorptionRate = np.zeros([3, N])
scatteringRate = np.zeros([3, N])
totalRate = np.zeros([3, N])

for idx_dz in range(0, 3):

    for idx in range(0, N):

        # update the diple radial position coordinate:
        dipolePosition_r = sphereRadius[idx] + dipoleDistance[idx] + dz[idx_dz]
        dipolePosition_sphericalComponents[0] = dipolePosition_r

        # calculate the modified dipole absorption and scattering rates:
        [W_a, W_s, W_t] = Mie_dipoleEmitter_rates(
        emissionWavelength[idx],
        sphereRadius[idx],
        dipolePosition_sphericalComponents,
        dipoleVector,
        refractiveIndex_sphere[idx],
        refractiveIndex_medium[idx],
        n_max
        )

        # for testing purposes, perpendicular dipole orientation to the nanosphere
        # surface:
        """[W_a, W_s, W_t] = Mie_dipoleEmitter_rates_testing.perpendicular(
            emissionWavelength[idx],
            sphereRadius[idx],
            dipolePosition_sphericalComponents,
            dipoleVector,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
        )"""

        # for testing purposes, parallel dipole orientation to the nanosphere
        # surface:
        """[W_a, W_s, W_t] = Mie_dipoleEmitter_rates_testing.parallel(
            emissionWavelength[idx],
            sphereRadius[idx],
            dipolePosition_sphericalComponents,
            dipoleVector,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
        )"""

        """[W_a, W_s, W_t] = Mie_dipoleEmitter_rates_testing.vectorized(
            emissionWavelength[idx],
            sphereRadius[idx],
            dipolePosition_sphericalComponents,
            dipoleVector,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
        )"""

        absorptionRate[idx_dz, idx] = W_a
        scatteringRate[idx_dz, idx] = W_s
        totalRate[idx_dz, idx] = W_t


# VISUALIZATION

# The "x" quantity in case of wavelength sweep:
x = emissionWavelength*10**9				# plot as the function of wavelength in nm
xlabel = 'wavelength [nm]'

# The "x" quantity in case of particle radius sweep:

# x = sphereRadius*10**9
# xlabel = 'particle radius'

# The "x" quantity in case of particle diameter sweep:
# x = 2 * sphereRadius * 10 ** 9
# xlabel = "particle diameter"
# The "x" quantity in case of particle radius sweep, dimensionless:
# x = 2*np.pi*sphereRadius/emissionWavelength			# plot as the function of the
# dimensionless diffraction parameter
# xlabel = 'dimensionless diffraction parameter'

# The "x" quantity in case of dipole distance sweep:
# x = dipoleDistance*10**9				# plot as the function of wavelength in nm
# xlabel = 'dipole distance [nm]'


fig = plt.figure()
DPI = fig.get_dpi()
fig.set_size_inches(fig_size[0]/float(DPI), fig_size[1]/float(DPI))

plt.plot(x, absorptionRate[0, :], label='dz={} nm'.format(dz[0] * 1e9))
plt.plot(x, absorptionRate[1, :], label='dz={} nm'.format(dz[1] * 1e9))
plt.plot(x, absorptionRate[2, :], label='dz={} nm'.format(dz[2] * 1e9))
plt.legend()
plt.title(('Absorption rate'))
plt.xlabel(xlabel)
plt.ylabel("Rate")
plt.ylim(0, 90)
plt.grid()
plt.show()


fig = plt.figure()
DPI = fig.get_dpi()
fig.set_size_inches(fig_size[0]/float(DPI), fig_size[1]/float(DPI))

plt.plot(x, scatteringRate[0, :], label='dz={} nm'.format(dz[0] * 1e9))
plt.plot(x, scatteringRate[1, :], label='dz={} nm'.format(dz[1] * 1e9))
plt.plot(x, scatteringRate[2, :], label='dz={} nm'.format(dz[2] * 1e9))
plt.legend()
plt.title(('Radiative rate'))
plt.xlabel(xlabel)
plt.ylabel("Rate")
plt.ylim(1, 4)
plt.grid()
plt.show()


QE = scatteringRate / (scatteringRate + absorptionRate)

fig = plt.figure()
DPI = fig.get_dpi()
fig.set_size_inches(fig_size[0]/float(DPI), fig_size[1]/float(DPI))

plt.plot(x, QE[0, :], x, QE[1, :], x, QE[2, :])
plt.title(('Quantum efficiency'))
plt.xlabel(xlabel)
plt.ylabel("Rate")
plt.ylim(0, 0.9)
plt.grid()
plt.show()
