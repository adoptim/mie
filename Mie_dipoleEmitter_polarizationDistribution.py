import numpy as np
import math as math

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

import matplotlib.pyplot as plt
import os
from tkinter import filedialog

workingDirectory = filedialog.askdirectory()
simulationName = "Atto647N_80nm"


fluorophore = {}
#fluorophore['excitationWavelength'] = 647 * 10 ** -9
#fluorophore['emissionWavelength'] = 670 * 10 ** -9
#fluorophore['quantumYield'] = 0.65
fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92

n_max = 10

sphereRadius = 1 / 2 * 80 * 10 ** -9


dipoleDistance = 18 * 10 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
# refractiveIndex_medium = np.ones((refractiveIndex_medium.size))
# refractiveIndex_sphere = refractiveIndex_medium

# number of different dipole positions:
dipolePosition_numberOfPoints = 12345

# number of different dipole orientations:
dipoleVector_numberOfPoints = 200

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)
# collectionAngle = np.pi


dipolePositionObject = Mie_dipoleVectors.dipolePosition.evenDistribution_4pi(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
# testing:
#dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.perpendicular(1)

# slow rotation:
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])


# fast rotation:
#excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_fastRotation()
#emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_fastRotation(fluorophore['quantumYield'])


# fast rotation:
#excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
#emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()

#farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree_orientation()
#farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationChannelSignals()
#farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizedPSF(32)
intensityFilteringRatio = 0.0
farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)
#farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.sumSignal()

signalNormalizationBoolean = True

farfieldQuantity, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)


"""filteringValues = np.linspace(0.0, 0.25, 11)
for intensitiyFilteringRatio in filteringValues:

    filteringBoolean = np.full(farfieldQuantity.shape[0], True, dtype=bool)
    if intensitiyFilteringRatio != 0:
        boolean_channelX = farfieldQuantity[:, 0] >= intensitiyFilteringRatio
        boolean_channelY = farfieldQuantity[:, 1] >= intensitiyFilteringRatio
        
        filteringBoolean = boolean_channelX & boolean_channelY
        
    polarizationChannelSignals = farfieldQuantity[filteringBoolean, :]
    
    # polarization degre calculation:
    polarizationValues = (
        (polarizationChannelSignals[:, 0] - polarizationChannelSignals[:, 1])
        / (polarizationChannelSignals[:, 0] + polarizationChannelSignals[:, 1]))

    histogramFigure = visualization.polarizationDegreeHistogram(polarizationValues, sphereRadius, fluorophore['emissionWavelength'])
    
    histogramFigure.savefig("diameter_{:.0f}nm_emission_{:.0f}nm_slowRotation_filtering_{:.3f}.png".format(sphereRadius*1E9 * 2, fluorophore['emissionWavelength'] * 1E9, intensitiyFilteringRatio))
"""

histogramFigure = visualization.polarizationDegreeHistogram(farfieldQuantity, sphereRadius, fluorophore['emissionWavelength'])
#histogramFigure = visualization.brightnessHistogram(farfieldQuantity, sphereRadius, fluorophore['emissionWavelength'])

#polarized_PSF = farfieldQuantity[0]
#psfFigure = visualization.polarizedPFS(polarized_PSF, sphereRadius, fluorophore['emissionWavelength'])

# visualizing the polarization or intensity distribution of the blinking
# events on the spherical surface:
#meshFigure = visualization.sphere_mesh3D(dipolePosition, farfieldQuantity)

"""
polDegree_vect = farfieldQuantity[0, :]
dipoleVector_numberOfPoints = 100
phi = 0 * np.ones(dipoleVector_numberOfPoints)
theta_vect = np.linspace(-np.pi/2, np.pi/2, dipoleVector_numberOfPoints)
visualization.polarizationOrientationDependence(polDegree_vect, theta_vect, phi, dipolePosition, sphereRadius, fluorophore['emissionWavelength'])
"""

histogramFigureName = os.path.join(workingDirectory, "polarizationDegreeHistogram_{}.png".format(simulationName))
meshFigureName = os.path.join(workingDirectory, "polarizationDegreeMesh_{}.png".format(simulationName))
histogramDataName = os.path.join(workingDirectory, "polarizationDegreeData_{}.csv".format(simulationName))

plt.savefig(histogramFigureName)
#plt.savefig(meshFigureName)
outputData = np.vstack((dipolePosition, farfieldQuantity)).T
np.savetxt(histogramDataName, np.vstack((dipolePosition, farfieldQuantity)).T, delimiter=",")
