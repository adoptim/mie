import numpy as np
import matplotlib.pyplot as plt
import math as math

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite
from Mie_calculation.Mie_dipoleEmitter_rates import Mie_dipoleEmitter_rates
import Mie_calculation.Mie_dipoleEmitter_rates_testing \
    as Mie_dipoleEmitter_rates_testing


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

# SWEEP PARAMETERS

n_max = 10

N = 100

sphereRadius = 1 / 2 * np.linspace(60, 200, num=N) * 10 ** -9

def poldeg_calulation(fluorophore, sphereRadius, position="x"):
    
    N = np.size(sphereRadius)
    
    emissionWavelength = np.linspace(fluorophore['emissionWavelength'], fluorophore['emissionWavelength'], num=N) # m
    
    
    dipoleDistance = np.linspace(18, 18, num=N) * 10 ** -9
    
    # OTHER PARAMETERS
    
    # refractive indices of the nanosphere and the immersion medium:
    compositionRatio = 0.3
    refractiveIndex_sphere = refractiveIndex_composite(
        refractiveIndex.gold(emissionWavelength),
        refractiveIndex.silver(emissionWavelength),
        compositionRatio
    )
    # refractiveIndex_sphere = refractiveIndex.gold(emissionWavelength)
    refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
    # refractiveIndex_medium = np.real(refractiveIndex.vacuum(emissionWavelength))
    # refractiveIndex_sphere = refractiveIndex_medium
    
    # POLARIZATION DEGREE
    
    polarizationDegree = np.zeros(emissionWavelength.shape)
    polarizationDegree_QE = np.zeros(emissionWavelength.shape)
    polarizationDegree_excitation = np.zeros(emissionWavelength.shape)
    
    for idx in range(0, N):
    
        sphereRadius_act = sphereRadius[idx]
    
        dipoleDistance_act = dipoleDistance[idx]
    
        # OTHER PARAMETERS
    
        # number of different dipole positions:
        dipolePosition_numberOfPoints = 1
    
        # number of different dipole orientations:
        dipoleVector_numberOfPoints = 200
        #dipoleVector_numberOfPoints = 1
    
        # number of points for the electric field sampling:
        electricField_numberOfPoints_4PI = 500
    
        # distance of the electric field sampling from the center, let it be the
        # experimental objective focal length:
        samplingDistance = 2*10**-3
    
        # collection (half) angle of the objective
        # TODO: water vs immersion layer
        collectionAngle = math.asin(1.49/1.52)
        # collectionAngle = np.pi
    
    
        if position=="x":
            dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        elif position=="y":
            dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideY(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        else:
            raise Exception("Invalid position \"{}\".".format(position))

        dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
    
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree_orientation()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationChannelSignals()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizedPSF(32)
        intensityFilteringRatio = 0.0
        farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.sumSignal()
    
        #signalNormalizationBoolean = True
        signalNormalizationBoolean = False
    
        # slow rotation:
        excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
        emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
    
        farfieldQuantity, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )
    
        # slow rotation:
        excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
        emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.no_absorption(fluorophore['quantumYield'])
    
        farfieldQuantity_QE, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )
    
        # slow rotation:
        excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
        emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
    
        farfieldQuantity_excitation, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )
    
        polarizationDegree[idx] = farfieldQuantity
        polarizationDegree_QE[idx] = farfieldQuantity_QE
        polarizationDegree_excitation[idx] = farfieldQuantity_excitation
        
    return polarizationDegree, polarizationDegree_QE, polarizationDegree_excitation


fluorophore = {}

fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92
polarizationDegree_AF488_x, polarizationDegree_QE_AF488_x, polarizationDegree_excitation_AF488_x = poldeg_calulation(fluorophore, sphereRadius, position="x")
polarizationDegree_AF488_y, polarizationDegree_QE_AF488_y, polarizationDegree_excitation_AF488_y = poldeg_calulation(fluorophore, sphereRadius, position="y")

polarizationDegree_QE_AF488_diff_x = polarizationDegree_QE_AF488_x-polarizationDegree_AF488_x
polarizationDegree_excitation_AF488_x = polarizationDegree_excitation_AF488_x-polarizationDegree_AF488_x
polarizationDegree_QE_AF488_diff_y = -(polarizationDegree_QE_AF488_y-polarizationDegree_AF488_y)
polarizationDegree_excitation_AF488_y = -(polarizationDegree_excitation_AF488_y-polarizationDegree_AF488_y)


fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65
polarizationDegree_Atto647N_x, polarizationDegree_QE_Atto647N_x, polarizationDegree_excitation_Atto647N_x = poldeg_calulation(fluorophore, sphereRadius, position="x")
polarizationDegree_Atto647N_y, polarizationDegree_QE_Atto647N_y, polarizationDegree_excitation_Atto647N_y = poldeg_calulation(fluorophore, sphereRadius, position="y")

polarizationDegree_QE_Atto647N_x = polarizationDegree_QE_Atto647N_x-polarizationDegree_Atto647N_x
polarizationDegree_excitation_Atto647N_x = polarizationDegree_excitation_Atto647N_x-polarizationDegree_Atto647N_x
polarizationDegree_QE_Atto647N_y = -(polarizationDegree_QE_Atto647N_y-polarizationDegree_Atto647N_y)
polarizationDegree_excitation_Atto647N_y = -(polarizationDegree_excitation_Atto647N_y-polarizationDegree_Atto647N_y)

# VISUALIZATION

# The "x" quantity in case of wavelength sweep:
#x = emissionWavelength*10**9				# plot as the function of wavelength in nm
#xlabel = 'wavelength [nm]'

# The "x" quantity in case of particle radius sweep:

#x = sphereRadius*10**9
#xlabel = 'particle radius'

# The "x" quantity in case of particle diameter sweep:
x = 2 * sphereRadius * 10 ** 9
xlabel = "particle diameter"
# The "x" quantity in case of particle radius sweep, dimensionless:
# x = 2*np.pi*sphereRadius/emissionWavelength			# plot as the function of the
# dimensionless diffraction parameter
# xlabel = 'dimensionless diffraction parameter'

# The "x" quantity in case of dipole distance sweep:
# x = dipoleDistance*10**9				# plot as the function of wavelength in nm
# xlabel = 'dipole distance [nm]'

fig, ax = plt.subplots()

plt.plot(x, polarizationDegree_QE_AF488_diff_x, label='AF488, no absorption', color = 'blue', linestyle='dashed', linewidth=2)
plt.plot(x, polarizationDegree_QE_AF488_diff_y, color = 'blue', linestyle='dashed', linewidth=2)
ax.fill_between(x, polarizationDegree_QE_AF488_diff_x, polarizationDegree_QE_AF488_diff_y, alpha=0.4)
plt.plot(x, polarizationDegree_QE_Atto647N_x, label='Atto647N, no absorption', color = 'red', linestyle='dashed', linewidth=2)
plt.plot(x, polarizationDegree_QE_Atto647N_y, color = 'red', linestyle='dashed', linewidth=2)
ax.fill_between(x, polarizationDegree_QE_Atto647N_x, polarizationDegree_QE_Atto647N_y, alpha=0.4)

plt.plot(x, polarizationDegree_excitation_AF488_x, label='AF488, uniform excitation', color = 'blue', linestyle='dotted', linewidth=2)
plt.plot(x, polarizationDegree_excitation_AF488_y, color = 'blue', linestyle='dotted', linewidth=2)
ax.fill_between(x, polarizationDegree_excitation_AF488_x, polarizationDegree_excitation_AF488_y, alpha=0.4)
plt.plot(x, polarizationDegree_excitation_Atto647N_x, label='Atto647N,  uniform excitation', color = 'red', linestyle='dotted', linewidth=2)
plt.plot(x, polarizationDegree_excitation_Atto647N_y, color = 'red', linestyle='dotted', linewidth=2)
ax.fill_between(x, polarizationDegree_excitation_Atto647N_x, polarizationDegree_excitation_Atto647N_y, alpha=0.4)

plt.legend()
#plt.legend(loc='upper left')

plt.xlabel(xlabel, fontsize=14)
plt.ylabel("degree of polarization difference", fontsize=14)

plt.show()
