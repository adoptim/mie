import numpy as np
from tkinter import filedialog
import os

import Mie_calculation.auxiliary.visualization as visualization

measuredDataName = filedialog.askopenfilename(filetypes = [('csv', '*.csv'), ('dat', '*.dat'), ('all','*.*')])
measuredDataDirectory = os.path.dirname(measuredDataName)
simulatedDataName = filedialog.askopenfilename(initialdir = measuredDataDirectory, filetypes = [('csv', '*.csv'), ('dat', '*.dat'), ('all','*.*')])

measuredData = np.loadtxt(measuredDataName, skiprows=1, delimiter=',')
simulatedData  = np.loadtxt(simulatedDataName, delimiter=',')

polarizationDegree_measured = measuredData[:, 0]
polarizationDegree_simulated = simulatedData[:, 3]

sphereRadius = 67/2
emissionWavelength = 670

visualization.polarizationDegreeDoubleHistogram(polarizationDegree_simulated, polarizationDegree_measured, sphereRadius, emissionWavelength, colortheme = 'blue')
