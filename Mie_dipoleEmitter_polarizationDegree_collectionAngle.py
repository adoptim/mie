import numpy as np
import matplotlib.pyplot as plt
import math as math

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite
from Mie_calculation.Mie_dipoleEmitter_rates import Mie_dipoleEmitter_rates
from Mie_calculation.Mie_dipoleEmitter_rates import freeSpaceRadiationRate


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors
import Mie_calculation.auxiliary.Mie_physicalConstants as Mie_physicalConstants

# SWEEP PARAMETERS
n_max=15
sphereRadius = 1 / 2 * 80 * 10 ** -9

N = 100
collectionAngle = np.linspace(5, 90, num=N) * np.pi/180

def poldeg_calulation(fluorophore, sphereRadius, rotationType = 'slow'):
    # Returns the rate enhancements of an ideal dipole (the intrinsic quantum
    # yield of the fluorophore is not taken into account here...).
    
    emissionWavelength = np.linspace(fluorophore['emissionWavelength'], fluorophore['emissionWavelength'], num=N) # m
    
    
    dipoleDistance = 18 * 10 ** -9
    
    # OTHER PARAMETERS
    
    # refractive indices of the nanosphere and the immersion medium:
    compositionRatio = 0.3
    refractiveIndex_sphere = refractiveIndex_composite(
        refractiveIndex.gold(emissionWavelength),
        refractiveIndex.silver(emissionWavelength),
        compositionRatio
    )
    # refractiveIndex_sphere = refractiveIndex.gold(emissionWavelength)
    refractiveIndex_medium = np.real(refractiveIndex.water(emissionWavelength))
    #refractiveIndex_medium = np.real(refractiveIndex.vacuum(emissionWavelength))
    #refractiveIndex_sphere = refractiveIndex_medium
    

    # POLARIZATION DEGREE

    polarizationDegree_x_numerical = np.zeros(emissionWavelength.shape)
    polarizationDegree_y_numerical = np.zeros(emissionWavelength.shape)

    for idx in range(0, N):

        sphereRadius_act = sphereRadius

        dipoleDistance_act = dipoleDistance

        collectionAngle_act = collectionAngle[idx]

        # OTHER PARAMETERS

        # number of different dipole positions:
        dipolePosition_numberOfPoints = 1

        # number of different dipole orientations:
        dipoleVector_numberOfPoints = 20
        #dipoleVector_numberOfPoints = 1

        # number of points for the electric field sampling:
        electricField_numberOfPoints_4PI = 1000

        # distance of the electric field sampling from the center, let it be the
        # experimental objective focal length:
        samplingDistance = 2*10**-3

        dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        #dipolePositionObject = Mie_dipoleVectors.dipole_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
        #dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideY(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        #dipolePositionObject = Mie_dipoleVectors.dipolePosition.top(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        #dipolePositionObject = Mie_dipoleVectors.dipolePosition.arbitrary(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints, 90*np.pi/180, 20*np.pi/180)
        #dipolePositionObject = Mie_dipoleVectors.dipolePosition.arbitrary(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints, 90*np.pi/180, 0*np.pi/180)

        dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
        #dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.perpendicular(dipoleVector_numberOfPoints)
        #dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.parallel_theta(dipoleVector_numberOfPoints)

        if rotationType == 'slow':
            # slow rotation:
            excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
            emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
        elif rotationType == 'fast':
            # fast rotation:
            excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_fastRotation()
            emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_fastRotation(fluorophore['quantumYield'])
            
        elif rotationType == 'none':
            excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
            emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()
        else:
            raise Exception('Invalid rotation type was given.')

        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree_orientation()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationChannelSignals()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizedPSF(32)
        intensityFilteringRatio = 0.0
        farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.sumSignal()
        #farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationChannelSignals()

        #signalNormalizationBoolean = True
        signalNormalizationBoolean = False

        farfieldQuantity, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle_act,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )
        
        polarizationDegree_x_numerical[idx] = farfieldQuantity[0]
        
        dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideY(sphereRadius_act, dipoleDistance_act, dipolePosition_numberOfPoints)
        farfieldQuantity, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
            fluorophore,
            dipolePositionObject,
            dipoleOrientationObject,
            electricField_numberOfPoints_4PI,
            samplingDistance,
            collectionAngle_act,
            refractiveIndex_sphere[idx],
            refractiveIndex_medium[idx],
            excitationWeightingFunction,
            emissionWeightingFunction,
            farfieldQuantityFunction,
            signalNormalizationBoolean,
            n_max
        )
        
        polarizationDegree_y_numerical[idx] = -farfieldQuantity[0]

    return polarizationDegree_x_numerical, polarizationDegree_y_numerical,


fluorophore = {}

fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92
#collectionAngle = 0.3
polarizationDegree_numerical_520_x, polarizationDegree_numerical_520_y = poldeg_calulation(fluorophore, sphereRadius, rotationType = 'slow')

fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65
polarizationDegree_numerical_670_x, polarizationDegree_numerical_670_y = poldeg_calulation(fluorophore, sphereRadius, rotationType = 'slow')

# VISUALIZATION

# The "x" quantity in case of particle diameter sweep:
x = collectionAngle * 180/np.pi
xlabel = "collection angle [°]"

fig, ax = plt.subplots()

plt.plot(x, polarizationDegree_numerical_520_x, label='AF488', color = 'blue', linestyle='dashed', linewidth=2)
plt.plot(x, polarizationDegree_numerical_520_y, color = 'blue', linestyle='dashed', linewidth=2)
ax.fill_between(x, polarizationDegree_numerical_520_x, polarizationDegree_numerical_520_y, alpha=0.4)
plt.plot(x, polarizationDegree_numerical_670_x, label='Atto647N', color = 'red', linestyle='dashed', linewidth=2)
plt.plot(x, polarizationDegree_numerical_670_y, color = 'red', linestyle='dashed', linewidth=2)
ax.fill_between(x, polarizationDegree_numerical_670_x, polarizationDegree_numerical_670_y, alpha=0.4)

plt.gca().set_ylim(top=1.0)

plt.legend()
#plt.legend(loc='upper left')

plt.xlabel(xlabel, fontsize=14)
plt.ylabel("degree of polarization", fontsize=14)

plt.show()
