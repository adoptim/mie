import numpy as np
import matplotlib.pyplot as plt

calibrationDataFileName = '/home/n/Work/corrin/cooperation/Au_Ag_nanospheres_polarization/publication/figures_experiment/particle_size_calibration.txt'

calibrationData  = np.loadtxt(calibrationDataFileName, delimiter='\t')

def calibrationFunction(x, coefficients):
    y = coefficients[0] + coefficients[1]*x + coefficients[2]*x**2
    return y

coefficients = [-1.86, 0.0486, -0.000190]

x = np.linspace(60, 125, num=65)
y = calibrationFunction(x, coefficients)

calibrationLabel = "f(x) = {}$x^2$+{}$x${}".format(coefficients[2], coefficients[1], coefficients[0])

fig=plt.figure(figsize=[5.4, 4.8])
plt.rc('font', size=14)
plt.plot(x, y, color="black", linewidth=2.0)
plt.scatter(calibrationData[:,0], calibrationData[:,1], 150, color="black", marker="x", linewidth=3)
plt.xlabel('particle diameter [nm]', size = 16)
plt.ylabel('contrast', color="black", size = 16)
plt.text(70, 0.4 , calibrationLabel, size = 14)
plt.show