import numpy as np
import math as math

import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

fluorophore = {}
#fluorophore['excitationWavelength'] = 647 * 10 ** -9
#fluorophore['emissionWavelength'] = 670 * 10 ** -9
#fluorophore['quantumYield'] = 0.65
fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92

n_max = 10

sphereRadius = 1 / 2 * 100 * 10 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
#refractiveIndex_sphere = refractiveIndex_medium

# number of different dipole positions:
dipolePosition_numberOfPoints = 100

# number of different dipole orientations:
tiltingNumber = dipolePosition_numberOfPoints

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)
# collectionAngle = np.pi


# slow rotation:
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
#quenchingCoeff = 8
#concentration = 1
#fluorophore['quantumYield'] = fluorophore['quantumYield'] / (concentration * quenchingCoeff)
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])



intensityFilteringRatio = 0.0
farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)

signalNormalizationBoolean = True

dipoleDistance = 5 * 10 ** -9
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_5, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)

dipoleDistance = 10 * 10 ** -9
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_10, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)


dipoleDistance = 20 * 10 ** -9
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_20, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)

dipoleDistance = 40 * 10 ** -9
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_30, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)

dipoleDistance = 80 * 10 ** -9
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_40, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)

dipoleDistance = 160 * 10 ** -9
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
farfieldQuantity_50, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)

dipoleDistance = 1 * 10 ** -9
dipolePositionObject = Mie_dipoleVectors.dipolePosition.sideX(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.tilting_phi(tiltingNumber)
refractiveIndex_sphere = refractiveIndex_medium
farfieldQuantity_controll, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max
)

angles = np.linspace(0, 90, tiltingNumber)

plt.xlabel('Azimuth angle [°]')
#plt.xlabel('Polar angle [°]')
plt.ylabel('Polarization degree')
#plt.plot(angles, farfieldQuantity_5, label='5 nm distance')
plt.plot(angles, farfieldQuantity_10, label='10 nm distance')
plt.plot(angles, farfieldQuantity_20, label='20 nm distance')
plt.plot(angles, farfieldQuantity_30, label='40 nm distance')
plt.plot(angles, farfieldQuantity_40, label='80 nm distance')
#plt.plot(angles, farfieldQuantity_50, label='160 nm distance')
plt.plot(angles, farfieldQuantity_controll, label='without nanoparticle')
plt.legend()
plt.show()

