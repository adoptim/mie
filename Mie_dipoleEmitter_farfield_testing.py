import numpy as np
import math as math

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite

import Mie_calculation.Mie_coefficients as Mie_coefficients
import Mie_calculation.Mie_sphericalVectorWaves as Mie_sphericalVectorWaves
import Mie_calculation.Mie_dipoleEmitter_electricField as \
    Mie_dipoleEmitter_electricField
import Mie_calculation.Mie_electricField as Mie_electricField

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors
import Mie_calculation.auxiliary.pointDistributionOnSphere as \
    pointDistributionOnSphere
import Mie_calculation.auxiliary.coordinateManagement as coordinateManagement
import Mie_calculation.auxiliary.Mie_summationAccessaries as \
    Mie_summationAccessaries
import Mie_calculation.auxiliary.cropPositionsOnSphere as cropPositionsOnSphere

import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as \
    Mie_dipoleEmitter_farfieldQuantities

import Mie_calculation.auxiliary.visualization as visualization

def calculateFarfield(
    
    fluorophore,
    dipolePosition,
    dipoleVector,
    sphereRadius,
    dipoleDistance,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    farfieldQuantityFunction,
):

    n = 10
    m = 1
    sigma = 1
    
    linearIndex_n_m_sigma = Mie_summationAccessaries.indexMapping_n_m_sigma(n, m, sigma)
    
    n_max = n
    
    emissionWavelength = fluorophore['emissionWavelength']
    excitationWavelength = fluorophore['excitationWavelength']

    # point coordinate for the electric farfield sampling, evenly distributed
    # on a conical sphere, 3xN array:
    positionVector_farField = \
        pointDistributionOnSphere.\
        getPositionVectorsWithinCone_sphericalCoordinates(
            electricField_numberOfPoints_4PI, samplingDistance,
            collectionAngle, "even")

    # the summation index dependent constant:
    D_sequence = Mie_summationAccessaries.get_D_sequence(n_max)

    # spherical vector waves for all n, m, sigma index and for all sampling
    # position:
    (M_3_array, N_3_array) = \
        Mie_sphericalVectorWaves.arraysForDipoleElectricField(
            positionVector_farField, emissionWavelength,
            refractiveIndex_medium, n_max)

    farfieldQuantity = farfieldQuantityFunction.initializeArray(1, 1)

    # expansion coefficients for all n, m, sigma index and for all dipole
    # orientation:
    (s_array, t_array, u_array, v_array) = \
        Mie_coefficients.expansionCoefficients_orientationArray(
            dipoleVector, dipolePosition, sphereRadius,
            emissionWavelength, refractiveIndex_sphere,
            refractiveIndex_medium, n_max)


    # the summation index dependent expansion coefficients belonging to
    # the given dipole orientation:
    idxOrientation = 0
    s_sequence_orientation = s_array[idxOrientation, :]
    t_sequence_orientation = t_array[idxOrientation, :]
    u_sequence_orientation = u_array[idxOrientation, :]
    v_sequence_orientation = v_array[idxOrientation, :]

    # electric field distribution on a sampling points within the cone
    # with fixed dipole orientation and dipole position:
    electricField_scattered = \
        Mie_dipoleEmitter_electricField.getElectricField_scattered(
            D_sequence,
            s_sequence_orientation, t_sequence_orientation,
            u_sequence_orientation, v_sequence_orientation,
            M_3_array, N_3_array)
        
    electricField_dipole = \
        Mie_dipoleEmitter_electricField.getElectricField_dipole(
            D_sequence,
            s_sequence_orientation, t_sequence_orientation,
            u_sequence_orientation, v_sequence_orientation,
            M_3_array, N_3_array)
    
    #electricField = electricField_scattered
    #electricField = electricField_dipole
    electricField = electricField_scattered + electricField_dipole

    # get the signal (~photon count) of the three polarization
    # components (the "z" component should be negligible) for the given
    # dipole orientation and dipole position:
    farfieldQuantity[0, 0, :] =\
        farfieldQuantityFunction.getQuantityFromFarfield(
            positionVector_farField, electricField,
            collectionAngle, samplingDistance, refractiveIndex_medium)

    freeDipoleSignal = 1.0
    farfieldQuantity_orientationAveraged, filteringBoolean = farfieldQuantityFunction.averageOrientations(farfieldQuantity, freeDipoleSignal)

    return [electricField, positionVector_farField, farfieldQuantity_orientationAveraged]



fluorophore = {}
fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65
#fluorophore['excitationWavelength'] = 488 * 10 ** -9
#fluorophore['emissionWavelength'] = 520 * 10 ** -9
#fluorophore['quantumYield'] = 0.92

sphereRadius = 1 / 2 * 125 * 10 ** -9

dipoleDistance = 18 * 10 ** -9

# OTHER PARAMETERS

# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
# refractiveIndex_medium = np.ones((refractiveIndex_medium.size))
#refractiveIndex_sphere = refractiveIndex_medium

# position of the dipole
# the radial coordinate of the dipole position ( initializing here as 0 as it
# should be set in the loop over the sweep parameters:
dipolePosition_r = sphereRadius + dipoleDistance
# the polar angle of the dipole position:
dipolePosition_theta = 90. * np.pi/180
# the azimuthal angle of the dipole position:
dipolePosition_fi = 0. * np.pi/180
# the position of the dipole emitter in spherical coordiante system centered at the sphere center:
dipolePosition_sphericalComponents = np.array([
    [dipolePosition_r],
    [dipolePosition_theta],
    [dipolePosition_fi]])

# strength and orientation of the dipole (components of the dipole vector in
# the spherical coordiante system at the dipole position, the "r", "theta"
# and "fi" unit vectors, respectively):
dipoleStrength = 1.0
dipoleVector_theta = 0. * np.pi/180
dipoleVector_phi = 0. * np.pi/180
dipoleVector = dipoleStrength * np.array(
    [
        [np.cos(dipoleVector_theta)],
        [np.sin(dipoleVector_theta) * np.cos(dipoleVector_phi)],
        [np.sin(dipoleVector_theta) * np.sin(dipoleVector_phi)],
    ]
)

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)
# collectionAngle = np.pi


intensityFilteringRatio = 0.0
farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)
#farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.sumSignal()


[electricField, positionVector_farField, farfieldQuantity] = calculateFarfield(
    
    fluorophore,
    dipolePosition_sphericalComponents,
    dipoleVector,
    sphereRadius,
    dipoleDistance,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    farfieldQuantityFunction,
)


print(farfieldQuantity)


intensity_channels = Mie_dipoleEmitter_electricField.electricFieldToIntensity(electricField, refractiveIndex_medium)

intensity = np.sum(intensity_channels, axis=1)
print(np.sum(intensity))

# visualizing the polarization or intensity distribution of the blinking
# events on the spherical surface:
visualization.sphere_mesh3D(positionVector_farField, intensity, 'intensity')

intensity_r = intensity_channels[:,0]
intensity_theta = intensity_channels[:,1]
intensity_phi = intensity_channels[:,2]

visualization.sphere_mesh3D(positionVector_farField, intensity_r, 'intensity r')
visualization.sphere_mesh3D(positionVector_farField, intensity_theta, 'intensity theta')
visualization.sphere_mesh3D(positionVector_farField, intensity_phi, 'intensity phi')
