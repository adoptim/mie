import numpy as np
import math as math

import matplotlib.pyplot as plt

import opticalConstants.refractiveIndex_elements as refractiveIndex
from opticalConstants.refractiveIndex_composite import \
    refractiveIndex_composite


import Mie_calculation.Mie_dipoleEmitter_orientationWeighting as Mie_dipoleEmitter_orientationWeighting
import Mie_calculation.Mie_dipoleEmitter_farfieldQuantities as Mie_dipoleEmitter_farfieldQuantities
import Mie_calculation.Mie_dipoleEmitter_measurementSimulation as Mie_dipoleEmitter_measurementSimulation

import Mie_calculation.auxiliary.visualization as visualization

import Mie_calculation.Mie_dipoleVectors as Mie_dipoleVectors

n_max = 10

sphereRadius = 1 / 2 * 80 * 10 ** -9

# OTHER PARAMETERS

dipoleDistance = 18 * 10 ** -9

# number of different dipole positions:
dipolePosition_numberOfPoints = 100

theta = np.linspace(0, 180, num=dipolePosition_numberOfPoints) * np.pi/180
phi_X = np.ones(dipolePosition_numberOfPoints) * 0 * np.pi/180
phi_Y = np.ones(dipolePosition_numberOfPoints) * 90 * np.pi/180
dipolePositionObject_X = Mie_dipoleVectors.dipolePosition.arbitrary(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints, theta, phi_X)
dipolePositionObject_Y = Mie_dipoleVectors.dipolePosition.arbitrary(sphereRadius, dipoleDistance, dipolePosition_numberOfPoints, theta, phi_Y)

# number of different dipole orientations:
dipoleVector_numberOfPoints = 200

# number of points for the electric field sampling:
electricField_numberOfPoints_4PI = 500

# distance of the electric field sampling from the center, let it be the
# experimental objective focal length:
samplingDistance = 2*10**-3

# collection (half) angle of the objective
# TODO: water vs immersion layer
collectionAngle = math.asin(1.49/1.52)
# collectionAngle = np.pi



intensityFilteringRatio = 0.0
farfieldQuantityFunction = Mie_dipoleEmitter_farfieldQuantities.farfieldQuantity.polarizationDegree(intensityFilteringRatio)

signalNormalizationBoolean = False

fluorophore = {}
fluorophore['excitationWavelength'] = 488 * 10 ** -9
fluorophore['emissionWavelength'] = 520 * 10 ** -9
fluorophore['quantumYield'] = 0.92


# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
#refractiveIndex_sphere = refractiveIndex_medium

# slow rotation:
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
#excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
#emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()

dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
farfieldQuantity_520_X, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject_X,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max,
    coverSlipCropping = False
)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
farfieldQuantity_520_Y, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject_Y,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max,
    coverSlipCropping = False
)

fluorophore = {}
fluorophore['excitationWavelength'] = 647 * 10 ** -9
fluorophore['emissionWavelength'] = 670 * 10 ** -9
fluorophore['quantumYield'] = 0.65


# refractive indices of the nanosphere and the immersion medium:
compositionRatio = 0.3
refractiveIndex_sphere = refractiveIndex_composite(
    refractiveIndex.gold(fluorophore['emissionWavelength']),
    refractiveIndex.silver(fluorophore['emissionWavelength']),
    compositionRatio
)
refractiveIndex_medium = np.real(refractiveIndex.water(fluorophore['emissionWavelength']))
#refractiveIndex_sphere = refractiveIndex_medium

# slow rotation:
excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.planeWave_direction_X_polarization_Z_slowRotation()
emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.excitationLimitedCase_slowRotation(fluorophore['quantumYield'])
#excitationWeightingFunction = Mie_dipoleEmitter_orientationWeighting.excitationWeighting.none()
#emissionWeightingFunction = Mie_dipoleEmitter_orientationWeighting.emissionWeighting.none()

dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
farfieldQuantity_670_X, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject_X,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max,
    coverSlipCropping = False
)
dipoleOrientationObject = Mie_dipoleVectors.dipoleOrientation.evenDistribution_4pi(dipoleVector_numberOfPoints)
farfieldQuantity_670_Y, dipolePosition = Mie_dipoleEmitter_measurementSimulation.simulate(
    fluorophore,
    dipolePositionObject_Y,
    dipoleOrientationObject,
    electricField_numberOfPoints_4PI,
    samplingDistance,
    collectionAngle,
    refractiveIndex_sphere,
    refractiveIndex_medium,
    excitationWeightingFunction,
    emissionWeightingFunction,
    farfieldQuantityFunction,
    signalNormalizationBoolean,
    n_max,
    coverSlipCropping = False
)

theta_degree = theta * 180/np.pi

plt.figure()
plt.xlabel('latitudinal position [°]', fontsize=14)
#plt.xlabel('Polar angle [°]')
plt.ylabel('degree of polarization', fontsize=14)
plt.plot(theta_degree, np.abs(farfieldQuantity_520_X), label='520 nm emission', linewidth=2, linestyle='dashed', color='blue')
plt.plot(theta_degree, np.abs(farfieldQuantity_520_Y), linewidth=2, linestyle='dashed', color='blue')
plt.gca().fill_between(theta_degree, np.abs(farfieldQuantity_520_X), np.abs(farfieldQuantity_520_Y), alpha=0.4)
plt.plot(theta_degree, np.abs(farfieldQuantity_670_X), label='670 nm emission', linewidth=2, linestyle='dashed', color='red')
plt.plot(theta_degree, np.abs(farfieldQuantity_670_Y), linewidth=2, linestyle='dashed', color='red')
plt.gca().fill_between(theta_degree, np.abs(farfieldQuantity_670_X), np.abs(farfieldQuantity_670_Y), alpha=0.4)

#plt.legend(loc='upper center')
#plt.xticks(ticks=np.array([-90, -60, -30, 0, 30, 60, 90]));
plt.legend(loc='upper right')
plt.xticks(ticks=np.array([0, 30, 60, 90, 120, 150, 180]));

#plt.legend(shadow=True, fontsize='x-large')

plt.show()

